#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(comparison_not_equal)

    BOOST_AUTO_TEST_CASE(with_same_type)
    {
        safe_integer a = 1;
        safe_integer b = 2;
        safe_integer c = 2;

        BOOST_TEST(a != b);
        BOOST_TEST(!(b != c));
    }

    BOOST_AUTO_TEST_CASE(with_same_type_constexpr_ness)
    {
        safe_integer constexpr a = 1;
        safe_integer constexpr b = 2;

        auto constexpr ab = a != b; BOOST_TEST(ab);
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_1)
    {
        auto constexpr i32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t> a = i32_max;

        safe_integer<int64_t> b = i32_max + 1L;
        safe_integer<int64_t> c = i32_max;

        BOOST_TEST(a != b);
        BOOST_TEST(!(a != c));

        BOOST_TEST(b != a);
        BOOST_TEST(!(c != a));
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_2)
    {
        auto constexpr i32_min = std::numeric_limits<int32_t>::min();

        safe_integer<int32_t> a = i32_min;

        safe_integer<int64_t> b = i32_min - 1L;
        safe_integer<int64_t> c = i32_min;

        BOOST_TEST(a != b);
        BOOST_TEST(!(a != c));

        BOOST_TEST(b != a);
        BOOST_TEST(!(c != a));
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_constexpr_ness)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<int64_t> constexpr b = 2;

        auto constexpr ab = a != b; BOOST_TEST(ab);
        auto constexpr ba = b != a; BOOST_TEST(ba);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_1)
    {
        auto constexpr i32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t> a = i32_max;

        safe_integer<uint32_t> b = i32_max + 1L;
        safe_integer<uint32_t> c = i32_max;

        BOOST_TEST(a != b);
        BOOST_TEST(!(a != c));

        BOOST_TEST(b != a);
        BOOST_TEST(!(c != a));
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_2)
    {
        safe_integer<uint32_t> a = 0;

        safe_integer<int32_t> b = -1;
        safe_integer<int32_t> c = 0;

        BOOST_TEST(a != b);
        BOOST_TEST(!(a != c));

        BOOST_TEST(b != a);
        BOOST_TEST(!(c != a));
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_constexpr_ness)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<uint32_t> constexpr b = 2;

        auto constexpr ab = a != b; BOOST_TEST(ab);
        auto constexpr ba = b != a; BOOST_TEST(ba);
    }

    BOOST_AUTO_TEST_CASE(with_builtin_type)
    {
        safe_integer<int> si1 = 1;

        int i1 = 2;
        int i2 = 1;

        BOOST_TEST(si1 != i1);
        BOOST_TEST(!(si1 != i2));

        BOOST_TEST(i1 != si1);
        BOOST_TEST(!(i2 != si1));
    }

    BOOST_AUTO_TEST_CASE(with_builtin_type_constexpr_ness)
    {
        safe_integer constexpr a = 1;
        int constexpr b = 2;

        auto constexpr ab = a != b; BOOST_TEST(ab);
        auto constexpr ba = b != a; BOOST_TEST(ba);
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type_1)
    {
        auto constexpr i0 = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t> si1 = i0;

        int64_t i1 = i0 + 1L;
        int64_t i2 = i0;

        BOOST_TEST(si1 != i1);
        BOOST_TEST(!(si1 != i2));

        BOOST_TEST(i1 != si1);
        BOOST_TEST(!(i2 != si1));
    }

    BOOST_AUTO_TEST_CASE(with_differnet_bultin_type_2)
    {
        auto constexpr i0 = std::numeric_limits<int32_t>::min();

        safe_integer<int32_t> si1 = i0;

        int64_t i1 = i0 - 1L;
        int64_t i2 = i0;

        BOOST_TEST(si1 != i1);
        BOOST_TEST(!(si1 != i2));

        BOOST_TEST(i1 != si1);
        BOOST_TEST(!(i2 != si1));
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type_constexpr_ness)
    {
        safe_integer<int32_t> constexpr a = 1;
        int64_t constexpr b = 2;

        auto constexpr ab = a != b; BOOST_TEST(ab);
        auto constexpr ba = b != a; BOOST_TEST(ba);
    }

    BOOST_AUTO_TEST_CASE(with_diffently_signed_builtin_type_1)
    {
        auto constexpr i0 = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t> si1 = i0;

        uint64_t i1 = i0 + 1L;
        uint64_t i2 = i0;

        BOOST_TEST(si1 != i1);
        BOOST_TEST(!(si1 != i2));

        BOOST_TEST(i1 != si1);
        BOOST_TEST(!(i2 != si1));
    }

    BOOST_AUTO_TEST_CASE(with_diffently_signed_builtin_type_2)
    {
        safe_integer<uint32_t> si1 = 0;

        int32_t i1 = -1;
        int32_t i2 = 0;

        BOOST_TEST(i1 != si1);
        BOOST_TEST(!(i2 != si1));

        BOOST_TEST(i1 != si1);
        BOOST_TEST(!(i2 != si1));
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_constexpr_ness)
    {
        safe_integer<int32_t> constexpr a = 1;
        uint64_t constexpr b = 2;

        auto constexpr ab = a != b; BOOST_TEST(ab);
        auto constexpr ba = b != a; BOOST_TEST(ba);

        safe_integer<uint32_t> constexpr c = 1;
        int64_t constexpr d = 2;

        auto constexpr cd = c != d; BOOST_TEST(cd);
        auto constexpr dc = d != c; BOOST_TEST(dc);
    }

    BOOST_AUTO_TEST_CASE(between_bounded_safe_integer_overlapped)
    {
        safe_integer<int, 0, 1> a = 1;

        safe_integer<int, 1, 2> b = 2;
        safe_integer<int, 1, 2> c = 1;

        BOOST_TEST(a != b);
        BOOST_TEST(b != a);

        BOOST_TEST(!(a != c));
        BOOST_TEST(!(c != a));
    }

    BOOST_AUTO_TEST_CASE(between_bounded_safe_integer_not_overlapped)
    {
        safe_integer<int, 0, 1> a = 1;
        safe_integer<int, 2, 3> b = 2;

        BOOST_TEST(a != b);
        BOOST_TEST(b != a);
    }

    BOOST_AUTO_TEST_CASE(between_bounded_safe_integer_constexpr_ness)
    {
        safe_integer<int, 0, 1> constexpr a = 1;
        safe_integer<int, 1, 2> constexpr b = 2;
        safe_integer<int, 2, 3> constexpr c = 3;

        auto constexpr ab = a != b; BOOST_TEST(ab);
        auto constexpr ba = b != a; BOOST_TEST(ba);
        auto constexpr ac = a != c; BOOST_TEST(ac);
    }

BOOST_AUTO_TEST_SUITE_END() // comparison_not_equal_
