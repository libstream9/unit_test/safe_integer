#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(comparison_equal)

    BOOST_AUTO_TEST_CASE(with_same_type)
    {
        safe_integer a = 1;
        safe_integer b = 1;
        safe_integer c = 2;

        BOOST_TEST(a == b);
        BOOST_TEST(!(a == c));
    }

    BOOST_AUTO_TEST_CASE(with_same_type_constexpr)
    {
        safe_integer constexpr a = 1;
        safe_integer constexpr b = 1;

        static_assert(a == b);
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_1)
    {
        auto constexpr i32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t> a = i32_max;

        safe_integer<int64_t> b = i32_max;
        safe_integer<int64_t> c = i32_max + 1L;

        BOOST_TEST(a == b);
        BOOST_TEST(!(a == c));

        BOOST_TEST(b == a);
        BOOST_TEST(!(c == a));
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_2)
    {
        auto constexpr i32_min = std::numeric_limits<int32_t>::min();

        safe_integer<int32_t> a = i32_min;

        safe_integer<int64_t> b = i32_min;
        safe_integer<int64_t> c = i32_min - 1L;

        BOOST_TEST(a == b);
        BOOST_TEST(!(a == c));

        BOOST_TEST(b == a);
        BOOST_TEST(!(c == a));
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<int64_t> constexpr b = 1;

        static_assert(a == b);
        static_assert(b == a);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_1)
    {
        safe_integer<int32_t> a = INT32_MAX;

        safe_integer<uint32_t> b = INT32_MAX;
        safe_integer<uint32_t> c = INT32_MAX + 1L;

        BOOST_TEST(a == b);
        BOOST_TEST(!(a == c));

        BOOST_TEST(b == a);
        BOOST_TEST(!(c == a));
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_2)
    {
        safe_integer<uint32_t> a = 0;

        safe_integer<int32_t> b = 0;
        safe_integer<int32_t> c = -1;

        BOOST_TEST(a == b);
        BOOST_TEST(!(a == c));

        BOOST_TEST(b == a);
        BOOST_TEST(!(c == a));
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<uint32_t> constexpr b = 1;

        static_assert(a == b);
        static_assert(b == a);
    }

    BOOST_AUTO_TEST_CASE(with_builtin_type)
    {
        safe_integer<int> a = 1;

        int b = 1;
        int c = 2;

        BOOST_TEST(a == b);
        BOOST_TEST(!(a == c));

        BOOST_TEST(b == a);
        BOOST_TEST(!(c == a));
    }

    BOOST_AUTO_TEST_CASE(with_builtin_type_constexpr)
    {
        safe_integer constexpr a = 1;
        int constexpr b = 1;

        static_assert(a == b);
        static_assert(b == a);
    }

    BOOST_AUTO_TEST_CASE(with_differnet_bultin_type_1)
    {
        safe_integer<int32_t> si1 = INT32_MAX;

        int64_t i1 = INT32_MAX;
        int64_t i2 = INT32_MAX + 1L;

        BOOST_TEST(si1 == i1);
        BOOST_TEST(!(si1 == i2));

        BOOST_TEST(i1 == si1);
        BOOST_TEST(!(i2 == si1));
    }

    BOOST_AUTO_TEST_CASE(with_differnet_bultin_type_2)
    {
        safe_integer<int32_t> si1 = INT32_MAX;

        int64_t i1 = INT32_MAX;
        int64_t i2 = INT32_MAX - 1L;

        BOOST_TEST(si1 == i1);
        BOOST_TEST(!(si1 == i2));

        BOOST_TEST(i1 == si1);
        BOOST_TEST(!(i2 == si1));
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        int64_t constexpr b = 1;

        static_assert(a == b);
        static_assert(b == a);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_1)
    {
        safe_integer<int32_t> si1 = INT32_MAX;

        uint64_t i1 = INT32_MAX;
        uint64_t i2 = INT32_MAX + 1L;

        BOOST_TEST(si1 == i1);
        BOOST_TEST(!(si1 == i2));

        BOOST_TEST(si1 == i1);
        BOOST_TEST(!(si1 == i2));
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_2)
    {
        safe_integer<uint32_t> si1 = 0;

        int32_t i1 = 0;
        int32_t i2 = -1;

        BOOST_TEST(i1 == si1);
        BOOST_TEST(!(i2 == si1));

        BOOST_TEST(i1 == si1);
        BOOST_TEST(!(i2 == si1));
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        uint64_t constexpr b = 1;

        static_assert(a == b);
        static_assert(b == a);

        safe_integer<uint32_t> constexpr c = 1;
        int64_t constexpr d = 1;

        static_assert(c == d);
        static_assert(d == c);
    }

    BOOST_AUTO_TEST_CASE(with_bounded_safe_integer_overlapped_boundary)
    {
        safe_integer<int, 0, 1> a = 1;

        safe_integer<int, 1, 2> b = 1;
        safe_integer<int, 1, 2> c = 2;

        BOOST_TEST(a == b);
        BOOST_TEST(!(a == c));

        BOOST_TEST(b == a);
        BOOST_TEST(!(c == a));
    }

    BOOST_AUTO_TEST_CASE(with_bounded_safe_integer_not_overlapped_boundary)
    {
        safe_integer<int, 0, 1> a = 1;
        safe_integer<int, 2, 3> b = 2;

        BOOST_TEST(!(a == b));
        BOOST_TEST(!(b == a));
    }

    BOOST_AUTO_TEST_CASE(with_bounded_safe_integer_constexpr)
    {
        safe_integer<int, 0, 1> constexpr a = 1;
        safe_integer<int, 1, 2> constexpr b = 1;
        safe_integer<int, 2, 3> constexpr c = 2;

        static_assert(a == b);
        static_assert(b == a);
        static_assert(!(a == c));
    }

BOOST_AUTO_TEST_SUITE_END() // comparison_equal

BOOST_AUTO_TEST_SUITE(equal_)

    BOOST_AUTO_TEST_CASE(signed_with_signed)
    {
        safe_integer<int> a = 1;
        safe_integer<int> b = 1;
        safe_integer<int> c = 2;

        BOOST_TEST(equal(a, b));

        BOOST_TEST(!equal(a, c));
    }

    BOOST_AUTO_TEST_CASE(unsigned_with_unsigned)
    {
        safe_integer<unsigned> a = 1;
        safe_integer<unsigned> b = 1;
        safe_integer<unsigned> c = 2;

        BOOST_TEST(equal(a, b));

        BOOST_TEST(!equal(a, c));
    }

    BOOST_AUTO_TEST_CASE(signed_with_unsigned)
    {
        safe_integer<int> a = 1;
        safe_integer<unsigned> b = 1;
        safe_integer<unsigned> c = 2;

        BOOST_TEST(equal(a, b));
        BOOST_TEST(equal(b, a));

        BOOST_TEST(!equal(a, c));
        BOOST_TEST(!equal(c, a));
    }

    BOOST_AUTO_TEST_CASE(non_negative_signed_with_unsigned)
    {
        safe_integer<int, 0, 100> a = 1;
        safe_integer<unsigned> b = 1;
        safe_integer<unsigned> c = 2;

        BOOST_TEST(equal(a, b));
        BOOST_TEST(equal(b, a));

        BOOST_TEST(!equal(a, c));
        BOOST_TEST(!equal(c, a));
    }

    BOOST_AUTO_TEST_CASE(with_builtin)
    {
        safe_integer a = 1;
        int b = 1;
        int c = 2;

        BOOST_TEST(equal(a, b));
        BOOST_TEST(equal(b, a));

        BOOST_TEST(!equal(a, c));
        BOOST_TEST(!equal(c, a));
    }

    BOOST_AUTO_TEST_CASE(not_overlapped)
    {
        safe_integer<int32_t, -10, -1> a = -1;
        safe_integer<int32_t, 0, 10> b = 1;

        BOOST_TEST(!equal(a, b));
        BOOST_TEST(!equal(b, a));
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer constexpr a = 1;
        safe_integer constexpr b = 1;
        safe_integer constexpr c = 2;

        static_assert(equal(a, b));
        static_assert(equal(b, a));

        static_assert(!equal(a, c));
        static_assert(!equal(c, a));
    }

BOOST_AUTO_TEST_SUITE_END() // equal_
