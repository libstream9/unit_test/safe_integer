#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(comparison_greater_equal)

    BOOST_AUTO_TEST_CASE(with_same_type)
    {
        safe_integer a = 1;
        safe_integer b = 0;
        safe_integer c = 1;
        safe_integer d = 2;

        BOOST_TEST(a >= b);
        BOOST_TEST(a >= c);
        BOOST_TEST(!(a >= d));
    }

    BOOST_AUTO_TEST_CASE(with_same_type_constexpr)
    {
        safe_integer constexpr a = 2;
        safe_integer constexpr b = 1;

        auto constexpr ab = a >= b; BOOST_TEST(ab);
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_1)
    {
        auto constexpr i32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t> a = i32_max;

        safe_integer<int64_t> b = i32_max - 1L;
        safe_integer<int64_t> c = i32_max;
        safe_integer<int64_t> d = i32_max + 1L;

        BOOST_TEST(a >= b);
        BOOST_TEST(a >= c);
        BOOST_TEST(!(a >= d));

        BOOST_TEST(!(b >= a));
        BOOST_TEST(c >= a);
        BOOST_TEST(d >= a);
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_2)
    {
        auto constexpr i32_min = std::numeric_limits<int32_t>::min();

        safe_integer<int32_t> a = i32_min;

        safe_integer<int64_t> b = i32_min - 1L;
        safe_integer<int64_t> c = i32_min;
        safe_integer<int64_t> d = i32_min + 1L;

        BOOST_TEST(a >= b);
        BOOST_TEST(a >= c);
        BOOST_TEST(!(a >= d));

        BOOST_TEST(!(b >= a));
        BOOST_TEST(c >= a);
        BOOST_TEST(d >= a);
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_constexpr)
    {
        safe_integer<int32_t> constexpr a = 2;
        safe_integer<int64_t> constexpr b = 1;

        auto constexpr ab = a >= b; BOOST_TEST(ab);
        auto constexpr ba = b >= a; BOOST_TEST(!ba);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_1)
    {
        auto constexpr i32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t> a = i32_max;

        safe_integer<uint32_t> b = i32_max - 1u;
        safe_integer<uint32_t> c = i32_max;
        safe_integer<uint32_t> d = i32_max + 1u;

        BOOST_TEST(a >= b);
        BOOST_TEST(a >= c);
        BOOST_TEST(!(a >= d));

        BOOST_TEST(!(b >= a));
        BOOST_TEST(c >= a);
        BOOST_TEST(d >= a);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_2)
    {
        safe_integer<uint32_t> a = 0;

        safe_integer<int32_t> b = -1;
        safe_integer<int32_t> c = 0;
        safe_integer<int32_t> d = 1;

        BOOST_TEST(a >= b);
        BOOST_TEST(a >= c);
        BOOST_TEST(!(a >= d));

        BOOST_TEST(!(b >= a));
        BOOST_TEST(c >= a);
        BOOST_TEST(d >= a);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_constexpr)
    {
        safe_integer<int32_t> constexpr a = 2;
        safe_integer<uint32_t> constexpr b = 1;

        auto constexpr ab = a >= b; BOOST_TEST(ab);
        auto constexpr ba = b >= a; BOOST_TEST(!ba);
    }

    BOOST_AUTO_TEST_CASE(with_builtin_type_1)
    {
        safe_integer<int> si1 = 1;

        int i1 = 0;
        int i2 = 1;
        int i3 = 2;

        BOOST_TEST(si1 >= i1);
        BOOST_TEST(si1 >= i2);
        BOOST_TEST(!(si1 >= i3));

        BOOST_TEST(!(i1 >= si1));
        BOOST_TEST(i2 >= si1);
        BOOST_TEST(i3 >= si1);
    }

    BOOST_AUTO_TEST_CASE(with_builtin_type_constexpr)
    {
        safe_integer constexpr a = 2;
        int constexpr b = 1;

        auto constexpr ab = a >= b; BOOST_TEST(ab);
        auto constexpr ba = b >= a; BOOST_TEST(!ba);
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type_1)
    {
        auto constexpr i0 = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t> si1 = i0;

        int64_t i1 = i0 - 1L;
        int64_t i2 = i0;
        int64_t i3 = i0 + 1L;

        BOOST_TEST(si1 >= i1);
        BOOST_TEST(si1 >= i2);
        BOOST_TEST(!(si1 >= i3));

        BOOST_TEST(!(i1 >= si1));
        BOOST_TEST(i2 >= si1);
        BOOST_TEST(i3 >= si1);
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type_2)
    {
        auto constexpr i0 = std::numeric_limits<int32_t>::min();

        safe_integer<int32_t> si1 = i0;

        int64_t i1 = i0 - 1L;
        int64_t i2 = i0;
        int64_t i3 = i0 + 1L;

        BOOST_TEST(si1 >= i1);
        BOOST_TEST(si1 >= i2);
        BOOST_TEST(!(si1 >= i3));

        BOOST_TEST(!(i1 >= si1));
        BOOST_TEST(i2 >= si1);
        BOOST_TEST(i3 >= si1);
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 2;
        int64_t constexpr b = 1;

        auto constexpr ab = a >= b; BOOST_TEST(ab);
        auto constexpr ba = b >= a; BOOST_TEST(!ba);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_1)
    {
        auto constexpr i0 = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t> si1 = i0;

        uint32_t i1 = i0 - 1u;
        uint32_t i2 = i0;
        uint32_t i3 = i0 + 1u;

        BOOST_TEST(si1 >= i1);
        BOOST_TEST(si1 >= i2);
        BOOST_TEST(!(si1 >= i3));

        BOOST_TEST(!(i1 >= si1));
        BOOST_TEST(i2 >= si1);
        BOOST_TEST(i3 >= si1);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_2)
    {
        safe_integer<uint32_t> si1 = 0;

        int32_t i1 = -1;
        int32_t i2 = 0;
        int32_t i3 = 1;

        BOOST_TEST(si1 >= i1);
        BOOST_TEST(si1 >= i2);
        BOOST_TEST(!(si1 >= i3));

        BOOST_TEST(!(i1 >= si1));
        BOOST_TEST(i2 >= si1);
        BOOST_TEST(i3 >= si1);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 2;
        uint32_t constexpr b = 1;

        auto constexpr ab = a >= b; BOOST_TEST(ab);
        auto constexpr ba = b >= a; BOOST_TEST(!ba);

        safe_integer<uint32_t> constexpr c = 2;
        int32_t constexpr d = 1;

        auto constexpr cd = c >= d; BOOST_TEST(cd);
        auto constexpr dc = d >= c; BOOST_TEST(!dc);
    }

    BOOST_AUTO_TEST_CASE(between_bounded_safe_integer_overlapped)
    {
        safe_integer<int, 0, 1> a = 1;

        safe_integer<int, 0, 2> b = 0;
        safe_integer<int, 0, 2> c = 1;
        safe_integer<int, 0, 2> d = 2;

        BOOST_TEST(a >= b);
        BOOST_TEST(a >= c);
        BOOST_TEST(!(a >= d));

        BOOST_TEST(!(b >= a));
        BOOST_TEST(c >= a);
        BOOST_TEST(d >= a);
    }

    BOOST_AUTO_TEST_CASE(between_bounded_safe_integer_not_overlapped)
    {
        safe_integer<int, 2, 3> a = 2;
        safe_integer<int, 0, 1> b = 1;

        BOOST_TEST(a >= b);
        BOOST_TEST(!(b >= a));
    }

    BOOST_AUTO_TEST_CASE(between_bounded_safe_integer_constexpr)
    {
        safe_integer<int, 2, 3> constexpr a = 2;
        safe_integer<int, 1, 2> constexpr b = 1;
        safe_integer<int, 0, 1> constexpr c = 1;

        auto constexpr ab = a >= b; BOOST_TEST(ab);
        auto constexpr ac = a >= c; BOOST_TEST(ac);
        auto constexpr ca = c >= a; BOOST_TEST(!ca);
    }

BOOST_AUTO_TEST_SUITE_END() // comparison_greater_equal
