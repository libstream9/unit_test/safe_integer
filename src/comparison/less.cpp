#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

#include <cstdint>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(comparison_less)

    BOOST_AUTO_TEST_CASE(with_same_type)
    {
        safe_integer a = 1;
        safe_integer b = 2;
        safe_integer c = 1;
        safe_integer d = 0;

        BOOST_TEST(a < b);
        BOOST_TEST(!(a < c));
        BOOST_TEST(!(a < d));
    }

    BOOST_AUTO_TEST_CASE(with_same_type_constexpr)
    {
        safe_integer constexpr a = 1;
        safe_integer constexpr b = 2;

        static_assert(a < b);
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_1)
    {
        safe_integer<int32_t> a = INT32_MAX;

        safe_integer<int64_t> b = INT32_MAX + 1L;
        safe_integer<int64_t> c = INT32_MAX;
        safe_integer<int64_t> d = INT32_MAX - 1L;

        BOOST_TEST(a < b);
        BOOST_TEST(!(a < c));
        BOOST_TEST(!(a < d));

        BOOST_TEST(!(b < a));
        BOOST_TEST(!(c < a));
        BOOST_TEST(d < a);
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_2)
    {
        safe_integer<int32_t> a = INT32_MIN;

        safe_integer<int64_t> b = INT32_MIN + 1L;
        safe_integer<int64_t> c = INT32_MIN;
        safe_integer<int64_t> d = INT32_MIN - 1L;

        BOOST_TEST(a < b);
        BOOST_TEST(!(a < c));
        BOOST_TEST(!(a < d));

        BOOST_TEST(!(b < a));
        BOOST_TEST(!(c < a));
        BOOST_TEST(d < a);
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<int64_t> constexpr b = 2;

        static_assert(a < b);
        static_assert(!(b < a));
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_1)
    {
        safe_integer<int32_t> a = INT32_MAX;

        safe_integer<uint32_t> b = INT32_MAX + 1u;
        safe_integer<uint32_t> c = INT32_MAX;
        safe_integer<uint32_t> d = INT32_MAX - 1u;

        BOOST_TEST(a < b);
        BOOST_TEST(!(a < c));
        BOOST_TEST(!(a < d));

        BOOST_TEST(!(b < a));
        BOOST_TEST(!(c < a));
        BOOST_TEST(d < a);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_2)
    {
        safe_integer<uint32_t> a = 0;

        safe_integer<int32_t> b = 1;
        safe_integer<int32_t> c = 0;
        safe_integer<int32_t> d = -1;

        BOOST_TEST(a < b);
        BOOST_TEST(!(a < c));
        BOOST_TEST(!(a < d));

        BOOST_TEST(!(b < a));
        BOOST_TEST(!(c < a));
        BOOST_TEST(d < a);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<uint32_t> constexpr b = 2;

        static_assert(a < b);
        static_assert(!(b < a));
    }

    BOOST_AUTO_TEST_CASE(with_builtin_type)
    {
        safe_integer<int> si1 = 1;

        int i1 = 2;
        int i2 = 1;
        int i3 = 0;

        BOOST_TEST(si1 < i1);
        BOOST_TEST(!(si1 < i2));
        BOOST_TEST(!(si1 < i3));

        BOOST_TEST(!(i1 < si1));
        BOOST_TEST(!(i2 < si1));
        BOOST_TEST(i3 < si1);
    }

    BOOST_AUTO_TEST_CASE(with_builtin_type_constexpr)
    {
        safe_integer constexpr a = 1;
        int constexpr b = 2;

        static_assert(a < b);
        static_assert(!(b < a));
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type_1)
    {
        safe_integer<int32_t> si1 = INT32_MAX;

        int64_t i1 = INT32_MAX + 1L;
        int64_t i2 = INT32_MAX;
        int64_t i3 = INT32_MAX - 1L;

        BOOST_TEST(si1 < i1);
        BOOST_TEST(!(si1 < i2));
        BOOST_TEST(!(si1 < i3));

        BOOST_TEST(!(i1 < si1));
        BOOST_TEST(!(i2 < si1));
        BOOST_TEST(i3 < si1);
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type_2)
    {
        safe_integer<int32_t> si1 = INT32_MIN;

        int64_t i1 = INT32_MIN + 1L;
        int64_t i2 = INT32_MIN;
        int64_t i3 = INT32_MIN - 1L;

        BOOST_TEST(si1 < i1);
        BOOST_TEST(!(si1 < i2));
        BOOST_TEST(!(si1 < i3));

        BOOST_TEST(!(i1 < si1));
        BOOST_TEST(!(i2 < si1));
        BOOST_TEST(i3 < si1);
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        int64_t constexpr b = 2;

        static_assert(a < b);
        static_assert(!(b < a));
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_1)
    {
        safe_integer<int32_t> si1 = INT32_MAX;

        uint32_t i1 = INT32_MAX + 1u;
        uint32_t i2 = INT32_MAX;
        uint32_t i3 = INT32_MAX - 1u;

        BOOST_TEST(si1 < i1);
        BOOST_TEST(!(si1 < i2));
        BOOST_TEST(!(si1 < i3));

        BOOST_TEST(!(i1 < si1));
        BOOST_TEST(!(i2 < si1));
        BOOST_TEST(i3 < si1);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_2)
    {
        safe_integer<uint32_t> si1 = 0;

        int32_t i1 = 1;
        int32_t i2 = 0;
        int32_t i3 = -1;

        BOOST_TEST(si1 < i1);
        BOOST_TEST(!(si1 < i2));
        BOOST_TEST(!(si1 < i3));

        BOOST_TEST(!(i1 < si1));
        BOOST_TEST(!(i2 < si1));
        BOOST_TEST(i3 < si1);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        uint32_t constexpr b = 2;

        static_assert(a < b);
        static_assert(!(b < a));

        safe_integer<uint32_t> constexpr c = 1;
        int32_t constexpr d = 2;

        static_assert(c < d);
        static_assert(!(d < c));
    }

    BOOST_AUTO_TEST_CASE(between_bounded_safe_integer_overlapped)
    {
        safe_integer<int, 0, 1> a = 1;

        safe_integer<int, 0, 2> b = 2;
        safe_integer<int, 0, 2> c = 1;
        safe_integer<int, 0, 2> d = 0;

        BOOST_TEST(a < b);
        BOOST_TEST(!(a < c));
        BOOST_TEST(!(a < d));

        BOOST_TEST(!(b < a));
        BOOST_TEST(!(c < a));
        BOOST_TEST(d < a);
    }

    BOOST_AUTO_TEST_CASE(between_bounded_safe_integer_not_overlapped)
    {
        safe_integer<int, 0, 1> a = 1;
        safe_integer<int, 2, 3> b = 2;

        BOOST_TEST(a < b);
        BOOST_TEST(!(b < a));
    }

    BOOST_AUTO_TEST_CASE(between_bounded_safe_integer_constexpr)
    {
        safe_integer<int, 0, 1> constexpr a = 1;
        safe_integer<int, 1, 2> constexpr b = 2;
        safe_integer<int, 2, 3> constexpr c = 2;

        static_assert(a < b);
        static_assert(a < c);
        static_assert(!(c < a));
    }

BOOST_AUTO_TEST_SUITE_END() // comparison_less

BOOST_AUTO_TEST_SUITE(less_)

    BOOST_AUTO_TEST_CASE(signed_with_signed)
    {
        safe_integer<int> a = 1;
        safe_integer<int> b = 2;
        safe_integer<int> c = 1;
        safe_integer<int> d = 0;

        BOOST_TEST(less(a, b));
        BOOST_TEST(!less(a, c));
        BOOST_TEST(!less(a, d));
    }

    BOOST_AUTO_TEST_CASE(unsigned_with_unsigned)
    {
        safe_integer<unsigned> a = 1;
        safe_integer<unsigned> b = 2;
        safe_integer<unsigned> c = 1;
        safe_integer<unsigned> d = 0;

        BOOST_TEST(less(a, b));
        BOOST_TEST(!less(a, c));
        BOOST_TEST(!less(a, d));
    }

    BOOST_AUTO_TEST_CASE(signed_with_unsigned)
    {
        safe_integer<int> a = 1;
        safe_integer<unsigned> b = 2;
        safe_integer<unsigned> c = 1;
        safe_integer<unsigned> d = 0;

        BOOST_TEST(less(a, b));
        BOOST_TEST(!less(a, c));
        BOOST_TEST(!less(a, d));
    }

    BOOST_AUTO_TEST_CASE(unsigned_with_signed)
    {
        safe_integer<unsigned> a = 1;
        safe_integer<int> b = 2;
        safe_integer<int> c = 1;
        safe_integer<int> d = 0;

        BOOST_TEST(less(a, b));
        BOOST_TEST(!less(a, c));
        BOOST_TEST(!less(a, d));
    }

    BOOST_AUTO_TEST_CASE(non_negative_signed_with_unsigned)
    {
        safe_integer<int, 0, 100> a = 1;
        safe_integer<unsigned> b = 2;
        safe_integer<unsigned> c = 1;
        safe_integer<unsigned> d = 0;

        BOOST_TEST(less(a, b));
        BOOST_TEST(!less(a, c));
        BOOST_TEST(!less(a, d));
    }

    BOOST_AUTO_TEST_CASE(unsigned_with_non_negative_signed)
    {
        safe_integer<unsigned> a = 1;
        safe_integer<int, 0, 100> b = 2;
        safe_integer<int, 0, 100> c = 1;
        safe_integer<int, 0, 100> d = 0;

        BOOST_TEST(less(a, b));
        BOOST_TEST(!less(a, c));
        BOOST_TEST(!less(a, d));
    }

    BOOST_AUTO_TEST_CASE(safe_integer_with_builtin)
    {
        safe_integer<int> a = 1;
        int b = 2;
        int c = 1;
        int d = 0;

        BOOST_TEST(less(a, b));
        BOOST_TEST(!less(a, c));
        BOOST_TEST(!less(a, d));
    }

    BOOST_AUTO_TEST_CASE(builtin_with_safe_integer)
    {
        int a = 1;
        safe_integer<int> b = 2;
        safe_integer<int> c = 1;
        safe_integer<int> d = 0;

        BOOST_TEST(less(a, b));
        BOOST_TEST(!less(a, c));
        BOOST_TEST(!less(a, d));
    }

    BOOST_AUTO_TEST_CASE(not_overlapped)
    {
        safe_integer<int, -10, -1> a = -1;
        safe_integer<int, 0, 10> b = 1;

        BOOST_TEST(less(a, b));
        BOOST_TEST(!less(b, a));
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int> constexpr a = 1;
        safe_integer<int> constexpr b = 2;
        safe_integer<int> constexpr c = 1;
        safe_integer<int> constexpr d = 0;

        static_assert(less(a, b));
        static_assert(!less(a, c));
        static_assert(!less(a, d));
    }

BOOST_AUTO_TEST_SUITE_END() // equal_
