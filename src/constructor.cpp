#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

namespace safe_integers::tests {

BOOST_AUTO_TEST_SUITE(constructor)

    BOOST_AUTO_TEST_CASE(default_)
    {
        safe_integer<int> a;

        BOOST_TEST(a == int());
    }

    BOOST_AUTO_TEST_CASE(from_value)
    {
        size_t const a = 2;
        safe_integer<size_t> sa { a };

        BOOST_TEST(sa == a);
    }

    BOOST_AUTO_TEST_CASE(by_deduction)
    {
        int const a = 1;
        safe_integer sa { a };

        static_assert(std::is_same_v<decltype(sa), safe_integer<int>>);

        BOOST_TEST(sa == a);
    }

    BOOST_AUTO_TEST_CASE(from_convertible_value)
    {
        long l = 1;
        safe_integer<int> si1 { l };
        BOOST_TEST(si1 == l);

        long long ll = 100;
        safe_integer<int> si2 { ll };
        BOOST_TEST(si2 == ll);

        char c = 3;
        safe_integer<int> si3 { c };
        BOOST_TEST(si3 == c);

        short s = 4;
        safe_integer<int> si4 { s };
        BOOST_TEST(si4 == s);

        uint64_t u64 = 5;
        safe_integer<int> si5 { u64 };
        BOOST_TEST(si5 == u64);
    }

    BOOST_AUTO_TEST_CASE(copy_construction)
    {
        safe_integer a { 1 };
        auto b = a;

        BOOST_TEST(a == b);
    }

    BOOST_AUTO_TEST_CASE(copy_from_different_safe_integer)
    {
        safe_integer<int64_t> a { 10 };

        safe_integer<int32_t> b { a };

        BOOST_TEST(a == b);
    }

    BOOST_AUTO_TEST_CASE(copy_from_different_safe_integer_2)
    {
        safe_integer<int> a { 100 };

        safe_integer<int, 0> b { a };

        BOOST_TEST(a == b);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        auto construct = []{
            return safe_integer<int> { INT64_MAX };
        };

        BOOST_CHECK_THROW(construct(), overflow_error);
    }

    BOOST_AUTO_TEST_CASE(overflow_bounded)
    {
        auto construct = []{
            return safe_integer<int, 0, 100> { 101 };
        };

        BOOST_CHECK_THROW(construct(), overflow_error);
    }

    BOOST_AUTO_TEST_CASE(overflow_from_safe_integer_1)
    {
        auto construct = []{
            safe_integer a = INT64_MAX;

            return safe_integer<int> { a };
        };

        BOOST_CHECK_THROW(construct(), overflow_error);
    }

    BOOST_AUTO_TEST_CASE(overflow_from_safe_integer_2)
    {
        auto construct = []{
            safe_integer a = 101;

            return safe_integer<int, 0, 100> { a };
        };

        BOOST_CHECK_THROW(construct(), overflow_error);
    }

    BOOST_AUTO_TEST_CASE(overflow_from_bounded_safe_integer)
    {
        auto construct = []{
            safe_integer<int, 0, 200>  a = 200;

            return safe_integer<int, 0, 100> { a };
        };

        BOOST_CHECK_THROW(construct(), overflow_error);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        auto construct = []{
            return safe_integer<int> { INT64_MIN };
        };

        BOOST_CHECK_THROW(construct(), underflow_error);
    }

    BOOST_AUTO_TEST_CASE(underflow_bounded)
    {
        auto construct = []{
            return safe_integer<int, 0, 100> { -1 };
        };

        BOOST_CHECK_THROW(construct(), underflow_error);
    }

    BOOST_AUTO_TEST_CASE(underflow_from_safe_integer_1)
    {
        auto construct = []{
            safe_integer a = INT64_MIN;

            return safe_integer<int> { a };
        };

        BOOST_CHECK_THROW(construct(), underflow_error);
    }

    BOOST_AUTO_TEST_CASE(underflow_from_safe_integer_2)
    {
        auto construct = []{
            safe_integer a = -10;

            return safe_integer<int, 0, 100> { a };
        };

        BOOST_CHECK_THROW(construct(), underflow_error);
    }

    BOOST_AUTO_TEST_CASE(underflow_from_bounded_safe_integer)
    {
        auto construct = []{
            safe_integer<int, -100, 100>  a = -10;

            return safe_integer<int, 0, 100> { a };
        };

        BOOST_CHECK_THROW(construct(), underflow_error);
    }

    BOOST_AUTO_TEST_CASE(literal_)
    {
        using namespace stream9::safe_integers::literals;

        auto si = 100_i;

        static_assert(std::same_as<decltype(si), safe_integer<unsigned long long int>>);
        BOOST_TEST(si == 100);
    }

    BOOST_AUTO_TEST_CASE(unoverlapped_safe_integer)
    {
#if 0
        // This has to be compile error
        safe_integer<int, 0, 100> a;
        safe_integer<int, 101, 200> b { a };
#endif
    }

BOOST_AUTO_TEST_SUITE_END() // constructor

} // namespace safe_integers::tests

#if 0
BOOST_AUTO_TEST_CASE(bug1)
{
    //TODO
    safe_integer<uint32_t, -2, -1> a = -1; // this must be compile error
}
#endif
