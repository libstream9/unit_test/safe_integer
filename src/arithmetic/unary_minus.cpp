#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(unary_minus)

    BOOST_AUTO_TEST_CASE(signed_)
    {
        safe_integer a = 1;

        BOOST_TEST(-a == -1);
    }

    BOOST_AUTO_TEST_CASE(unsigned_)
    {
        safe_integer a = 1u;

        BOOST_TEST(-a == -1);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        safe_integer<int32_t> a = INT32_MIN;

        BOOST_CHECK_THROW(-a, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        safe_integer<uint64_t> a = UINT64_MAX;

        BOOST_CHECK_THROW(-a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer constexpr a = 1;
        safe_integer constexpr b = -1;

        auto constexpr c = -a;

        BOOST_TEST(c == b);
    }

    template<typename T>
    using result_t = decltype(-T());

    template<typename T>
    using safe_integer_result_t =
        typename result_t<safe_integer<T>>::value_type;

    template<typename T>
    using builtin_result_t = result_t<T>;

    template<typename T>
    auto constexpr compare_result_v = std::is_same_v<
            safe_integer_result_t<T>,
            builtin_result_t<T>
        >;

    BOOST_AUTO_TEST_CASE(same_result_type_as_builtin_type_arithmetic)
    {
        static_assert(compare_result_v<bool>);
        static_assert(compare_result_v<char>);
        static_assert(compare_result_v<short>);
        static_assert(compare_result_v<int>);
        static_assert(compare_result_v<long>);
        static_assert(compare_result_v<long long>);
        static_assert(compare_result_v<unsigned char>);
        static_assert(compare_result_v<unsigned short>);
        //static_assert(compare_result_v<unsigned int>);
        //static_assert(compare_result_v<unsigned long>);
        //static_assert(compare_result_v<unsigned long long>);
    }

BOOST_AUTO_TEST_SUITE_END() // unary_minus
