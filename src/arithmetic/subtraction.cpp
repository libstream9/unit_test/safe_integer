#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(subtraction)

BOOST_AUTO_TEST_SUITE(with_same_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 3;
        safe_integer b = 2;

        BOOST_TEST(a - b == 1);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        using integer = safe_integer<int32_t, -5, 5>;

        integer a = -1;
        integer b = 3;

        BOOST_TEST(a - b == -4);
        BOOST_TEST(b - a == 4);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = integer::max() - 1;
        integer b = -1;
        integer c = -2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = integer::min() + 1;
        integer b = 1;
        integer c = 2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        using integer = safe_integer<int32_t>;

        integer a = 1;
        integer b = 2;

        static_assert(std::is_same_v<decltype(a - b), integer>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        using integer = safe_integer<int32_t, -5, 5>;

        integer a = -1;
        integer b = 3;

        static_assert(std::is_same_v<
            decltype(a - b), safe_integer<int32_t, -10, 10>>);
        static_assert(std::is_same_v<
            decltype(b - a), safe_integer<int32_t, -10, 10>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto const int32_min = std::numeric_limits<int32_t>::min();
        auto const int32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t, -5, 10> a = 7;
        safe_integer<int32_t, int32_min, 10> b = 7;
        safe_integer<int32_t, -10, int32_max> c = -1;

        static_assert(std::is_same_v<
            decltype(a - b), safe_integer<int32_t, -15, int32_max> >);
        static_assert(std::is_same_v<
            decltype(b - a), safe_integer<int32_t, int32_min, 15> >);

        static_assert(std::is_same_v<
            decltype(b - b), safe_integer<int32_t>>);
        static_assert(std::is_same_v<
            decltype(c - c), safe_integer<int32_t>>);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer       a = 1;
        safe_integer const b = 2;

        BOOST_TEST(a - b == -1);
        BOOST_TEST(b - a == 1);
        BOOST_TEST(b - b == 0);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer constexpr a = 1;
        safe_integer constexpr b = 2;

        static_assert(a - b == -1);
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_safe_integer

BOOST_AUTO_TEST_SUITE(with_different_size_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 3;
        safe_integer<int64_t> b = 2;

        BOOST_TEST(a - b == 1);
        BOOST_TEST(b - a == -1);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        safe_integer<int64_t, -5, 5> b = 3;

        BOOST_TEST(a - b == -4);
        BOOST_TEST(b - a == 4);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int64_t> a = decltype(a)::max() - 1;
        safe_integer<int32_t> b = -1;
        safe_integer<int32_t> c = -2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int64_t> a = decltype(a)::min() + 1;
        safe_integer<int32_t> b = 1;
        safe_integer<int32_t> c = 2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        static_assert(std::is_same_v<decltype(a - b), safe_integer<int64_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<int32_t, -1, 2> a = -1;
        safe_integer<int64_t, -5, 5> b = 3;

        static_assert(std::is_same_v<
            decltype(a - b), safe_integer<int64_t, -6, 7>>);
        static_assert(std::is_same_v<
            decltype(b - a), safe_integer<int64_t, -7, 6>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto constexpr int64_min = std::numeric_limits<int64_t>::min();
        auto constexpr int64_max = std::numeric_limits<int64_t>::max();

        safe_integer<int64_t, -10, int64_max - 1L> a = -1;
        safe_integer<int32_t, -5, 10> b = 7;

        static_assert(std::is_same_v<
            decltype(a - b), safe_integer<int64_t, -20, int64_max> >);
        static_assert(std::is_same_v<
            decltype(b - a), safe_integer<int64_t, int64_min, 20> >);
        static_assert(std::is_same_v<
            decltype(a - a), safe_integer<int64_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer       a = 1;
        safe_integer const b = 2;

        BOOST_TEST(a - a == 0);
        BOOST_TEST(a - b == -1);
        BOOST_TEST(b - a == 1);
        BOOST_TEST(b - b == 0);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<int64_t> constexpr b = 2;

        static_assert(a - b == -1);
        static_assert(b - a == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_size_safe_integer

BOOST_AUTO_TEST_SUITE(with_different_sign_safe_integer)

    BOOST_AUTO_TEST_CASE(positive_positive)
    {
        safe_integer<int32_t> a = 3;
        safe_integer<uint32_t> b = 3;

        BOOST_TEST(a - b == 0);
        BOOST_TEST(b - a == 0);
    }

    BOOST_AUTO_TEST_CASE(negative_positive)
    {
        safe_integer<uint32_t> a = 3;
        safe_integer<int32_t> b = -3;

        BOOST_TEST(a - b == 6);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = 3;
        safe_integer<uint32_t, 0, 5> b = 3;

        BOOST_TEST(a - b == 0);
        BOOST_TEST(b - a == 0);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<uint32_t> a = decltype(a)::max() - 1;
        safe_integer<int32_t> b = -1;
        safe_integer<int32_t> c = -2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<uint32_t> a = decltype(a)::min() + 1;
        safe_integer<int32_t> b = 1;
        safe_integer<int32_t> c = 2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<uint32_t> a = 2;
        safe_integer<int32_t> b = 1;

        static_assert(std::is_same_v<decltype(a - b), safe_integer<uint32_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<int32_t, 2, 5> a = 3;
        safe_integer<uint32_t, 0, 2> b = 1;

        static_assert(std::is_same_v<
            decltype(a - b), safe_integer<uint32_t, 0, 5>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto constexpr uint32_min = std::numeric_limits<uint32_t>::min();
        auto constexpr uint32_max = std::numeric_limits<uint32_t>::max();

        safe_integer<uint32_t, 20, uint32_max - 1u> a = 20;
        safe_integer<int32_t, -5, 15> b = 7;
        safe_integer<uint32_t, 1, 2> c = 1;
        safe_integer<int32_t, -5, 30> d = 2;

        static_assert(std::is_same_v<
            decltype(a - b), safe_integer<uint32_t, 5, uint32_max> >);
        static_assert(std::is_same_v<
            decltype(b - c), safe_integer<uint32_t, uint32_min, 14> >);
        static_assert(std::is_same_v<
            decltype(a - d), safe_integer<uint32_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<uint32_t>       a = 0;
        safe_integer<uint32_t> const b = 0;
        safe_integer<int32_t>        c = 0;
        safe_integer<int32_t> const  d = 0;

        BOOST_TEST(a - d == 0);
        BOOST_TEST(b - c == 0);
        BOOST_TEST(b - d == 0);

        BOOST_TEST(d - a == 0);
        BOOST_TEST(c - b == 0);
        BOOST_TEST(d - b == 0);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<int64_t> constexpr b = 2;

        static_assert(a - b == -1);
        static_assert(b - a == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_safe_integer

BOOST_AUTO_TEST_SUITE(with_same_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 3;
        int b = 2;

        BOOST_TEST(a - b == 1);
        BOOST_TEST(b - a == -1);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        int32_t b = 3;

        BOOST_TEST(a - b == -4);
        BOOST_TEST(b - a == 4);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = integer::max() - 1;
        int32_t b = -1;
        int32_t c = -2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, overflow_error);

        int32_t d = std::numeric_limits<decltype(d)>::max() - 1;
        integer e = -1;
        integer f = -2;

        BOOST_CHECK_NO_THROW(d - e);
        BOOST_CHECK_THROW(d - f, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = integer::min() + 1;
        int32_t b = 1;
        int32_t c = 2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, underflow_error);

        int32_t d = std::numeric_limits<decltype(d)>::min() + 1;
        integer e = 1;
        integer f = 2;

        BOOST_CHECK_NO_THROW(d - e);
        BOOST_CHECK_THROW(d - f, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        using integer = safe_integer<int32_t>;

        integer a = 1;
        int32_t b = 2;

        static_assert(std::is_same_v<decltype(a - b), integer>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        int32_t b = 3;

        static_assert(std::is_same_v<
            decltype(a - b), safe_integer<int32_t> >);
        static_assert(std::is_same_v<
            decltype(b - a), safe_integer<int32_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<int> a = 1;
        int const         b = 2;

        BOOST_TEST(a - b == -1);
        BOOST_TEST(b - a == 1);

        safe_integer<int> const c = 1;
        int                     d = 2;

        BOOST_TEST(c - d == -1);
        BOOST_TEST(d - a == 1);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        int32_t constexpr b = 2;

        static_assert(a - b == -1);
        static_assert(b - a == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_builtin_type

BOOST_AUTO_TEST_SUITE(with_different_size_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 3;
        int64_t b = 2;

        BOOST_TEST(a - b == 1);

        int32_t d = 3;
        safe_integer<int64_t> e = 2;

        BOOST_TEST(d - e == 1);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        int64_t b = 3;

        BOOST_TEST(a - b == -4);
        BOOST_TEST(b - a == 4);

        safe_integer<int64_t, -5, 5> c = -1;
        int32_t d = 3;

        BOOST_TEST(c - d == -4);
        BOOST_TEST(d - c == 4);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int64_t> a = decltype(a)::max() - 1;
        int32_t b = -1;
        int32_t c = -2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, overflow_error);

        int64_t d = std::numeric_limits<decltype(d)>::max() - 1;
        safe_integer<int32_t> e = -1;
        safe_integer<int32_t> f = -2;

        BOOST_CHECK_NO_THROW(d - e);
        BOOST_CHECK_THROW(d - f, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int64_t> a = decltype(a)::min() + 1;
        int32_t b = 1;
        int32_t c = 2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, underflow_error);

        int64_t d = std::numeric_limits<decltype(d)>::min() + 1;
        safe_integer<int32_t> e = 1;
        safe_integer<int32_t> f = 2;

        BOOST_CHECK_NO_THROW(d - e);
        BOOST_CHECK_THROW(d - f, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        int64_t b = 2;

        static_assert(std::is_same_v<decltype(a - b), safe_integer<int64_t>>);
        static_assert(std::is_same_v<decltype(b - a), safe_integer<int64_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        auto constexpr int32_min = std::numeric_limits<int32_t>::min();
        auto constexpr int32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int64_t, -1, 2> a = -1;
        int32_t b = 3;

        static_assert(std::is_same_v<
            decltype(a - b),
            safe_integer<int64_t, -1L - int32_max, 2L - int32_min> >);
        static_assert(std::is_same_v<
            decltype(b - a),
            safe_integer<int64_t, int32_min -2L, int32_max + 1L> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto constexpr int32_max = std::numeric_limits<int32_t>::max();
        auto constexpr int64_min = std::numeric_limits<int64_t>::min();
        auto constexpr int64_max = std::numeric_limits<int64_t>::max();

        safe_integer<int64_t, -10, int64_max - 1L> a = -1;
        int32_t b = 7;
        safe_integer<int64_t, int64_min + 5L, int64_max - 1L> c = 5;

        static_assert(std::is_same_v<
            decltype(a - b),
            safe_integer<int64_t, -10L - int32_max, int64_max> >);
        static_assert(std::is_same_v<
            decltype(b - a),
            safe_integer<int64_t, int64_min, int32_max - -10L> >);
        static_assert(std::is_same_v<
            decltype(b - c), safe_integer<int64_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<int32_t> const a = 1;
        safe_integer<int32_t>       b = 1;
        int64_t const c = 2;
        int64_t       d = 2;

        BOOST_TEST(a - c == -1);
        BOOST_TEST(c - a == 1);
        BOOST_TEST(a - d == -1);
        BOOST_TEST(d - a == 1);
        BOOST_TEST(b - c == -1);
        BOOST_TEST(c - b == 1);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        int64_t constexpr b = 2;

        static_assert(a - b == -1);
        static_assert(b - a == 1);
    }


BOOST_AUTO_TEST_SUITE_END() // with_different_size_builtin_type

BOOST_AUTO_TEST_SUITE(with_different_sign_builtin_type)

    BOOST_AUTO_TEST_CASE(positive_positive)
    {
        safe_integer<int32_t> a = 2;
        uint32_t b = 2;

        BOOST_TEST(a - b == 0);
        BOOST_TEST(b - a == 0);

        int32_t c = 2;
        safe_integer<uint32_t> d = 2;

        BOOST_TEST(c - d == 0);
        BOOST_TEST(d - c == 0);
    }

    BOOST_AUTO_TEST_CASE(negative_positive)
    {
        uint32_t a = 2;
        safe_integer<int32_t> b = -2;

        BOOST_TEST(a - b == 4);

        safe_integer<uint32_t> c = 2;
        int32_t d = -2;

        BOOST_TEST(c - d == 4);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = 3;
        uint32_t b = 3;

        BOOST_TEST(a - b == 0);
        BOOST_TEST(b - a == 0);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<uint32_t> a = decltype(a)::max() - 1;
        int32_t b = -1;
        int32_t c = -2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<uint32_t> a = 1;
        int32_t b = 1;
        int32_t c = 2;

        BOOST_CHECK_NO_THROW(a - b);
        BOOST_CHECK_THROW(a - c, underflow_error);

        int32_t d = 1;
        safe_integer<uint32_t> e = 1;
        safe_integer<uint32_t> f = 2;

        BOOST_CHECK_NO_THROW(d - e);
        BOOST_CHECK_THROW(d - f, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        auto constexpr int32_max = std::numeric_limits<int32_t>::max();

        safe_integer<uint32_t> a = 1;
        int32_t b = 1;

        static_assert(std::is_same_v<decltype(a - b), safe_integer<uint32_t>>);
        static_assert(std::is_same_v<
                decltype(b - a), safe_integer<uint32_t, 0, int32_max> >);

        safe_integer<int32_t> c = 2;
        uint32_t d = 1;

        static_assert(std::is_same_v<
                decltype(c - d), safe_integer<uint32_t, 0, int32_max> >);
        static_assert(std::is_same_v<decltype(d - c), safe_integer<uint32_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<int32_t, 2, 5> a = 3;
        uint32_t b = 1;

        static_assert(std::is_same_v<
            decltype(a - b), safe_integer<uint32_t, 0, 5> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto constexpr uint32_max = std::numeric_limits<uint32_t>::max();

        safe_integer<uint32_t, 2, uint32_max - 1u> a = 3;
        int32_t b = 1;

        static_assert(std::is_same_v<
            decltype(a - b), safe_integer<uint32_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<uint32_t>       a = 0;
        safe_integer<uint32_t> const b = 0;
        int32_t        c = 0;
        int32_t const  d = 0;

        BOOST_TEST(a - d == 0);
        BOOST_TEST(b - c == 0);
        BOOST_TEST(b - d == 0);

        BOOST_TEST(d - a == 0);
        BOOST_TEST(c - b == 0);
        BOOST_TEST(d - b == 0);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 3;
        uint32_t constexpr b = 2;
        safe_integer<int32_t> constexpr c = 1;

        static_assert(a - b == 1);
        static_assert(b - c == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_builtin_type

template<typename T, typename U>
using result_t = decltype(std::declval<T>() - std::declval<U>());

template<typename T, typename U>
using safe_integer_result_t =
    typename result_t<safe_integer<T>, safe_integer<U>>::value_type;

template<typename T, typename U>
using builtin_result_t = result_t<T, U>;

template<typename T, typename U>
auto constexpr compare_result_v = std::is_same_v<
        safe_integer_result_t<T, U>,
        builtin_result_t<T, U>
    >;

BOOST_AUTO_TEST_CASE(same_arithmetic_type_conversion_as_builtin_types)
{
    static_assert(compare_result_v<bool, bool>);
    static_assert(compare_result_v<char, char>);
    static_assert(compare_result_v<short, short>);
    static_assert(compare_result_v<int, int>);
    static_assert(compare_result_v<int, long>);
    static_assert(compare_result_v<int, long long>);
    static_assert(compare_result_v<int, unsigned int>);
    static_assert(compare_result_v<int, unsigned long>);
    static_assert(compare_result_v<int, unsigned long long>);
    static_assert(compare_result_v<long, long>);
    static_assert(compare_result_v<long, long long>);
    static_assert(compare_result_v<long, unsigned int>);
    static_assert(compare_result_v<long, unsigned long>);
    static_assert(compare_result_v<long, unsigned long long>);
    static_assert(compare_result_v<long long, long long>);
    static_assert(compare_result_v<long long, unsigned int>);
    static_assert(compare_result_v<long long, unsigned long>);
    static_assert(compare_result_v<long long, unsigned long long>);
    static_assert(compare_result_v<unsigned int, unsigned int>);
    static_assert(compare_result_v<unsigned int, unsigned long>);
    static_assert(compare_result_v<unsigned int, unsigned long long>);
    static_assert(compare_result_v<unsigned long, unsigned long>);
    static_assert(compare_result_v<unsigned long, unsigned long long>);
    static_assert(compare_result_v<unsigned long long, unsigned long long>);
}

BOOST_AUTO_TEST_SUITE_END() // subtraction
