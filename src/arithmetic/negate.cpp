#include <boost/test/unit_test.hpp>

#include <type_traits>

#include <stream9/safe_integer/safe_integer.hpp>
#include <stream9/safe_integer/arithmetic/negate.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic)

BOOST_AUTO_TEST_SUITE(negate_)

    BOOST_AUTO_TEST_CASE(signed_)
    {
        safe_integer<int32_t> a = 100;

        auto oc = negate(a);

        static_assert(std::is_same_v<
                decltype(oc),
                arithmetic_outcome<
                    safe_integer<int32_t, -INT32_MAX, INT32_MAX> > >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -100);
    }

    BOOST_AUTO_TEST_CASE(unsigned_)
    {
        safe_integer<uint32_t> a = 100u;

        auto oc = negate(a);

        auto constexpr min = -static_cast<int64_t>(UINT32_MAX);

        static_assert(std::is_same_v<
                decltype(oc),
                arithmetic_outcome<
                    safe_integer<int64_t, min, 0> > >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -100);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        if constexpr (is_twos_complement_integer_v<>) {
            safe_integer<int32_t> a = INT32_MIN;

            auto oc = negate(a);

            BOOST_TEST(!oc);
            BOOST_TEST(oc.error() == arithmetic_errc::overflow);
        }
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        safe_integer<uint64_t> a = UINT64_MAX;

        auto oc = negate(a);

        BOOST_TEST(!oc);
        BOOST_TEST(oc.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 100;

        auto constexpr oc = negate(a);

        static_assert(oc);
        static_assert(*oc == -100);
    }

    BOOST_AUTO_TEST_SUITE(bound)

        BOOST_AUTO_TEST_CASE(pos_pos)
        {
            safe_integer<int, 10, 20> a;

            using result_t = typename decltype(negate(a))::value_type;

            static_assert(result_t::min() == -20);
            static_assert(result_t::max() == -10);
        }

        BOOST_AUTO_TEST_CASE(neg_pos)
        {
            safe_integer<int, -10, 20> a;

            using result_t = typename decltype(negate(a))::value_type;

            static_assert(result_t::min() == -20);
            static_assert(result_t::max() == 10);
        }

        BOOST_AUTO_TEST_CASE(neg_neg)
        {
            safe_integer<int, -20, -10> a;

            using result_t = typename decltype(negate(a))::value_type;

            static_assert(result_t::min() == 10);
            static_assert(result_t::max() == 20);
        }

    BOOST_AUTO_TEST_SUITE_END() // bound

BOOST_AUTO_TEST_SUITE_END() // negate_

BOOST_AUTO_TEST_SUITE_END() // arithmetic
