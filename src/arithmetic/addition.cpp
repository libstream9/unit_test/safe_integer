#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(addition)

BOOST_AUTO_TEST_SUITE(with_same_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 1;
        safe_integer b = 2;
        safe_integer c = 3;

        BOOST_TEST(a + b == c);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        using integer = safe_integer<int32_t, -5, 5>;

        integer a = -1;
        integer b = 3;

        BOOST_TEST(a + b == 2);
        BOOST_TEST(b + a == 2);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = 0;
        integer b = 1;
        integer c = integer::max();

        BOOST_CHECK_NO_THROW(a + c);
        BOOST_CHECK_THROW(b + c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = 0;
        integer b = -1;
        integer c = integer::min();

        BOOST_CHECK_NO_THROW(a + c);
        BOOST_CHECK_THROW(b + c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        using integer = safe_integer<int32_t>;

        integer a = 1;
        integer b = 2;

        static_assert(std::is_same_v<decltype(a + b), integer>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        using integer = safe_integer<int32_t, -5, 5>;

        integer a = -1;
        integer b = 3;

        static_assert(std::is_same_v<
            decltype(a + b), safe_integer<int32_t, -10, 10>>);
        static_assert(std::is_same_v<
            decltype(b + a), safe_integer<int32_t, -10, 10>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto const int32_min = std::numeric_limits<int32_t>::min();
        auto const int32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t, -10, int32_max> a = -1;
        safe_integer<int32_t, -5, 10> b = 7;
        safe_integer<int32_t, int32_min, 10> c = 7;

        static_assert(std::is_same_v<
            decltype(a + b), safe_integer<int32_t, -15>>);
        static_assert(std::is_same_v<
            decltype(b + a), safe_integer<int32_t, -15>>);

        static_assert(std::is_same_v<
            decltype(b + c), safe_integer<int32_t, int32_min, 20>>);
        static_assert(std::is_same_v<
            decltype(c + b), safe_integer<int32_t, int32_min, 20>>);

        static_assert(std::is_same_v<
            decltype(a + c), safe_integer<int32_t>>);
        static_assert(std::is_same_v<
            decltype(c + a), safe_integer<int32_t>>);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer       a = 1;
        safe_integer const b = 2;

        BOOST_TEST(a + b == 3);
        BOOST_TEST(b + a == 3);
        BOOST_TEST(b + b == 4);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer constexpr a = 1;
        safe_integer constexpr b = 2;

        static_assert(a + b == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_safe_integer

BOOST_AUTO_TEST_SUITE(with_different_size_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        BOOST_TEST(a + b == 3);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        safe_integer<int64_t, -5, 5> b = 3;

        BOOST_TEST(a + b == 2);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int64_t> a = decltype(a)::max() - 1;
        safe_integer<int32_t> b = 1;
        safe_integer<int32_t> c = 2;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int64_t> a = decltype(a)::min() + 1;
        safe_integer<int32_t> b = -1;
        safe_integer<int32_t> c = -2;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        static_assert(std::is_same_v<decltype(a + b), safe_integer<int64_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        safe_integer<int64_t, -5, 5> b = 3;

        static_assert(std::is_same_v<
            decltype(a + b), safe_integer<int64_t, -10, 10>>);
        static_assert(std::is_same_v<
            decltype(b + a), safe_integer<int64_t, -10, 10>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto constexpr int64_min = std::numeric_limits<int64_t>::min();
        auto constexpr int64_max = std::numeric_limits<int64_t>::max();

        safe_integer<int64_t, -10, int64_max> a = -1;
        safe_integer<int32_t, -5, 10> b = 7;
        safe_integer<int64_t, int64_min, 10> c = 7;

        static_assert(std::is_same_v<
            decltype(a + b), safe_integer<int64_t, -15>>);
        static_assert(std::is_same_v<
            decltype(b + a), safe_integer<int64_t, -15>>);

        static_assert(std::is_same_v<
            decltype(b + c), safe_integer<int64_t, int64_min, 20>>);
        static_assert(std::is_same_v<
            decltype(c + b), safe_integer<int64_t, int64_min, 20>>);

        static_assert(std::is_same_v<
            decltype(a + c), safe_integer<int64_t>>);
        static_assert(std::is_same_v<
            decltype(c + a), safe_integer<int64_t>>);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<int32_t>       a = 1;
        safe_integer<int64_t> const b = 2;

        BOOST_TEST(a + b == 3);
        BOOST_TEST(b + a == 3);
        BOOST_TEST(b + b == 4);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<int64_t> constexpr b = 2;

        static_assert(a + b == 3);
        static_assert(b + a == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_size_safe_integer

BOOST_AUTO_TEST_SUITE(with_different_sign_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<uint32_t> b = 2;

        BOOST_TEST(a + b == 3);
        BOOST_TEST(b + a == 3);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        safe_integer<uint32_t, 5, 10> b = 7;

        BOOST_TEST(a + b == 6);
        BOOST_TEST(b + a == 6);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_overflow_1)
    {
        safe_integer<int64_t> a = decltype(a)::max() - 1;
        safe_integer<uint32_t> b = 1;
        safe_integer<uint32_t> c = 2;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, overflow_error);

        BOOST_CHECK_NO_THROW(b + a);
        BOOST_CHECK_THROW(c + a, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_overflow_2)
    {
        safe_integer<uint32_t> a = decltype(a)::max() - 1;
        safe_integer<int32_t> b = 1;
        safe_integer<int32_t> c = 2;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, overflow_error);

        BOOST_CHECK_NO_THROW(b + a);
        BOOST_CHECK_THROW(c + a, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_underflow_1)
    {
        safe_integer<int32_t> a = -2;
        safe_integer<uint32_t> b = 2;
        safe_integer<uint32_t> c = 1;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, underflow_error);

        BOOST_CHECK_NO_THROW(b + a);
        BOOST_CHECK_THROW(c + a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_underflow_2)
    {
        safe_integer<uint32_t> a = 1;
        safe_integer<int32_t> b = -1;
        safe_integer<int32_t> c = -2;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, underflow_error);

        BOOST_CHECK_NO_THROW(b + a);
        BOOST_CHECK_THROW(c + a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(no_conversion_error_unlike_builtin_arithmetic)
    {
        safe_integer<uint32_t> a = 1;
        safe_integer<int32_t> b = -1;

        static_assert(std::is_same_v<decltype(a + b)::value_type, uint32_t>);

        BOOST_TEST(a + b == 0);
        BOOST_TEST(b + a == 0);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<uint32_t> a = 1;
        safe_integer<int32_t> b = 2;

        static_assert(std::is_same_v<decltype(a + b), safe_integer<uint32_t>>);
        static_assert(std::is_same_v<decltype(b + a), safe_integer<uint32_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_normal)
    {
        safe_integer<int32_t, -10, 10> a = -1;
        safe_integer<uint32_t, 5, 10> b = 7;

        static_assert(std::is_same_v<
            decltype(a + b), safe_integer<uint32_t, 0, 20>>);
        static_assert(std::is_same_v<
            decltype(b + a), safe_integer<uint32_t, 0, 20>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto const int32_max = std::numeric_limits<int32_t>::max();
        auto const uint32_max = std::numeric_limits<uint32_t>::max();

        safe_integer<int32_t, -10, int32_max> a = -1;
        safe_integer<uint32_t, 5, 10> b = 7;
        safe_integer<uint32_t, 5, uint32_max> c = 7;

        static_assert(std::is_same_v<
            decltype(a + b), safe_integer<uint32_t, 0, int32_max + 10u>>);
        static_assert(std::is_same_v<
            decltype(b + a), safe_integer<uint32_t, 0, int32_max + 10u>>);

        static_assert(std::is_same_v<
            decltype(a + c), safe_integer<uint32_t>>);
        static_assert(std::is_same_v<
            decltype(c + a), safe_integer<uint32_t>>);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<uint32_t>       a = 1;
        safe_integer<uint32_t> const b = 2;
        safe_integer<int32_t>        c = 3;
        safe_integer<int32_t> const  d = 4;

        BOOST_TEST(a + d == 5);
        BOOST_TEST(b + c == 5);
        BOOST_TEST(b + d == 6);

        BOOST_TEST(d + a == 5);
        BOOST_TEST(c + b == 5);
        BOOST_TEST(d + b == 6);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<uint32_t> constexpr b = 2;

        static_assert(a + b == 3);
        static_assert(b + a == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_safe_integer

BOOST_AUTO_TEST_SUITE(with_same_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 1;
        int b = 2;
        safe_integer c = 3;

        BOOST_TEST(a + b == c);
        BOOST_TEST(b + a == c);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        int32_t b = 3;

        BOOST_TEST(a + b == 2);
        BOOST_TEST(b + a == 2);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int32_t> a = 0;
        safe_integer<int32_t> b = 1;
        int32_t c = std::numeric_limits<int32_t>::max();

        BOOST_CHECK_NO_THROW(a + c);
        BOOST_CHECK_THROW(b + c, overflow_error);

        BOOST_CHECK_NO_THROW(c + a);
        BOOST_CHECK_THROW(c + b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int32_t> a = 0;
        safe_integer<int32_t> b = -1;
        int32_t c = std::numeric_limits<int32_t>::min();

        BOOST_CHECK_NO_THROW(a + c);
        BOOST_CHECK_THROW(b + c, underflow_error);

        BOOST_CHECK_NO_THROW(c + a);
        BOOST_CHECK_THROW(c + b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        int32_t b = 2;

        static_assert(std::is_same_v<decltype(a + b), safe_integer<int32_t>>);
        static_assert(std::is_same_v<decltype(b + a), safe_integer<int32_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        int32_t b = 3;

        static_assert(std::is_same_v<
            decltype(a + b), safe_integer<int32_t>>);
        static_assert(std::is_same_v<
            decltype(b + a), safe_integer<int32_t>>);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer       a = 1;
        safe_integer const b = 2;
        int       c = 3;
        int const d = 4;

        BOOST_TEST(a + d == 5);
        BOOST_TEST(b + c == 5);
        BOOST_TEST(b + d == 6);

        BOOST_TEST(d + a == 5);
        BOOST_TEST(c + b == 5);
        BOOST_TEST(d + b == 6);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        int32_t constexpr b = 2;

        static_assert(a + b == 3);
        static_assert(b + a == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_builtin_type

BOOST_AUTO_TEST_SUITE(with_different_size_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 1;
        int64_t b = 2;
        safe_integer<int32_t> c = 3;

        BOOST_TEST(a + b == c);
        BOOST_TEST(b + a == c);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        int64_t b = 3;

        BOOST_TEST(a + b == 2);
        BOOST_TEST(b + a == 2);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int64_t> a = decltype(a)::max() - 1;
        int32_t b = 1;
        int32_t c = 2;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, overflow_error);

        BOOST_CHECK_NO_THROW(b + a);
        BOOST_CHECK_THROW(c + a, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int64_t> a = decltype(a)::min() + 1;
        int32_t b = -1;
        int32_t c = -2;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, underflow_error);

        BOOST_CHECK_NO_THROW(b + a);
        BOOST_CHECK_THROW(c + a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int64_t> a = 1;
        int32_t b = 2;

        static_assert(std::is_same_v<decltype(a + b), safe_integer<int64_t>>);
        static_assert(std::is_same_v<decltype(b + a), safe_integer<int64_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        auto constexpr int32_min = std::numeric_limits<int32_t>::min();
        auto constexpr int32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int64_t, -5, 5> a = -1;
        int32_t b = 3;

        static_assert(std::is_same_v<
            decltype(a + b),
            safe_integer<int64_t, int32_min - 5L, int32_max + 5L> >);
        static_assert(std::is_same_v<
            decltype(b + a),
            safe_integer<int64_t, int32_min - 5L, int32_max + 5L> >);
    }

    template<typename> class typeof;
    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto constexpr int32_min = std::numeric_limits<int32_t>::min();
        auto constexpr int32_max = std::numeric_limits<int32_t>::max();
        auto constexpr int64_min = std::numeric_limits<int64_t>::min();
        auto constexpr int64_max = std::numeric_limits<int64_t>::max();

        safe_integer<int64_t, -10, int64_max> a = -1;
        int32_t b = 7;
        safe_integer<int64_t, int64_min, 10> c = 7;

        static_assert(std::is_same_v<
            decltype(a + b), safe_integer<int64_t, -10L + int32_min>>);
        static_assert(std::is_same_v<
            decltype(b + a), safe_integer<int64_t, -10L + int32_min>>);

        static_assert(std::is_same_v<
            decltype(b + c),
            safe_integer<int64_t, int64_min, int32_max + 10L> >);
        static_assert(std::is_same_v<
            decltype(c + b),
            safe_integer<int64_t, int64_min, int32_max + 10L> >);

        static_assert(std::is_same_v<
            decltype(a + c), safe_integer<int64_t>>);
        static_assert(std::is_same_v<
            decltype(c + a), safe_integer<int64_t>>);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<int64_t>       a = 1;
        safe_integer<int64_t> const b = 2;
        int32_t       c = 3;
        int32_t const d = 4;

        BOOST_TEST(a + d == 5);
        BOOST_TEST(b + c == 5);
        BOOST_TEST(b + d == 6);

        BOOST_TEST(d + a == 5);
        BOOST_TEST(c + b == 5);
        BOOST_TEST(d + b == 6);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        int64_t constexpr b = 2;

        static_assert(a + b == 3);
        static_assert(b + a == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_size_builtin_type

BOOST_AUTO_TEST_SUITE(with_different_sign_builtin_type)

    BOOST_AUTO_TEST_CASE(normal_1)
    {
        safe_integer<int32_t> a = 1;
        uint32_t b = 2;

        BOOST_TEST(a + b == 3);
        BOOST_TEST(b + a == 3);
    }

    BOOST_AUTO_TEST_CASE(normal_2)
    {
        safe_integer<uint32_t> a = 1;
        int32_t b = 2;

        BOOST_TEST(a + b == 3);
        BOOST_TEST(b + a == 3);
    }

    BOOST_AUTO_TEST_CASE(bounded_1)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        uint32_t b = 7;

        BOOST_TEST(a + b == 6);
        BOOST_TEST(b + a == 6);
    }

    BOOST_AUTO_TEST_CASE(bounded_2)
    {
        safe_integer<uint32_t, 0, 5> a = 3;
        int32_t b = 7;

        BOOST_TEST(a + b == 10);
        BOOST_TEST(b + a == 10);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_overflow_1)
    {
        safe_integer<int64_t> a = decltype(a)::max() - 1;
        uint32_t b = 1;
        uint32_t c = 2;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, overflow_error);

        BOOST_CHECK_NO_THROW(b + a);
        BOOST_CHECK_THROW(c + a, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_overflow_2)
    {
        safe_integer<uint32_t> a = decltype(a)::max() - 1;
        int32_t b = 1;
        int32_t c = 2;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, overflow_error);

        BOOST_CHECK_NO_THROW(b + a);
        BOOST_CHECK_THROW(c + a, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_underflow_1)
    {
        safe_integer<int32_t> a = -2;
        uint32_t b = 2;
        uint32_t c = 1;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, underflow_error);

        BOOST_CHECK_NO_THROW(b + a);
        BOOST_CHECK_THROW(c + a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_underflow_2)
    {
        safe_integer<uint32_t> a = 1;
        int32_t b = -1;
        int32_t c = -2;

        BOOST_CHECK_NO_THROW(a + b);
        BOOST_CHECK_THROW(a + c, underflow_error);

        BOOST_CHECK_NO_THROW(b + a);
        BOOST_CHECK_THROW(c + a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(no_conversion_error_unlike_builtin_arithmetic)
    {
        safe_integer<uint32_t> a = 1;
        int32_t b = -1;

        static_assert(std::is_same_v<decltype(a + b)::value_type, uint32_t>);

        BOOST_TEST(a + b == 0);
        BOOST_TEST(b + a == 0);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<uint32_t> a = 1;
        int32_t b = 2;

        static_assert(std::is_same_v<decltype(a + b), safe_integer<uint32_t>>);
        static_assert(std::is_same_v<decltype(b + a), safe_integer<uint32_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_normal)
    {
        auto constexpr uint32_min = std::numeric_limits<uint32_t>::min();
        auto constexpr uint32_max = std::numeric_limits<uint32_t>::max();

        safe_integer<int64_t, -10, 10> a = -1;
        uint32_t b = 7;

        static_assert(std::is_same_v<
            decltype(a + b),
            safe_integer<int64_t, -10L + uint32_min, 10L + uint32_max> >);
        static_assert(std::is_same_v<
            decltype(b + a),
            safe_integer<int64_t, -10L + uint32_min, 10L + uint32_max> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto const int32_max = std::numeric_limits<int32_t>::max();
        auto const uint32_max = std::numeric_limits<uint32_t>::max();

        safe_integer<uint32_t, 2, 10> a = 5;
        int32_t b = 7;
        safe_integer<uint32_t, 2, uint32_max> c = 5;

        static_assert(std::is_same_v<
            decltype(a + b), safe_integer<uint32_t, 0, int32_max + 10u>>);
        static_assert(std::is_same_v<
            decltype(b + a), safe_integer<uint32_t, 0, int32_max + 10u>>);

        static_assert(std::is_same_v<
            decltype(b + c), safe_integer<uint32_t>>);
        static_assert(std::is_same_v<
            decltype(c + b), safe_integer<uint32_t>>);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<uint32_t>       a = 1;
        safe_integer<uint32_t> const b = 2;
        int32_t       c = 3;
        int32_t const d = 4;

        BOOST_TEST(a + d == 5);
        BOOST_TEST(b + c == 5);
        BOOST_TEST(b + d == 6);

        BOOST_TEST(d + a == 5);
        BOOST_TEST(c + b == 5);
        BOOST_TEST(d + b == 6);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        uint32_t constexpr b = 2;

        static_assert(a + b == 3);
        static_assert(b + a == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_builtin_type

template<typename T, typename U>
using result_t = decltype(std::declval<T>() + std::declval<U>());

template<typename T, typename U>
using safe_integer_result_t =
    typename result_t<safe_integer<T>, safe_integer<U>>::value_type;

template<typename T, typename U>
using builtin_result_t = result_t<T, U>;

template<typename T, typename U>
inline auto constexpr compare_result_v = std::is_same_v<
        safe_integer_result_t<T, U>,
        builtin_result_t<T, U>
    >;

BOOST_AUTO_TEST_CASE(same_result_type_as_builtin_arithmetic)
{
    static_assert(compare_result_v<bool, bool>);
    static_assert(compare_result_v<char, char>);
    static_assert(compare_result_v<short, short>);
    static_assert(compare_result_v<int, int>);
    static_assert(compare_result_v<int, long>);
    static_assert(compare_result_v<int, long long>);
    static_assert(compare_result_v<int, unsigned int>);
    static_assert(compare_result_v<int, unsigned long>);
    static_assert(compare_result_v<int, unsigned long long>);
    static_assert(compare_result_v<long, long>);
    static_assert(compare_result_v<long, long long>);
    static_assert(compare_result_v<long, unsigned int>);
    static_assert(compare_result_v<long, unsigned long>);
    static_assert(compare_result_v<long, unsigned long long>);
    static_assert(compare_result_v<long long, long long>);
    static_assert(compare_result_v<long long, unsigned int>);
    static_assert(compare_result_v<long long, unsigned long>);
    static_assert(compare_result_v<long long, unsigned long long>);
    static_assert(compare_result_v<unsigned int, unsigned int>);
    static_assert(compare_result_v<unsigned int, unsigned long>);
    static_assert(compare_result_v<unsigned int, unsigned long long>);
    static_assert(compare_result_v<unsigned long, unsigned long>);
    static_assert(compare_result_v<unsigned long, unsigned long long>);
    static_assert(compare_result_v<unsigned long long, unsigned long long>);
}

BOOST_AUTO_TEST_SUITE_END() // addtion
