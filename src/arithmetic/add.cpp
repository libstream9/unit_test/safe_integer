#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>
#include <stream9/safe_integer/arithmetic/add.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic)

BOOST_AUTO_TEST_SUITE(add_)

BOOST_AUTO_TEST_SUITE(with_same_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 1;
        safe_integer b = 2;

        auto c = add(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 3);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        using integer = safe_integer<int32_t, -5, 5>;

        integer a = -1;
        integer b = 3;

        auto c = add(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 2);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = 0;
        integer b = 1;
        integer c = integer::max();

        BOOST_TEST(add(a, c));

        auto d = add(b, c);
        BOOST_TEST(!d);
        BOOST_TEST(d.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = 0;
        integer b = -1;
        integer c = integer::min();

        BOOST_TEST(add(a, c));

        auto d = add(b, c);
        BOOST_TEST(!d);
        BOOST_TEST(d.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        using integer = safe_integer<int32_t>;

        integer a = 1;
        integer b = 2;

        static_assert(std::is_same_v<
                    decltype(add(a, b)), arithmetic_outcome<integer> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        using integer = safe_integer<int32_t, -5, 5>;

        integer a = -1;
        integer b = 3;

        static_assert(std::is_same_v<
            decltype(add(a, b)),
            arithmetic_outcome<safe_integer<int32_t, -10, 10>> >);
        static_assert(std::is_same_v<
            decltype(add(b, a)),
            arithmetic_outcome<safe_integer<int32_t, -10, 10>> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        safe_integer<int32_t, -10, INT32_MAX> a = -1;
        safe_integer<int32_t, -5, 10> b = 7;
        safe_integer<int32_t, INT32_MIN, 10> c = 7;

        static_assert(std::is_same_v<
            decltype(add(a, b)),
            arithmetic_outcome<safe_integer<int32_t, -15>> >);
        static_assert(std::is_same_v<
            decltype(add(b, a)),
            arithmetic_outcome<safe_integer<int32_t, -15>> >);

        static_assert(std::is_same_v<
            decltype(add(b, c)),
            arithmetic_outcome<safe_integer<int32_t, INT32_MIN, 20>> >);
        static_assert(std::is_same_v<
            decltype(add(c, b)),
            arithmetic_outcome<safe_integer<int32_t, INT32_MIN, 20>> >);

        static_assert(std::is_same_v<
            decltype(add(a, c)),
            arithmetic_outcome<safe_integer<int32_t>> >);
        static_assert(std::is_same_v<
            decltype(add(c, a)),
            arithmetic_outcome<safe_integer<int32_t>> >);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer constexpr a = 1;
        safe_integer constexpr b = 2;

        static_assert(add(a, b).value() == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_type

BOOST_AUTO_TEST_SUITE(with_different_size)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        auto c = add(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 3);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        safe_integer<int64_t, -5, 5> b = 3;

        auto c = add(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 2);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int64_t> a = decltype(a)::max() - 1;
        safe_integer<int32_t> b = 1;
        safe_integer<int32_t> c = 2;

        BOOST_TEST(add(a, b));

        auto e = add(a, c);

        BOOST_TEST(!e);
        BOOST_TEST(e.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int64_t> a = decltype(a)::min() + 1;
        safe_integer<int32_t> b = -1;
        safe_integer<int32_t> c = -2;

        BOOST_TEST(add(a, b));

        auto d = add(a, c);

        BOOST_TEST(!d);
        BOOST_TEST(d.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        static_assert(std::is_same_v<
                decltype(add(a, b)),
                arithmetic_outcome<safe_integer<int64_t>> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        safe_integer<int64_t, -5, 5> b = 3;

        static_assert(std::is_same_v<
            decltype(add(a, b)),
            arithmetic_outcome<safe_integer<int64_t, -10, 10>> >);
        static_assert(std::is_same_v<
            decltype(add(b, a)),
            arithmetic_outcome<safe_integer<int64_t, -10, 10>> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        safe_integer<int64_t, -10, INT64_MAX> a = -1;
        safe_integer<int32_t, -5, 10> b = 7;
        safe_integer<int64_t, INT64_MIN, 10> c = 7;

        static_assert(std::is_same_v<
            decltype(add(a, b)),
            arithmetic_outcome<safe_integer<int64_t, -15>> >);
        static_assert(std::is_same_v<
            decltype(add(b, a)),
            arithmetic_outcome<safe_integer<int64_t, -15>> >);

        static_assert(std::is_same_v<
            decltype(add(b, c)),
            arithmetic_outcome<safe_integer<int64_t, INT64_MIN, 20>> >);
        static_assert(std::is_same_v<
            decltype(add(c, b)),
            arithmetic_outcome<safe_integer<int64_t, INT64_MIN, 20>> >);

        static_assert(std::is_same_v<
            decltype(add(a, c)),
            arithmetic_outcome<safe_integer<int64_t>> >);
        static_assert(std::is_same_v<
            decltype(add(c, a)),
            arithmetic_outcome<safe_integer<int64_t>> >);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<int64_t> constexpr b = 2;

        static_assert(add(a, b).value() == 3);
        static_assert(add(b, a).value() == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_size

BOOST_AUTO_TEST_SUITE(with_different_sign_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<uint32_t> b = 2;

        auto c = add(a, b);

        BOOST_TEST(c); BOOST_TEST(*c == 3);

        auto d = add(b, a);

        BOOST_TEST(d); BOOST_TEST(*d == 3);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        safe_integer<uint32_t, 5, 10> b = 7;

        auto c = add(a, b);

        BOOST_TEST(c); BOOST_TEST(*c == 6);

        auto d = add(b, a);

        BOOST_TEST(d); BOOST_TEST(*d == 6);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_overflow_1)
    {
        safe_integer<int64_t> a = decltype(a)::max() - 1;
        safe_integer<uint32_t> b = 1;
        safe_integer<uint32_t> c = 2;

        BOOST_TEST(add(a, b));

        auto d = add(a, c);
        BOOST_TEST(!d);
        BOOST_TEST(d.error() == arithmetic_errc::overflow);

        BOOST_TEST(add(b, a));

        auto e = add(c, a);
        BOOST_TEST(!e);
        BOOST_TEST(e.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_overflow_2)
    {
        safe_integer<uint32_t> a = decltype(a)::max() - 1;
        safe_integer<int32_t> b = 1;
        safe_integer<int32_t> c = 2;

        BOOST_TEST(add(a, b));

        auto d = add(a, c);
        BOOST_TEST(!d);
        BOOST_TEST(d.error() == arithmetic_errc::overflow);

        BOOST_TEST(add(b, a));

        auto e = add(c, a);
        BOOST_TEST(!e);
        BOOST_TEST(e.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_underflow_1)
    {
        safe_integer<int32_t> a = -2;
        safe_integer<uint32_t> b = 2;
        safe_integer<uint32_t> c = 1;

        BOOST_TEST(add(a, b));

        auto d = add(a, c);
        BOOST_TEST(!d);
        BOOST_TEST(d.error() == arithmetic_errc::underflow);

        BOOST_TEST(add(b, a));

        auto e = add(c, a);
        BOOST_TEST(!e);
        BOOST_TEST(e.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(arthmetic_underflow_2)
    {
        safe_integer<uint32_t> a = 1;
        safe_integer<int32_t> b = -1;
        safe_integer<int32_t> c = -2;

        BOOST_TEST(add(a, b));

        auto d = add(a, c);
        BOOST_TEST(!d);
        BOOST_TEST(d.error() == arithmetic_errc::underflow);

        BOOST_TEST(add(b, a));

        auto e = add(c, a);
        BOOST_TEST(!e);
        BOOST_TEST(e.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<uint32_t> a = 1;
        safe_integer<int32_t> b = 2;

        static_assert(std::is_same_v<
                decltype(add(a, b)),
                arithmetic_outcome<safe_integer<uint32_t>> >);
        static_assert(std::is_same_v<
                decltype(add(b, a)),
                arithmetic_outcome<safe_integer<uint32_t>> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_normal)
    {
        safe_integer<int32_t, -10, 10> a = -1;
        safe_integer<uint32_t, 5, 10> b = 7;

        static_assert(std::is_same_v<
            decltype(add(a, b)),
            arithmetic_outcome<safe_integer<uint32_t, 0, 20>> >);
        static_assert(std::is_same_v<
            decltype(add(b, a)),
            arithmetic_outcome<safe_integer<uint32_t, 0, 20>> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        safe_integer<int32_t, -10, INT32_MAX> a = -1;
        safe_integer<uint32_t, 5, 10> b = 7;
        safe_integer<uint32_t, 5, UINT32_MAX> c = 7;

        static_assert(std::is_same_v<
            decltype(add(a, b)),
            arithmetic_outcome<safe_integer<uint32_t, 0, INT32_MAX + 10u>> >);
        static_assert(std::is_same_v<
            decltype(add(b, a)),
            arithmetic_outcome<safe_integer<uint32_t, 0, INT32_MAX + 10u>> >);

        static_assert(std::is_same_v<
            decltype(add(a, c)),
            arithmetic_outcome<safe_integer<uint32_t>> >);
        static_assert(std::is_same_v<
            decltype(add(c, a)),
            arithmetic_outcome<safe_integer<uint32_t>> >);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<uint32_t> constexpr b = 2;

        static_assert(add(a, b).value() == 3);
        static_assert(add(b, a).value() == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_safe_integer

BOOST_AUTO_TEST_SUITE_END() // add_

BOOST_AUTO_TEST_SUITE_END() // arithmetic
