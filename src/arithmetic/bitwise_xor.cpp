#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(bitwise_xor)

    BOOST_AUTO_TEST_CASE(with_same_safe_integer)
    {
        safe_integer a = 1;
        safe_integer b = 2;
        safe_integer c = 3;

        BOOST_TEST((a ^ b) == c);
    }

    BOOST_AUTO_TEST_CASE(with_same_safe_integer_constexpr)
    {
        safe_integer constexpr a = 1;
        safe_integer constexpr b = 2;

        auto constexpr d = a ^ b; BOOST_TEST(d == 3);
    }

    BOOST_AUTO_TEST_CASE(with_builtin_type)
    {
        safe_integer<int> a = 1;
        int b = 2;

        BOOST_TEST((a ^ b) == 3);
        BOOST_TEST((b ^ a) == 3);
    }

    BOOST_AUTO_TEST_CASE(with_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        int32_t constexpr b = 2;

        auto constexpr c = a ^ b; BOOST_TEST(c == 3);
        auto constexpr d = b ^ a; BOOST_TEST(d == 3);
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type)
    {
        safe_integer<int32_t> a = 1;
        int64_t b = 2;

        BOOST_TEST((a ^ b) == 3);
        BOOST_TEST((b ^ a) == 3);
    }

    BOOST_AUTO_TEST_CASE(with_different_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        int64_t constexpr b = 2;

        auto constexpr c = a ^ b; BOOST_TEST(c == 3);
        auto constexpr d = b ^ a; BOOST_TEST(d == 3);
    }

    BOOST_AUTO_TEST_CASE(with_different_signed_builtin_type_1)
    {
        safe_integer<int32_t> a = 1;
        uint32_t b = 2;

        BOOST_TEST((a ^ b) == 3);
        BOOST_TEST((b ^ a) == 3);
    }

    BOOST_AUTO_TEST_CASE(with_different_signed_builtin_type_2)
    {
        safe_integer<uint32_t> a = 1;
        int32_t b = 2;

        BOOST_TEST((a ^ b) == 3);
        BOOST_TEST((b ^ a) == 3);
    }

    BOOST_AUTO_TEST_CASE(with_different_signed_builtin_type_conversion_error)
    {
        safe_integer<uint32_t> a = 1;
        int32_t b =  2;
        int32_t c = -2;

        BOOST_CHECK_NO_THROW(a ^ b);
        BOOST_CHECK_THROW(a ^ c, conversion_error);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        uint32_t constexpr b = 2;

        auto constexpr c = a ^ b; BOOST_TEST(c == 3);
        auto constexpr d = b ^ a; BOOST_TEST(d == 3);
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        BOOST_TEST((a ^ b) == 3);
        BOOST_TEST((b ^ a) == 3);
    }

    BOOST_AUTO_TEST_CASE(with_different_safe_integer_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<int64_t> constexpr b = 2;

        auto constexpr c = a ^ b; BOOST_TEST(c == 3);
        auto constexpr d = b ^ a; BOOST_TEST(d == 3);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<uint64_t> b = 2;

        BOOST_TEST((a ^ b) == 3);
        BOOST_TEST((b ^ a) == 3);
    }

    BOOST_AUTO_TEST_CASE(with_differently_signed_safe_integer_constexpr)
    {
        safe_integer<int32_t> constexpr a = 1;
        safe_integer<uint64_t> constexpr b = 2;

        auto constexpr c = a ^ b; BOOST_TEST(c == 3);
        auto constexpr d = b ^ a; BOOST_TEST(d == 3);
    }

    BOOST_AUTO_TEST_CASE(boundary_will_disappear_from_result)
    {
        using integer = safe_integer<int, 0, 1>;

        integer a = 1;
        integer b = 1;

        BOOST_CHECK_NO_THROW(a ^ b);
        static_assert(std::is_same_v<decltype(a ^ b), safe_integer<int>>);
    }

    BOOST_AUTO_TEST_CASE(still_you_can_check_boundary_on_assignment)
    {
        using integer = safe_integer<int, 1, 2>;

        integer a = 1;
        integer b = 2;
        integer c = 1;

        BOOST_CHECK_NO_THROW(a ^ b);
        BOOST_CHECK_THROW(c = a ^ b, assignment_overflow_error);
    }

    template<typename T, typename U>
    using result_t = decltype(std::declval<T>() ^ std::declval<U>());

    template<typename T, typename U>
    using safe_integer_result_t =
        typename result_t<safe_integer<T>, safe_integer<U>>::value_type;

    template<typename T, typename U>
    using builtin_result_t = result_t<T, U>;

    template<typename T, typename U>
    auto constexpr compare_result_v = std::is_same_v<
            safe_integer_result_t<T, U>,
            builtin_result_t<T, U>
        >;

    BOOST_AUTO_TEST_CASE(same_result_type_as_builtin_types)
    {
        static_assert(compare_result_v<bool, bool>);
        static_assert(compare_result_v<char, char>);
        static_assert(compare_result_v<short, short>);
        static_assert(compare_result_v<int, int>);
        static_assert(compare_result_v<int, long>);
        static_assert(compare_result_v<int, long long>);
        static_assert(compare_result_v<int, unsigned int>);
        static_assert(compare_result_v<int, unsigned long>);
        static_assert(compare_result_v<int, unsigned long long>);
        static_assert(compare_result_v<long, long>);
        static_assert(compare_result_v<long, long long>);
        static_assert(compare_result_v<long, unsigned int>);
        static_assert(compare_result_v<long, unsigned long>);
        static_assert(compare_result_v<long, unsigned long long>);
        static_assert(compare_result_v<long long, long long>);
        static_assert(compare_result_v<long long, unsigned int>);
        static_assert(compare_result_v<long long, unsigned long>);
        static_assert(compare_result_v<long long, unsigned long long>);
        static_assert(compare_result_v<unsigned int, unsigned int>);
        static_assert(compare_result_v<unsigned int, unsigned long>);
        static_assert(compare_result_v<unsigned int, unsigned long long>);
        static_assert(compare_result_v<unsigned long, unsigned long>);
        static_assert(compare_result_v<unsigned long, unsigned long long>);
        static_assert(compare_result_v<unsigned long long, unsigned long long>);
    }

BOOST_AUTO_TEST_SUITE_END() // bitwise_xor
