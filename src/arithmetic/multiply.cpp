#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>
#include <stream9/safe_integer/arithmetic/multiply.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic)

BOOST_AUTO_TEST_SUITE(multiply_)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 2;
        safe_integer b = 2;

        auto c = multiply(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 4);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        safe_integer<int32_t> a = INT32_MAX;
        safe_integer<int32_t> b = 2;

        auto c = multiply(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        safe_integer<int32_t> a = INT32_MIN;
        safe_integer<int32_t> b = 2;

        auto c = multiply(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::underflow);
    }

BOOST_AUTO_TEST_SUITE_END() // multiply_

BOOST_AUTO_TEST_SUITE_END() // arithmetic
