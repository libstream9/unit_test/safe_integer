#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>
#include <stream9/safe_integer/arithmetic/divide.hpp>

#include <cstdint>
#include <iostream>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic)

BOOST_AUTO_TEST_SUITE(divide_)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 6;
        safe_integer b = 3;

        auto c = divide(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 2);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        if (is_twos_complement_integer_v<int32_t>) {
            safe_integer<int32_t> a = INT32_MIN;
            safe_integer<int32_t> b = -1;

            auto c = divide(a, b);

            BOOST_TEST(!c);
            BOOST_TEST(c.error() == arithmetic_errc::overflow);
        }
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        safe_integer<uint32_t> a = 10;
        safe_integer<int32_t> b = -1;

        auto c = divide(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(zero_division)
    {
        safe_integer<int32_t> a = 10;
        safe_integer<int32_t> b = 0;

        auto c = divide(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::divide_by_zero);
    }

    BOOST_AUTO_TEST_SUITE(boundary)

        template<typename T, typename U>
        auto
        brute_force(T, U)
        {
            auto const x1 = T::min(), x2 = T::max();
            auto const y1 = U::min(), y2 = U::max();

            using value_t = decltype(x1 / y1);

            auto min = std::numeric_limits<value_t>::max();
            auto max = std::numeric_limits<value_t>::min();

            for (auto x = x1; less_equal(x, x2); ++x) {
                for (auto y = y1; less_equal(y, y2); ++y) {
                    if (y == 0) continue;

                    auto q = safe_divide<value_t>(x, y);

                    min = less(*q, min) ? *q : min;
                    max = greater(*q, max) ? *q : max;
                }
            }

            return std::make_pair(min, max);
        }

        BOOST_AUTO_TEST_SUITE(lhs_pos_pos)

            BOOST_AUTO_TEST_CASE(rhs_pos_pos)
            {
                safe_integer<int, 5, 100> lhs;
                safe_integer<int, 10, 50> rhs;

                using result_t =
                        decltype(divide(lhs, rhs))::value_type;

                auto [min, max] = brute_force(lhs, rhs);

                BOOST_TEST(result_t::min() == min);
                BOOST_TEST(result_t::max() == max);
            }

            BOOST_AUTO_TEST_CASE(rhs_neg_pos)
            {
                safe_integer<int, 5, 100> lhs;
                safe_integer<int, -10, 50> rhs;

                using result_t =
                        decltype(divide(lhs, rhs))::value_type;

                auto [min, max] = brute_force(lhs, rhs);

                BOOST_TEST(result_t::min() == min);
                BOOST_TEST(result_t::max() == max);
            }

            BOOST_AUTO_TEST_CASE(rhs_neg_neg)
            {
                safe_integer<int, 5, 100> lhs;
                safe_integer<int, -50, -10> rhs;

                using result_t =
                        decltype(divide(lhs, rhs))::value_type;

                auto [min, max] = brute_force(lhs, rhs);

                BOOST_TEST(result_t::min() == min);
                BOOST_TEST(result_t::max() == max);
            }

        BOOST_AUTO_TEST_SUITE_END() // lhs_pos_pos

        BOOST_AUTO_TEST_SUITE(lhs_neg_pos)

            BOOST_AUTO_TEST_CASE(rhs_pos_pos)
            {
                safe_integer<int, -5, 100> lhs;
                safe_integer<int, 10, 50> rhs;

                using result_t =
                        decltype(divide(lhs, rhs))::value_type;

                auto [min, max] = brute_force(lhs, rhs);

                BOOST_TEST(result_t::min() == min);
                BOOST_TEST(result_t::max() == max);
            }

            BOOST_AUTO_TEST_CASE(rhs_neg_pos)
            {
                safe_integer<int, -5, 100> lhs;
                safe_integer<int, -10, 50> rhs;

                using result_t =
                        decltype(divide(lhs, rhs))::value_type;

                auto [min, max] = brute_force(lhs, rhs);

                BOOST_TEST(result_t::min() == min);
                BOOST_TEST(result_t::max() == max);
            }

            BOOST_AUTO_TEST_CASE(rhs_neg_neg)
            {
                safe_integer<int, -5, 100> lhs;
                safe_integer<int, -50, -10> rhs;

                using result_t =
                        decltype(divide(lhs, rhs))::value_type;

                auto [min, max] = brute_force(lhs, rhs);

                BOOST_TEST(result_t::min() == min);
                BOOST_TEST(result_t::max() == max);
            }

        BOOST_AUTO_TEST_SUITE_END() // lhs_neg_pos

        BOOST_AUTO_TEST_SUITE(lhs_neg_neg)

            BOOST_AUTO_TEST_CASE(rhs_pos_pos)
            {
                safe_integer<int, -100, -5> lhs;
                safe_integer<int, 10, 50> rhs;

                using result_t =
                        decltype(divide(lhs, rhs))::value_type;

                auto [min, max] = brute_force(lhs, rhs);

                BOOST_TEST(result_t::min() == min);
                BOOST_TEST(result_t::max() == max);
            }

            BOOST_AUTO_TEST_CASE(rhs_neg_pos)
            {
                safe_integer<int, -100, -5> lhs;
                safe_integer<int, -10, 50> rhs;

                using result_t =
                        decltype(divide(lhs, rhs))::value_type;

                auto [min, max] = brute_force(lhs, rhs);

                BOOST_TEST(result_t::min() == min);
                BOOST_TEST(result_t::max() == max);
            }

            BOOST_AUTO_TEST_CASE(rhs_neg_neg)
            {
                safe_integer<int, -100, -5> lhs;
                safe_integer<int, -50, -10> rhs;

                using result_t =
                        decltype(divide(lhs, rhs))::value_type;

                auto [min, max] = brute_force(lhs, rhs);

                BOOST_TEST(result_t::min() == min);
                BOOST_TEST(result_t::max() == max);
            }

        BOOST_AUTO_TEST_SUITE_END() // lhs_neg_neg

    BOOST_AUTO_TEST_SUITE_END() // boundary

BOOST_AUTO_TEST_SUITE_END() // divide_

BOOST_AUTO_TEST_SUITE_END() // arithmetic
