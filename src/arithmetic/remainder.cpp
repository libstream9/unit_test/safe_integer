#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(remainder_)

    BOOST_AUTO_TEST_CASE(same_safe_integer)
    {
        safe_integer a = 3;
        safe_integer b = 2;
        safe_integer c = 1;

        BOOST_TEST(a % b == c);
    }

    BOOST_AUTO_TEST_CASE(same_safe_integer_divided_by_zero)
    {
        safe_integer a = 3;
        safe_integer b = 2;
        safe_integer c = 0;

        BOOST_CHECK_NO_THROW(a % b);
        BOOST_CHECK_THROW(a % c, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(same_safe_integer_overflow)
    {
    }

    BOOST_AUTO_TEST_CASE(same_safe_integer_constexpr)
    {
        safe_integer constexpr a = 3;
        safe_integer constexpr b = 2;

        auto constexpr c = a % b; BOOST_TEST(c == 1);
    }

    BOOST_AUTO_TEST_CASE(builtin_type)
    {
        safe_integer a = 3;
        int b = 2;
        safe_integer c = 1;
        safe_integer d = 2;

        BOOST_TEST(a % b == c);
        BOOST_TEST(b % a == d);
    }

    BOOST_AUTO_TEST_CASE(builtin_type_divided_by_zero)
    {
        safe_integer a = 1;
        safe_integer b = 2;
        int c = 0;

        BOOST_CHECK_NO_THROW(a % b);
        BOOST_CHECK_THROW(a % c, divided_by_zero_error);

        int d = 1;
        safe_integer e = 2;
        safe_integer f = 0;

        BOOST_CHECK_NO_THROW(d % e);
        BOOST_CHECK_THROW(d % f, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(builtin_type_overflow)
    {
    }

    BOOST_AUTO_TEST_CASE(builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 3;
        int32_t constexpr b = 2;

        auto constexpr c = a % b; BOOST_TEST(c == 1);
        auto constexpr d = b % a; BOOST_TEST(d == 2);
    }

    BOOST_AUTO_TEST_CASE(different_builtin_type)
    {
        safe_integer<int32_t> a = 3;
        int64_t b = 2;
        safe_integer<int32_t> c = 1;

        BOOST_TEST(a % b == c);

        int32_t d = 3;
        safe_integer<int64_t> e = 2;
        int32_t f = 1;

        BOOST_TEST(d % e == f);
    }

    BOOST_AUTO_TEST_CASE(different_builtin_type_divided_by_zero)
    {
        safe_integer<int64_t> a = 1;
        int32_t b = 2;
        int32_t c = 0;

        BOOST_CHECK_NO_THROW(a % b);
        BOOST_CHECK_THROW(a % c, divided_by_zero_error);

        int64_t d = 1;
        safe_integer<int32_t> e = 2;
        safe_integer<int32_t> f = 0;

        BOOST_CHECK_NO_THROW(d % e);
        BOOST_CHECK_THROW(d % f, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(different_builtin_type_overflow)
    {
    }

    BOOST_AUTO_TEST_CASE(different_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 3;
        int64_t constexpr b = 2;

        auto constexpr c = a % b; BOOST_TEST(c == 1);
        auto constexpr d = b % a; BOOST_TEST(d == 2);
    }

    BOOST_AUTO_TEST_CASE(differently_signed_builtin_type_1)
    {
        safe_integer<int32_t> a = 3;
        uint32_t b = 2;
        safe_integer<int32_t> c = 1;

        BOOST_TEST(a % b == c);

        uint32_t d = 3;
        safe_integer<int32_t> e = 2;
        safe_integer<int32_t> f = 1;

        BOOST_TEST(d % e == f);
    }

    BOOST_AUTO_TEST_CASE(differently_signed_builtin_type_2)
    {
        safe_integer<uint32_t> a = 3;
        int32_t b = 2;
        safe_integer<uint32_t> c = 1;

        BOOST_TEST(a % b == c);

        int32_t d = 3;
        safe_integer<uint32_t> e = 2;
        safe_integer<uint32_t> f = 1;

        BOOST_TEST(d % e == f);
    }

    BOOST_AUTO_TEST_CASE(differently_signed_builtin_type_divided_by_zero)
    {
        safe_integer<uint32_t> a = 3;
        int32_t b = 2;
        int32_t c = 0;

        BOOST_CHECK_NO_THROW(a % b);
        BOOST_CHECK_THROW(a % c, divided_by_zero_error);

        uint32_t d = 3;
        safe_integer<int32_t> e = 2;
        safe_integer<int32_t> f = 0;

        BOOST_CHECK_NO_THROW(d % e);
        BOOST_CHECK_THROW(d % f, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(differently_signed_builtin_type_constexpr)
    {
        safe_integer<int32_t> constexpr a = 3;
        uint32_t constexpr b = 2;
        safe_integer<int32_t> constexpr c = 1;

        auto constexpr d = a % b; BOOST_TEST(d == 1);
        auto constexpr e = b % c; BOOST_TEST(e == 0);
    }

    BOOST_AUTO_TEST_CASE(different_safe_integer)
    {
        safe_integer<int32_t> a = 3;
        safe_integer<uint64_t> b = 2;
        safe_integer<int32_t> c = 1;

        BOOST_TEST(a % b == c);
    }

    BOOST_AUTO_TEST_CASE(different_safe_integer_divided_by_zero)
    {
        safe_integer<int64_t> a = 3;
        safe_integer<int32_t> b = 2;
        safe_integer<int32_t> c = 0;

        BOOST_CHECK_NO_THROW(a % b);
        BOOST_CHECK_THROW(a % c, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(different_safe_integer_constexpr)
    {
        safe_integer<int32_t> constexpr a = 3;
        safe_integer<int64_t> constexpr b = 2;

        auto constexpr c = a % b; BOOST_TEST(c == 1);
        auto constexpr d = b % a; BOOST_TEST(d == 2);
    }

    template<typename T, typename U>
    using result_t = decltype(std::declval<T>() % std::declval<U>());

    template<typename T, typename U>
    using safe_integer_result_t =
        typename result_t<safe_integer<T>, safe_integer<U>>::value_type;

    template<typename T, typename U>
    using builtin_result_t = result_t<T, U>;

    template<typename T, typename U>
    auto constexpr compare_result_v = std::is_same_v<
            safe_integer_result_t<T, U>,
            builtin_result_t<T, U>
        >;

    BOOST_AUTO_TEST_CASE(same_arithmetic_type_conversion_as_builtin_types)
    {
        static_assert(compare_result_v<bool, bool>);
        static_assert(compare_result_v<char, char>);
        static_assert(compare_result_v<short, short>);
        static_assert(compare_result_v<int, int>);
        static_assert(compare_result_v<int, long>);
        static_assert(compare_result_v<int, long long>);
        static_assert(compare_result_v<int, unsigned int>);
        static_assert(compare_result_v<int, unsigned long>);
        static_assert(compare_result_v<int, unsigned long long>);
        static_assert(compare_result_v<long, long>);
        static_assert(compare_result_v<long, long long>);
        static_assert(compare_result_v<long, unsigned int>);
        static_assert(compare_result_v<long, unsigned long>);
        static_assert(compare_result_v<long, unsigned long long>);
        static_assert(compare_result_v<long long, long long>);
        static_assert(compare_result_v<long long, unsigned int>);
        static_assert(compare_result_v<long long, unsigned long>);
        static_assert(compare_result_v<long long, unsigned long long>);
        static_assert(compare_result_v<unsigned int, unsigned int>);
        static_assert(compare_result_v<unsigned int, unsigned long>);
        static_assert(compare_result_v<unsigned int, unsigned long long>);
        static_assert(compare_result_v<unsigned long, unsigned long>);
        static_assert(compare_result_v<unsigned long, unsigned long long>);
        static_assert(compare_result_v<unsigned long long, unsigned long long>);
    }

BOOST_AUTO_TEST_SUITE_END() // remainder_
