#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>
#include <stream9/safe_integer/arithmetic/modulo.hpp>

#include <cstdint>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic)

BOOST_AUTO_TEST_SUITE(modulo_)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 6;
        safe_integer b = 3;

        auto c = modulo(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 0);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        safe_integer<int32_t> a = -10;
        safe_integer<uint32_t> b = 11;

        auto c = modulo(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(zero_division)
    {
        safe_integer<int32_t> a = 10;
        safe_integer<int32_t> b = 0;

        auto c = modulo(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::divide_by_zero);
    }

    BOOST_AUTO_TEST_SUITE(boundary)

        template<typename T, typename U>
        auto
        brute_force(T, U)
        {
            auto const x1 = T::min(), x2 = T::max();
            auto const y1 = U::min(), y2 = U::max();

            using value_t = decltype(x1 % y1);

            auto min = std::numeric_limits<value_t>::max();
            auto max = std::numeric_limits<value_t>::min();

            for (auto x = x1; less_equal(x, x2); ++x) {
                for (auto y = y1; less_equal(y, y2); ++y) {
                    if (y == 0) continue;

                    auto q = safe_modulo<value_t>(x, y);
                    value_t v {};
                    if (!q) {
                        if (q.error() == arithmetic_errc::overflow) {
                            v = std::numeric_limits<value_t>::max();
                        }
                        else if (q.error() == arithmetic_errc::underflow) {
                            v = std::numeric_limits<value_t>::min();
                        }
                        else {
                            assert(false);
                        }
                    }
                    else {
                        v = *q;
                    }

                    if (less(v, min)) {
                        min = v;
                    }
                    else if (greater(v, max)) {
                        max = v;
                    }
                }
            }

            return std::make_pair(min, max);
        }

        auto compare_with_brute_force = [](auto lhs, auto rhs) {
            using result_t =
                    typename decltype(modulo(lhs, rhs))::value_type;

            auto [min, max] = brute_force(lhs, rhs);

            BOOST_TEST(result_t::min() == min);
            BOOST_TEST(result_t::max() == max);
        };

        BOOST_AUTO_TEST_SUITE(lhs_pos_pos)

            BOOST_AUTO_TEST_SUITE(rhs_pos_pos)

                BOOST_AUTO_TEST_CASE(lhs_gt_rhs)
                {
                    safe_integer<int, 0, 60> lhs;
                    safe_integer<int, 10, 50> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(lhs_eq_rhs)
                {
                    safe_integer<int, 0, 50> lhs;
                    safe_integer<int, 10, 50> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(lhs_lt_rhs)
                {
                    safe_integer<int, 0, 40> lhs;
                    safe_integer<int, 10, 50> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

            BOOST_AUTO_TEST_SUITE_END() // rhs_pos_pos

            BOOST_AUTO_TEST_SUITE(rhs_neg_pos)

                BOOST_AUTO_TEST_CASE(lhs_gt_rhs)
                {
                    safe_integer<int,   0, 60> lhs1;
                    safe_integer<int, -10, 50> rhs1;

                    compare_with_brute_force(lhs1, rhs1);

                    safe_integer<int,   0, 60> lhs2;
                    safe_integer<int, -50, 10> rhs2;

                    compare_with_brute_force(lhs2, rhs2);
                }

                BOOST_AUTO_TEST_CASE(lhs_eq_rhs)
                {
                    safe_integer<int,   0, 50> lhs1;
                    safe_integer<int, -10, 50> rhs1;

                    compare_with_brute_force(lhs1, rhs1);

                    safe_integer<int,   0, 50> lhs2;
                    safe_integer<int, -50, 10> rhs2;

                    compare_with_brute_force(lhs2, rhs2);
                }

                BOOST_AUTO_TEST_CASE(lhs_lt_rhs)
                {
                    safe_integer<int,   0, 40> lhs1;
                    safe_integer<int, -10, 50> rhs1;

                    compare_with_brute_force(lhs1, rhs1);

                    safe_integer<int,   0, 40> lhs2;
                    safe_integer<int, -50, 10> rhs2;

                    compare_with_brute_force(lhs2, rhs2);
                }

            BOOST_AUTO_TEST_SUITE_END() // rhs_neg_pos

            BOOST_AUTO_TEST_SUITE(rhs_neg_neg)

                BOOST_AUTO_TEST_CASE(lhs_gt_rhs)
                {
                    safe_integer<int,   0,  60> lhs;
                    safe_integer<int, -50, -10> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(lhs_eq_rhs)
                {
                    safe_integer<int,   0,  50> lhs;
                    safe_integer<int, -50, -10> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(lhs_lt_rhs)
                {
                    safe_integer<int,   0,  40> lhs;
                    safe_integer<int, -50, -10> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

            BOOST_AUTO_TEST_SUITE_END() // rhs_neg_neg

        BOOST_AUTO_TEST_SUITE_END() // lhs_pos_pos

        BOOST_AUTO_TEST_SUITE(lhs_neg_pos)

            BOOST_AUTO_TEST_SUITE(rhs_pos_pos)

                BOOST_AUTO_TEST_CASE(lhs_gt_rhs)
                {
                    safe_integer<int, -60, 60> lhs;
                    safe_integer<int,  10, 50> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(lhs_eq_rhs)
                {
                    safe_integer<int, -50, 50> lhs;
                    safe_integer<int,  10, 50> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(lhs_lt_rhs)
                {
                    safe_integer<int, -40, 40> lhs;
                    safe_integer<int,  10, 50> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(int32_uint64)
                {
                    safe_integer<int32_t, -10, 20> lhs;
                    safe_integer<uint64_t, 0, 30> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

            BOOST_AUTO_TEST_SUITE_END() // rhs_pos_pos

            BOOST_AUTO_TEST_SUITE(rhs_neg_pos)

                BOOST_AUTO_TEST_CASE(lhs_gt_rhs)
                {
                    safe_integer<int, -10, 60> lhs1;
                    safe_integer<int, -10, 50> rhs1;

                    compare_with_brute_force(lhs1, rhs1);

                    safe_integer<int, -10, 60> lhs2;
                    safe_integer<int, -50, 10> rhs2;

                    compare_with_brute_force(lhs2, rhs2);

                    safe_integer<int, -60, 10> lhs3;
                    safe_integer<int, -10, 50> rhs3;

                    compare_with_brute_force(lhs3, rhs3);

                    safe_integer<int, -60, 10> lhs4;
                    safe_integer<int, -50, 10> rhs4;

                    compare_with_brute_force(lhs4, rhs4);
                }

                BOOST_AUTO_TEST_CASE(lhs_eq_rhs)
                {
                    safe_integer<int, -10, 50> lhs1;
                    safe_integer<int, -10, 50> rhs1;

                    compare_with_brute_force(lhs1, rhs1);

                    safe_integer<int, -10, 50> lhs2;
                    safe_integer<int, -50, 10> rhs2;

                    compare_with_brute_force(lhs2, rhs2);

                    safe_integer<int, -50, 10> lhs3;
                    safe_integer<int, -10, 50> rhs3;

                    compare_with_brute_force(lhs3, rhs3);

                    safe_integer<int, -50, 10> lhs4;
                    safe_integer<int, -50, 10> rhs4;

                    compare_with_brute_force(lhs4, rhs4);
                }

                BOOST_AUTO_TEST_CASE(lhs_lt_rhs)
                {
                    safe_integer<int, -10, 40> lhs1;
                    safe_integer<int, -10, 50> rhs1;

                    compare_with_brute_force(lhs1, rhs1);

                    safe_integer<int, -10, 40> lhs2;
                    safe_integer<int, -50, 10> rhs2;

                    compare_with_brute_force(lhs2, rhs2);

                    safe_integer<int, -40, 10> lhs3;
                    safe_integer<int, -10, 50> rhs3;

                    compare_with_brute_force(lhs3, rhs3);

                    safe_integer<int, -40, 10> lhs4;
                    safe_integer<int, -50, 10> rhs4;

                    compare_with_brute_force(lhs4, rhs4);
                }

            BOOST_AUTO_TEST_SUITE_END() // rhs_neg_pos

        BOOST_AUTO_TEST_SUITE_END() // lhs_neg_pos

        BOOST_AUTO_TEST_SUITE(lhs_neg_neg)

            BOOST_AUTO_TEST_SUITE(rhs_pos_pos)

                BOOST_AUTO_TEST_CASE(lhs_gt_rhs)
                {
                    safe_integer<int, -60, -10> lhs;
                    safe_integer<int,  10,  50> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(lhs_eq_rhs)
                {
                    safe_integer<int, -50, -10> lhs;
                    safe_integer<int,  10,  50> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(lhs_lt_rhs)
                {
                    safe_integer<int, -40, -10> lhs;
                    safe_integer<int,  10,  50> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

            BOOST_AUTO_TEST_SUITE_END() // rhs_pos_pos

            BOOST_AUTO_TEST_SUITE(rhs_neg_pos)

                BOOST_AUTO_TEST_CASE(lhs_gt_rhs)
                {
                    safe_integer<int, -60, -10> lhs1;
                    safe_integer<int, -10,  50> rhs1;

                    compare_with_brute_force(lhs1, rhs1);

                    safe_integer<int, -60, -10> lhs2;
                    safe_integer<int, -50,  10> rhs2;

                    compare_with_brute_force(lhs2, rhs2);
                }

                BOOST_AUTO_TEST_CASE(lhs_eq_rhs)
                {
                    safe_integer<int, -50, -10> lhs1;
                    safe_integer<int, -10,  50> rhs1;

                    compare_with_brute_force(lhs1, rhs1);

                    safe_integer<int, -50, -10> lhs2;
                    safe_integer<int, -50,  10> rhs2;

                    compare_with_brute_force(lhs2, rhs2);
                }

                BOOST_AUTO_TEST_CASE(lhs_lt_rhs)
                {
                    safe_integer<int, -40, -10> lhs1;
                    safe_integer<int, -10,  50> rhs1;

                    compare_with_brute_force(lhs1, rhs1);

                    safe_integer<int, -40, -10> lhs2;
                    safe_integer<int, -50,  10> rhs2;

                    compare_with_brute_force(lhs2, rhs2);
                }

            BOOST_AUTO_TEST_SUITE_END() // rhs_neg_pos

            BOOST_AUTO_TEST_SUITE(rhs_neg_neg)

                BOOST_AUTO_TEST_CASE(lhs_gt_rhs)
                {
                    safe_integer<int, -60,  10> lhs;
                    safe_integer<int, -50, -10> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(lhs_eq_rhs)
                {
                    safe_integer<int, -50,  10> lhs;
                    safe_integer<int, -50, -10> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

                BOOST_AUTO_TEST_CASE(lhs_lt_rhs)
                {
                    safe_integer<int, -40,  10> lhs;
                    safe_integer<int, -50, -10> rhs;

                    compare_with_brute_force(lhs, rhs);
                }

            BOOST_AUTO_TEST_SUITE_END() // rhs_neg_neg

        BOOST_AUTO_TEST_SUITE_END() // lhs_neg_neg

    BOOST_AUTO_TEST_SUITE_END() // boundary

BOOST_AUTO_TEST_SUITE_END() // modulo_

BOOST_AUTO_TEST_SUITE_END() // arithmetic
