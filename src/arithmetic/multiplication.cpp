#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic)

BOOST_AUTO_TEST_SUITE(multiplication)

BOOST_AUTO_TEST_SUITE(with_same_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 1;
        safe_integer b = 2;
        safe_integer c = 2;

        BOOST_TEST(a * b == c);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        using integer = safe_integer<int32_t, -5, 5>;

        integer a = -1;
        integer b = 3;

        BOOST_TEST(a * b == -3);
        BOOST_TEST(b * a == -3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow_1)
    {
        using integer = safe_integer<int32_t>;

        integer a = 2;
        integer b = 3;
        integer c = decltype(c)::max() / 2;

        BOOST_CHECK_NO_THROW(a * c);
        BOOST_CHECK_THROW(b * c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow_2)
    {
        if constexpr (is_twos_complement_integer_v<int>) {
            using integer = safe_integer<int>;

            integer a = integer::min() + 1;
            integer b = integer::min();
            integer c = -1;

            BOOST_CHECK_NO_THROW(a * c);
            BOOST_CHECK_THROW(b * c, overflow_error);
        }
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = 2;
        integer b = 3;
        integer c = decltype(c)::min() / 2;

        BOOST_CHECK_NO_THROW(a * c);
        BOOST_CHECK_THROW(b * c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        using integer = safe_integer<int32_t>;

        integer a = 1;
        integer b = 2;

        static_assert(std::is_same_v<decltype(a * b), integer>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<int32_t, 2, 5> pos_pos = 2;
        safe_integer<int32_t, -2, 5> neg_pos = 0;
        safe_integer<int32_t, -5, -2> neg_neg = -3;

        static_assert(std::is_same_v<
            decltype(pos_pos * pos_pos), safe_integer<int32_t, 4, 25> >);
        static_assert(std::is_same_v<
            decltype(pos_pos * neg_pos), safe_integer<int32_t, -10, 25> >);
        static_assert(std::is_same_v<
            decltype(pos_pos * neg_neg), safe_integer<int32_t, -25, -4> >);
        static_assert(std::is_same_v<
            decltype(neg_pos * neg_pos), safe_integer<int32_t, -10, 25> >);
        static_assert(std::is_same_v<
            decltype(neg_pos * neg_neg), safe_integer<int32_t, -25, 10> >);
        static_assert(std::is_same_v<
            decltype(neg_neg * neg_neg), safe_integer<int32_t, 4, 25> >);
    }

    template<typename> class typeof;
    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto const int32_min = std::numeric_limits<int32_t>::min();
        auto const int32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int32_t,              1,              2> a = 1;
        safe_integer<int32_t, int32_min + 10,              1> b = 0;
        safe_integer<int32_t,              0, int32_max - 10> c = 0;
        safe_integer<int32_t, int32_min + 10, int32_max - 10> d = 0;

        static_assert(std::is_same_v<
            decltype(a * b), safe_integer<int32_t, int32_min,         2> >);
        static_assert(std::is_same_v<
            decltype(a * c), safe_integer<int32_t,         0, int32_max> >);
        static_assert(std::is_same_v<
            decltype(a * d), safe_integer<int32_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer       a = 1;
        safe_integer const b = 2;

        BOOST_TEST(a * b == 2);
        BOOST_TEST(b * a == 2);
        BOOST_TEST(b * b == 4);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer constexpr a = 2;
        safe_integer constexpr b = 3;

        static_assert(a * b == 6);
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_safe_integer

BOOST_AUTO_TEST_SUITE(with_different_size_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 2;
        safe_integer<int64_t> b = 3;

        BOOST_TEST(a * b == 6);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        safe_integer<int64_t, -5, 5> b = 3;

        BOOST_TEST(a * b == -3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int64_t> a = decltype(a)::max() / 2;
        safe_integer<int32_t> b = 2;
        safe_integer<int32_t> c = 3;

        BOOST_CHECK_NO_THROW(a * b);
        BOOST_CHECK_THROW(a * c, overflow_error);

        BOOST_CHECK_NO_THROW(b * a);
        BOOST_CHECK_THROW(c * a, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int64_t> a = decltype(a)::min() / 2;
        safe_integer<int32_t> b = 2;
        safe_integer<int32_t> c = 3;

        BOOST_CHECK_NO_THROW(a * b);
        BOOST_CHECK_THROW(a * c, underflow_error);

        BOOST_CHECK_NO_THROW(b * a);
        BOOST_CHECK_THROW(c * a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        static_assert(std::is_same_v<decltype(a * b), safe_integer<int64_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<int32_t, 2, 5> a = 2;
        safe_integer<int64_t, -2, 5> b = 0;

        static_assert(std::is_same_v<
            decltype(a * b), safe_integer<int64_t, -10, 25>>);
        static_assert(std::is_same_v<
            decltype(b * a), safe_integer<int64_t, -10, 25>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto const int64_min = std::numeric_limits<int64_t>::min();
        auto const int64_max = std::numeric_limits<int64_t>::max();

        safe_integer<int32_t,              1,              2> a = 1;
        safe_integer<int64_t, int64_min + 10,              1> b = 0;
        safe_integer<int64_t,              0, int64_max - 10> c = 0;
        safe_integer<int64_t, int64_min + 10, int64_max - 10> d = 0;

        static_assert(std::is_same_v<
            decltype(a * b), safe_integer<int64_t, int64_min,         2> >);
        static_assert(std::is_same_v<
            decltype(a * c), safe_integer<int64_t,         0, int64_max> >);
        static_assert(std::is_same_v<
            decltype(a * d), safe_integer<int64_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<int32_t>       a = 1;
        safe_integer<int64_t> const b = 2;

        BOOST_TEST(a * b == 2);
        BOOST_TEST(b * a == 2);
        BOOST_TEST(b * b == 4);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 2;
        safe_integer<int64_t> constexpr b = 3;

        static_assert(a * b == 6);
        static_assert(b * a == 6);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_size_safe_integer

BOOST_AUTO_TEST_SUITE(with_different_sign_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 2;
        safe_integer<uint32_t> b = 3;

        BOOST_TEST(a * b == 6);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = 1;
        safe_integer<uint32_t, 0, 5> b = 3;

        BOOST_TEST(a * b == 3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<uint32_t> a = decltype(a)::max() / 2;
        safe_integer<int32_t> b = 2;
        safe_integer<int32_t> c = 3;

        BOOST_CHECK_NO_THROW(a * b);
        BOOST_CHECK_THROW(a * c, overflow_error);

        BOOST_CHECK_NO_THROW(b * a);
        BOOST_CHECK_THROW(c * a, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<uint32_t> a = 2;
        safe_integer<int32_t> b = 1;
        safe_integer<int32_t> c = -1;

        BOOST_CHECK_NO_THROW(a * b);
        BOOST_CHECK_THROW(a * c, underflow_error);

        BOOST_CHECK_NO_THROW(b * a);
        BOOST_CHECK_THROW(c * a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<uint32_t> b = 2;

        static_assert(std::is_same_v<decltype(a * b), safe_integer<uint32_t> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<uint32_t, 2, 5> a = 2;
        safe_integer<int32_t, -2, 5> b = 0;

        static_assert(std::is_same_v<
            decltype(a * b), safe_integer<uint32_t, 0, 25>>);
        static_assert(std::is_same_v<
            decltype(b * a), safe_integer<uint32_t, 0, 25>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        auto const int32_min = std::numeric_limits<int32_t>::min();
        auto const int32_max = std::numeric_limits<int32_t>::max();
        auto const uint32_max = std::numeric_limits<uint32_t>::max();

        safe_integer<uint32_t,        1,         4> a = 1;
        safe_integer<int32_t, int32_min,         1> b = 0;
        safe_integer<int32_t,         1, int32_max> c = 1;
        safe_integer<int32_t, int32_min, int32_max> d = 0;

        static_assert(std::is_same_v<
            decltype(a * b), safe_integer<uint32_t, 0,          4> >);
        static_assert(std::is_same_v<
            decltype(a * c), safe_integer<uint32_t, 1, uint32_max> >);
        static_assert(std::is_same_v<
            decltype(a * d), safe_integer<uint32_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<int32_t>       a = 1;
        safe_integer<uint32_t> const b = 2;

        BOOST_TEST(a * b == 2);
        BOOST_TEST(b * a == 2);
        BOOST_TEST(b * b == 4);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 2;
        safe_integer<uint32_t> constexpr b = 3;

        static_assert(a * b == 6);
        static_assert(b * a == 6);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_safe_integer

BOOST_AUTO_TEST_SUITE(with_same_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 2;
        int b = 3;
        safe_integer c = 6;

        BOOST_TEST(a * b == c);
        BOOST_TEST(b * a == c);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        int32_t b = 3;

        BOOST_TEST(a * b == -3);
        BOOST_TEST(b * a == -3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int32_t> a = 2;
        safe_integer<int32_t> b = 3;
        int32_t c = std::numeric_limits<int32_t>::max() / 2;

        BOOST_CHECK_NO_THROW(a * c);
        BOOST_CHECK_THROW(b * c, overflow_error);

        BOOST_CHECK_NO_THROW(c * a);
        BOOST_CHECK_THROW(c * b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int32_t> a = 2;
        safe_integer<int32_t> b = 3;
        int32_t c = std::numeric_limits<int32_t>::min() / 2;

        BOOST_CHECK_NO_THROW(a * c);
        BOOST_CHECK_THROW(b * c, underflow_error);

        BOOST_CHECK_NO_THROW(c * a);
        BOOST_CHECK_THROW(c * b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        int32_t b = 2;

        static_assert(std::is_same_v<decltype(a * b), safe_integer<int32_t>>);
        static_assert(std::is_same_v<decltype(b * a), safe_integer<int32_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        safe_integer<int32_t, 2, 5> a = 2;
        int32_t b = 0;

        static_assert(std::is_same_v<
            decltype(a * b), safe_integer<int32_t> >);
        static_assert(std::is_same_v<
            decltype(b * a), safe_integer<int32_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<int32_t> const a = 1;
        safe_integer<int32_t>       b = 1;
        int32_t const c = 2;
        int32_t       d = 2;

        BOOST_TEST(a * c == 2);
        BOOST_TEST(c * a == 2);
        BOOST_TEST(a * d == 2);
        BOOST_TEST(d * a == 2);
        BOOST_TEST(b * c == 2);
        BOOST_TEST(c * b == 2);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 2;
        int32_t constexpr b = 3;

        static_assert(a * b == 6);
        static_assert(b * a == 6);
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_builtin_type

BOOST_AUTO_TEST_SUITE(with_different_size_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 2;
        int64_t b = 3;

        BOOST_TEST(a * b == 6);
        BOOST_TEST(b * a == 6);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = -1;
        int64_t b = 3;

        BOOST_TEST(a * b == -3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int64_t> a = decltype(a)::max() / 2;
        int32_t b = 2;
        int32_t c = 3;

        BOOST_CHECK_NO_THROW(a * b);
        BOOST_CHECK_THROW(a * c, overflow_error);

        BOOST_CHECK_NO_THROW(b * a);
        BOOST_CHECK_THROW(c * a, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int64_t> a = decltype(a)::min() / 2;
        int32_t b = 2;
        int32_t c = 3;

        BOOST_CHECK_NO_THROW(a * b);
        BOOST_CHECK_THROW(a * c, underflow_error);

        BOOST_CHECK_NO_THROW(b * a);
        BOOST_CHECK_THROW(c * a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        int64_t b = 2;

        static_assert(std::is_same_v<decltype(a * b), safe_integer<int64_t>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        auto const int32_min = std::numeric_limits<int32_t>::min();
        auto const int32_max = std::numeric_limits<int32_t>::max();

        safe_integer<int64_t, 2, 5> a = 2;
        int32_t b = 0;

        static_assert(std::is_same_v<
            decltype(a * b),
            safe_integer<int64_t, int32_min * 5L, int32_max * 5L>>);
        static_assert(std::is_same_v<
            decltype(a * b),
            safe_integer<int64_t, int32_min * 5L, int32_max * 5L>>);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        safe_integer<int32_t, 2, 5> a = 2;
        int64_t b = 0;

        static_assert(std::is_same_v<
            decltype(a * b), safe_integer<int64_t> >);
        static_assert(std::is_same_v<
            decltype(a * b), safe_integer<int64_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<int32_t> const a = 1;
        safe_integer<int32_t>       b = 1;
        int64_t const c = 2;
        int64_t       d = 2;

        BOOST_TEST(a * c == 2);
        BOOST_TEST(c * a == 2);
        BOOST_TEST(a * d == 2);
        BOOST_TEST(d * a == 2);
        BOOST_TEST(b * c == 2);
        BOOST_TEST(c * b == 2);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 2;
        int64_t constexpr b = 3;

        static_assert(a * b == 6);
        static_assert(b * a == 6);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_size_builtin_type

BOOST_AUTO_TEST_SUITE(with_different_sign_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 2;
        uint32_t b = 3;

        BOOST_TEST(a * b == 6);
        BOOST_TEST(b * a == 6);

        safe_integer<uint32_t> c = 2;
        int32_t d = 3;

        BOOST_TEST(c * d == 6);
        BOOST_TEST(d * c == 6);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        safe_integer<int32_t, -5, 5> a = 1;
        uint32_t b = 3;

        BOOST_TEST(a * b == 3);
        BOOST_TEST(b * a == 3);

        safe_integer<uint32_t, 0, 5> c = 1;
        int32_t d = 3;

        BOOST_TEST(c * d == 3);
        BOOST_TEST(d * c == 3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int64_t> a = decltype(a)::max() / 2;
        uint32_t b = 2;
        uint32_t c = 3;

        BOOST_CHECK_NO_THROW(a * b);
        BOOST_CHECK_THROW(a * c, overflow_error);

        BOOST_CHECK_NO_THROW(b * a);
        BOOST_CHECK_THROW(c * a, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int64_t> a = decltype(a)::min() / 2;
        uint32_t b = 2;
        uint32_t c = 3;

        BOOST_CHECK_NO_THROW(a * b);
        BOOST_CHECK_THROW(a * c, underflow_error);

        BOOST_CHECK_NO_THROW(b * a);
        BOOST_CHECK_THROW(c * a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        uint32_t b = 2;

        static_assert(std::is_same_v<decltype(a * b), safe_integer<uint32_t> >);
        static_assert(std::is_same_v<decltype(b * a), safe_integer<uint32_t> >);

        safe_integer<uint32_t> c = 1;
        int32_t d = 2;

        static_assert(std::is_same_v<decltype(c * d), safe_integer<uint32_t> >);
        static_assert(std::is_same_v<decltype(d * c), safe_integer<uint32_t> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type)
    {
        auto const int32_max = std::numeric_limits<int32_t>::max();

        safe_integer<uint32_t, 0, 1> a = 1;
        int32_t b = 0;

        static_assert(std::is_same_v<
            decltype(a * b), safe_integer<uint32_t, 0, int32_max> >);
        static_assert(std::is_same_v<
            decltype(b * a), safe_integer<uint32_t, 0, int32_max> >);
    }

    BOOST_AUTO_TEST_CASE(bounded_return_type_saturation)
    {
        safe_integer<int32_t, 2, 5> a = 2;
        uint32_t b = 0;

        static_assert(std::is_same_v<
            decltype(a * b), safe_integer<uint32_t> >);
        static_assert(std::is_same_v<
            decltype(b * a), safe_integer<uint32_t> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer<int32_t> const a = 1;
        safe_integer<int32_t>       b = 1;
        uint32_t const c = 2;
        uint32_t       d = 2;

        BOOST_TEST(a * c == 2);
        BOOST_TEST(c * a == 2);
        BOOST_TEST(a * d == 2);
        BOOST_TEST(d * a == 2);
        BOOST_TEST(b * c == 2);
        BOOST_TEST(c * b == 2);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<int32_t> constexpr a = 2;
        uint32_t constexpr b = 3;

        static_assert(a * b == 6);
        static_assert(b * a == 6);
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_builtin_type

template<typename T, typename U>
using result_t = decltype(std::declval<T>() * std::declval<U>());

template<typename T, typename U>
using safe_integer_result_t =
    typename result_t<safe_integer<T>, safe_integer<U>>::value_type;

template<typename T, typename U>
using builtin_result_t = result_t<T, U>;

template<typename T, typename U>
auto constexpr compare_result_v = std::is_same_v<
        safe_integer_result_t<T, U>,
        builtin_result_t<T, U>
    >;

BOOST_AUTO_TEST_CASE(same_arithmetic_type_conversion_as_builtin_types)
{
    static_assert(compare_result_v<bool, bool>);
    static_assert(compare_result_v<char, char>);
    static_assert(compare_result_v<short, short>);
    static_assert(compare_result_v<int, int>);
    static_assert(compare_result_v<int, long>);
    static_assert(compare_result_v<int, long long>);
    static_assert(compare_result_v<int, unsigned int>);
    static_assert(compare_result_v<int, unsigned long>);
    static_assert(compare_result_v<int, unsigned long long>);
    static_assert(compare_result_v<long, long>);
    static_assert(compare_result_v<long, long long>);
    static_assert(compare_result_v<long, unsigned int>);
    static_assert(compare_result_v<long, unsigned long>);
    static_assert(compare_result_v<long, unsigned long long>);
    static_assert(compare_result_v<long long, long long>);
    static_assert(compare_result_v<long long, unsigned int>);
    static_assert(compare_result_v<long long, unsigned long>);
    static_assert(compare_result_v<long long, unsigned long long>);
    static_assert(compare_result_v<unsigned int, unsigned int>);
    static_assert(compare_result_v<unsigned int, unsigned long>);
    static_assert(compare_result_v<unsigned int, unsigned long long>);
    static_assert(compare_result_v<unsigned long, unsigned long>);
    static_assert(compare_result_v<unsigned long, unsigned long long>);
    static_assert(compare_result_v<unsigned long long, unsigned long long>);
}

BOOST_AUTO_TEST_SUITE_END() // multiplication

BOOST_AUTO_TEST_SUITE_END() // arithmetic

