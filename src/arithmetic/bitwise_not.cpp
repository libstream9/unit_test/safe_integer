#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(bitwise_not)

    BOOST_AUTO_TEST_CASE(signed_value)
    {
        safe_integer<int> a = 1;
        int b = 1;

        BOOST_TEST(~a == ~b);
    }

    BOOST_AUTO_TEST_CASE(unsigned_value)
    {
        safe_integer<unsigned> a = 1;
        unsigned b = 1;

        BOOST_TEST(~a == ~b);
    }

    template<typename T>
    using result_t =
        typename decltype(~std::declval<safe_integer<T>>())::value_type;

    template<typename T>
    inline constexpr auto same_result_type_v =
        std::is_same_v<result_t<T>, decltype(~std::declval<T>())>;


    BOOST_AUTO_TEST_CASE(result_type)
    {
        //TODO static_assert(same_result_type_v<bool>);
        static_assert(same_result_type_v<char>);
        static_assert(same_result_type_v<short>);
        static_assert(same_result_type_v<int>);
        static_assert(same_result_type_v<long>);
        static_assert(same_result_type_v<long long>);
        static_assert(same_result_type_v<unsigned char>);
        static_assert(same_result_type_v<unsigned short>);
        static_assert(same_result_type_v<unsigned int>);
        static_assert(same_result_type_v<unsigned long>);
        static_assert(same_result_type_v<unsigned long long>);
    }

BOOST_AUTO_TEST_SUITE_END() // bitwise_not
