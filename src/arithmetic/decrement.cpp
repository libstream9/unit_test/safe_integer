#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic)

BOOST_AUTO_TEST_SUITE(decrement)

BOOST_AUTO_TEST_SUITE(prefix)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 1;

        --a;

        BOOST_TEST(a == 0);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int> a = decltype(a)::min() + 1;

        BOOST_CHECK_NO_THROW(--a);
        BOOST_CHECK_THROW(--a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow)
    {
        safe_integer<int, 0, 1> a = 1;

        BOOST_CHECK_NO_THROW(--a);
        BOOST_CHECK_THROW(--a, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer a = 1;

        static_assert(std::is_same_v<decltype(--a), decltype(a)&>);
    }

    BOOST_AUTO_TEST_CASE(on_const)
    {
        // this has to be a compile error.
        //safe_integer<int> const a = 1;
        //--a;
    }

    BOOST_AUTO_TEST_CASE(on_constexpr)
    {
        // this has to be a compile error.
        //safe_integer<int> constexpr a = 1;
        //--a;
    }

BOOST_AUTO_TEST_SUITE_END() // prefix

BOOST_AUTO_TEST_SUITE(postfix)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer a = 1;

        BOOST_TEST(a-- == 1);
        BOOST_TEST(a == 0);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int> a = decltype(a)::min() + 1;

        BOOST_CHECK_NO_THROW(a--);
        BOOST_CHECK_THROW(a--, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow)
    {
        safe_integer<int, 0, 1> a = 1;

        BOOST_CHECK_NO_THROW(a--);
        BOOST_CHECK_THROW(a--, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer a = 1;

        static_assert(std::is_same_v<decltype(a--), decltype(a)>);
    }

    BOOST_AUTO_TEST_CASE(on_const)
    {
        // this has to be a compile error.
        //safe_integer<int> const a = 1;
        //--a;
    }

    BOOST_AUTO_TEST_CASE(on_constexpr)
    {
        // this has to be a compile error.
        //safe_integer<int> constexpr a = 1;
        //--a;
    }

BOOST_AUTO_TEST_SUITE_END() // postfix

BOOST_AUTO_TEST_SUITE_END() // decrement

BOOST_AUTO_TEST_SUITE_END() // arithmetic
