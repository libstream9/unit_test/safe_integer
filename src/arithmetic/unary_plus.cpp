#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(unary_plus)

    BOOST_AUTO_TEST_CASE(signed_)
    {
        safe_integer a = 1;

        BOOST_TEST(+a == a);
    }

    BOOST_AUTO_TEST_CASE(unsigned_)
    {
        safe_integer a = 1u;

        BOOST_TEST(+a == a);
    }

    BOOST_AUTO_TEST_CASE(bounded)
    {
        auto a = make_safe_integer<10, 20>(15);

        BOOST_TEST(+a == a);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer a = 1;

        static_assert(std::is_same_v<decltype(+a), decltype(a)>);

        safe_integer<int, 10, 20> b = 15;

        static_assert(std::is_same_v<decltype(+b), decltype(b)>);

        // integral promotion
        safe_integer<int16_t> c = 1;

        static_assert(std::is_same_v<
                decltype(+c), safe_integer<int, INT16_MIN, INT16_MAX> >);

        safe_integer<uint16_t> d = 1;

        static_assert(std::is_same_v<
            decltype(+d), safe_integer<int, 0, UINT16_MAX> >);
    }

    BOOST_AUTO_TEST_CASE(as_const)
    {
        safe_integer const a = 1;

        BOOST_TEST(+a == a);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer constexpr a = 1;

        static_assert(+a == a);
    }

    template<typename T>
    using result_t = decltype(+T());

    template<typename T>
    using safe_integer_result_t =
        typename result_t<safe_integer<T>>::value_type;

    template<typename T>
    using builtin_result_t = result_t<T>;

    template<typename T>
    auto constexpr compare_result_v = std::is_same_v<
            safe_integer_result_t<T>,
            builtin_result_t<T>
        >;

    BOOST_AUTO_TEST_CASE(same_result_type_as_builtin_type_arithmetic)
    {
        static_assert(compare_result_v<bool>);
        static_assert(compare_result_v<char>);
        static_assert(compare_result_v<short>);
        static_assert(compare_result_v<int>);
        static_assert(compare_result_v<long>);
        static_assert(compare_result_v<long long>);
        static_assert(compare_result_v<unsigned char>);
        static_assert(compare_result_v<unsigned short>);
        static_assert(compare_result_v<unsigned int>);
        static_assert(compare_result_v<unsigned long>);
        static_assert(compare_result_v<unsigned long long>);
    }

BOOST_AUTO_TEST_SUITE_END() // unary_plus
