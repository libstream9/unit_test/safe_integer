#include <boost/test/unit_test.hpp>

#include <climits>
#include <type_traits>

#include <stream9/safe_integer/safe_integer.hpp>
#include <stream9/safe_integer/arithmetic/promote.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic)

BOOST_AUTO_TEST_SUITE(promote_)

    BOOST_AUTO_TEST_CASE(bool_)
    {
        safe_integer<bool> a = true;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<int, 0, 1>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(char_)
    {
        safe_integer<char> a = 1;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<int, CHAR_MIN, CHAR_MAX>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(unsigned_char_)
    {
        safe_integer<unsigned char> a = 1;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<int, 0, UCHAR_MAX>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(short_)
    {
        safe_integer<short> a = 1;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<int, SHRT_MIN, SHRT_MAX>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(unsigned_short_)
    {
        safe_integer<unsigned short> a = 1;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<int, 0, USHRT_MAX>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(int_)
    {
        safe_integer<int> a = 1;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<int>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(unsigned_int_)
    {
        safe_integer<unsigned int> a = 1;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<unsigned int>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(long_)
    {
        safe_integer<long> a = 1;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<long>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(unsigned_long_)
    {
        safe_integer<unsigned long> a = 1;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<unsigned long>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(long_long_)
    {
        safe_integer<long long> a = 1;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<long long>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(unsigned_long_long_)
    {
        safe_integer<unsigned long long> a = 1;

        auto b = promote(a);

        static_assert(std::is_same_v<
                decltype(b), safe_integer<unsigned long long>>);

        BOOST_TEST(b == a);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        safe_integer<char> constexpr a = 1;

        auto constexpr b = promote(a);

        static_assert(b == a);
    }

BOOST_AUTO_TEST_SUITE_END() // promote_

BOOST_AUTO_TEST_SUITE_END() // arithmetic
