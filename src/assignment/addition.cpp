#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(assignment)

BOOST_AUTO_TEST_SUITE(addition)

BOOST_AUTO_TEST_SUITE(with_same_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        using integer = safe_integer<int>;

        integer a = 1;
        integer b = 2;

        a += b;

        BOOST_TEST(a == 3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = integer::max() - 1;
        integer b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = integer::min() + 1;
        integer b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow)
    {
        using integer = safe_integer<int32_t, 1, 2>;

        integer a = 1;
        integer b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow)
    {
        using integer = safe_integer<int32_t, -2, -1>;

        integer a = -1;
        integer b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        using integer = safe_integer<int>;

        integer a = 1;
        integer b = 2;

        static_assert(std::is_same_v<decltype(a += b), integer&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_safe_integer

BOOST_AUTO_TEST_SUITE(with_different_size_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        a += b;

        BOOST_TEST(a == 3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int64_t> a = decltype(a)::max() - 1;
        safe_integer<int32_t> b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int64_t> a = decltype(a)::min() + 1;
        safe_integer<int32_t> b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow_1)
    {
        safe_integer<int32_t, 1, 2> a = 1;
        safe_integer<int64_t> b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow_2)
    {
        safe_integer<int32_t> a = decltype(a)::max() - 1;
        safe_integer<int64_t> b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow_1)
    {
        safe_integer<int32_t, -2, -1> a = -1;
        safe_integer<int64_t> b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow_2)
    {
        safe_integer<int32_t> a = decltype(a)::min() + 1;
        safe_integer<int64_t> b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        static_assert(std::is_same_v<decltype(a += b), decltype(a)&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_size_safe_integer

BOOST_AUTO_TEST_SUITE(with_different_sign_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<uint32_t> b = 2;

        a += b;

        BOOST_TEST(a == 3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<uint32_t> a = decltype(a)::max() - 1;
        safe_integer<int32_t> b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        // impossible
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow_1)
    {
        safe_integer<int32_t, 1, 2> a = 1;
        safe_integer<uint32_t> b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow_2)
    {
        safe_integer<int32_t> a = decltype(a)::max() - 1;
        safe_integer<uint32_t> b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow_1)
    {
        safe_integer<uint32_t, 1, 2> a = 2;
        safe_integer<int64_t> b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow_2)
    {
        safe_integer<uint32_t> a = 1;
        safe_integer<int64_t> b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<uint32_t> b = 2;

        static_assert(std::is_same_v<decltype(a += b), decltype(a)&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_safe_integer

BOOST_AUTO_TEST_SUITE(with_same_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        using integer = safe_integer<int>;

        integer a = 1;
        int b = 2;

        a += b;

        BOOST_TEST(a == 3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = integer::max() - 1;
        int32_t b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        using integer = safe_integer<int32_t>;

        integer a = integer::min() + 1;
        int32_t b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow)
    {
        using integer = safe_integer<int32_t, 1, 2>;

        integer a = 1;
        int32_t b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow)
    {
        using integer = safe_integer<int32_t, -2, -1>;

        integer a = -1;
        int32_t b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        using integer = safe_integer<int>;

        integer a = 1;
        int b = 2;

        static_assert(std::is_same_v<decltype(a += b), integer&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_builtin_type

BOOST_AUTO_TEST_SUITE(with_different_size_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 1;
        int64_t b = 2;

        a += b;

        BOOST_TEST(a == 3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<int64_t> a = decltype(a)::max() - 1;
        int32_t b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        safe_integer<int64_t> a = decltype(a)::min() + 1;
        int32_t b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow_1)
    {
        safe_integer<int32_t, 1, 2> a = 1;
        int64_t b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow_2)
    {
        safe_integer<int32_t> a = decltype(a)::max() - 1;
        int64_t b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow_1)
    {
        safe_integer<int32_t, -2, -1> a = -1;
        int64_t b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow_2)
    {
        safe_integer<int32_t> a = decltype(a)::min() + 1;
        int64_t b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        int64_t b = 2;

        static_assert(std::is_same_v<decltype(a += b), decltype(a)&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_size_builtin_type

BOOST_AUTO_TEST_SUITE(with_different_sign_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 1;
        uint32_t b = 2;

        a += b;

        BOOST_TEST(a == 3);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        safe_integer<uint32_t> a = decltype(a)::max() - 1;
        int32_t b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_underflow)
    {
        // impossible
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow_1)
    {
        safe_integer<int32_t, 1, 2> a = 1;
        uint32_t b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow_2)
    {
        safe_integer<int32_t> a = decltype(a)::max() - 1;
        uint32_t b = 1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow_1)
    {
        safe_integer<uint32_t, 1, 2> a = 2;
        int64_t b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow_2)
    {
        safe_integer<uint32_t> a = 1;
        int64_t b = -1;

        BOOST_CHECK_NO_THROW(a += b);
        BOOST_CHECK_THROW(a += b, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        uint32_t b = 2;

        static_assert(std::is_same_v<decltype(a += b), decltype(a)&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_builtin_type

BOOST_AUTO_TEST_SUITE_END() // addition

BOOST_AUTO_TEST_SUITE_END() // assignment
