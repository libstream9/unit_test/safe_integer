#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(assignment)

BOOST_AUTO_TEST_SUITE(remainder)

BOOST_AUTO_TEST_SUITE(with_same_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        using integer = safe_integer<int>;

        integer a = 2;
        integer b = 2;

        a %= b;

        BOOST_TEST(a == 0);
    }

    BOOST_AUTO_TEST_CASE(divided_by_zero)
    {
        safe_integer a = 3;
        safe_integer b = 2;
        safe_integer c = 0;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow)
    {
        using integer = safe_integer<int32_t, -7, -2>;

        integer a = -7;
        integer b = -5;
        integer c = -2;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow)
    {
        using integer = safe_integer<int32_t, 2, 6>;

        integer a = 6;
        integer b = 4;
        integer c = 2;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        using integer = safe_integer<int>;

        integer a = 2;
        integer b = 2;

        static_assert(std::is_same_v<decltype(a %= b), integer&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_safe_integer

BOOST_AUTO_TEST_SUITE(with_different_size_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        a %= b;

        BOOST_TEST(a == 1);
    }

    BOOST_AUTO_TEST_CASE(divided_by_zero)
    {
        safe_integer<int32_t> a = 3;
        safe_integer<int64_t> b = 2;
        safe_integer<int64_t> c = 0;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow)
    {
        safe_integer<int32_t, -4, -1> a = -4;
        safe_integer<int64_t> b = -3;
        safe_integer<int64_t> c = -1;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow)
    {
        safe_integer<int32_t, 1, 4> a = 4;
        safe_integer<int64_t> b = 3;
        safe_integer<int64_t> c = 1;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t> b = 2;

        static_assert(std::is_same_v<decltype(a %= b), decltype(a)&>);
        static_assert(std::is_same_v<decltype(b %= a), decltype(b)&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_size_safe_integer

BOOST_AUTO_TEST_SUITE(with_different_sign_safe_integer)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 2;
        safe_integer<uint32_t> b = 2;

        a %= b;

        BOOST_TEST(a == 0);
    }

    BOOST_AUTO_TEST_CASE(divided_by_zero)
    {
        safe_integer<int32_t> a = 3;
        safe_integer<uint32_t> b = 2;
        safe_integer<uint32_t> c = 0;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        // impossible
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow)
    {
        // impossible
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow)
    {
        safe_integer<int32_t, 2, 6> a = 6;
        safe_integer<uint32_t> b = 4;
        safe_integer<uint32_t> c = 2;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<uint32_t> b = 2;

        static_assert(std::is_same_v<decltype(a %= b), decltype(a)&>);
        static_assert(std::is_same_v<decltype(b %= a), decltype(b)&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_safe_integer

BOOST_AUTO_TEST_SUITE(with_same_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int> a = 1;
        int b = 2;

        a %= b;

        BOOST_TEST(a == 1);
    }

    BOOST_AUTO_TEST_CASE(divided_by_zero)
    {
        safe_integer<int> a = 3;
        int b = 2;
        int c = 0;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow)
    {
        safe_integer<int, -7, -2> a = -7;
        int b = -5;
        int c = -2;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow)
    {
        safe_integer<int, 2, 6> a = 6;
        int b = 4;
        int c = 2;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int> a = 1;
        int b = 2;

        static_assert(std::is_same_v<decltype(a %= b), decltype(a)&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_same_builtin_type

BOOST_AUTO_TEST_SUITE(with_different_size_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 4;
        int64_t b = 2;

        a %= b;

        BOOST_TEST(a == 0);
    }

    BOOST_AUTO_TEST_CASE(divided_by_zero)
    {
        safe_integer<int32_t> a = 3;
        int64_t b = 2;
        int64_t c = 0;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow)
    {
        safe_integer<int32_t, -4, -1> a = -4;
        int64_t b = -3;
        int64_t c = -1;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, overflow_error);
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow)
    {
        safe_integer<int32_t, 1, 4> a = 4;
        int64_t b = 3;
        int64_t c = 1;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        int64_t b = 2;

        static_assert(std::is_same_v<decltype(a %= b), decltype(a)&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_size_builtin_type

BOOST_AUTO_TEST_SUITE(with_different_sign_builtin_type)

    BOOST_AUTO_TEST_CASE(normal)
    {
        safe_integer<int32_t> a = 4;
        uint32_t b = 2;

        a %= b;

        BOOST_TEST(a == 0);
    }

    BOOST_AUTO_TEST_CASE(divided_by_zero)
    {
        safe_integer<int32_t> a = 3;
        uint32_t b = 2;
        uint32_t c = 0;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, divided_by_zero_error);
    }

    BOOST_AUTO_TEST_CASE(arithmetic_overflow)
    {
        // impossible
    }

    BOOST_AUTO_TEST_CASE(assignment_overflow)
    {
        // impossible
    }

    BOOST_AUTO_TEST_CASE(assignment_underflow)
    {
        safe_integer<int32_t, 2, 6> a = 6;
        uint32_t b = 4;
        uint32_t c = 2;

        BOOST_CHECK_NO_THROW(a %= b);
        BOOST_CHECK_THROW(a %= c, underflow_error);
    }

    BOOST_AUTO_TEST_CASE(return_type)
    {
        safe_integer<int32_t> a = 1;
        uint32_t b = 2;

        static_assert(std::is_same_v<decltype(a %= b), decltype(a)&>);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        // assignment can not be a constexpr
    }

BOOST_AUTO_TEST_SUITE_END() // with_different_sign_builtin_type

BOOST_AUTO_TEST_SUITE_END() // remainder

BOOST_AUTO_TEST_SUITE_END() // assignment
