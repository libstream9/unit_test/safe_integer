#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/arithmetic/safe_multiply.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic)

BOOST_AUTO_TEST_SUITE(safe_multiply_)

BOOST_AUTO_TEST_SUITE(signed_signed)

    BOOST_AUTO_TEST_CASE(normal)
    {
        auto a = safe_multiply<int>(1, 2);

        BOOST_TEST(a);
        BOOST_TEST(*a == 2);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        auto a = safe_multiply<int>(INT32_MAX, 2);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        auto a = safe_multiply<int>(INT32_MIN, 2);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_multiply<int>(1, 2);

        static_assert(a);
        static_assert(*a == 2);

        auto constexpr b = safe_multiply<int>(INT32_MIN, 2);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::underflow);
    }

BOOST_AUTO_TEST_SUITE_END() // signed_signed

BOOST_AUTO_TEST_SUITE(signed_unsigned)

    BOOST_AUTO_TEST_CASE(normal)
    {
        auto a = safe_multiply<int>(1, 2u);

        BOOST_TEST(a);
        BOOST_TEST(*a == 2);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        auto a = safe_multiply<int16_t>(INT32_MAX, 2u);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        auto a = safe_multiply<int32_t>(INT32_MIN, 2u);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_multiply<int>(1, 2u);

        static_assert(a);
        static_assert(*a == 2);

        auto constexpr b = safe_multiply<int16_t>(INT32_MAX, 2u);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // signed_unsigned

BOOST_AUTO_TEST_SUITE(unsigned_signed)

    BOOST_AUTO_TEST_CASE(normal)
    {
        auto a = safe_multiply<int>(1u, 2);

        BOOST_TEST(a);
        BOOST_TEST(*a == 2);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        auto a = safe_multiply<uint32_t>(UINT32_MAX, 2);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        auto a = safe_multiply<uint32_t>(5u, -2);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_multiply<int>(1u, 2);

        static_assert(a);
        static_assert(*a == 2);

        auto constexpr b = safe_multiply<uint32_t>(UINT32_MAX, 2);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // unsigned_signed

BOOST_AUTO_TEST_SUITE(unsinged_unsigned)

    BOOST_AUTO_TEST_CASE(normal)
    {
        auto a = safe_multiply<int>(2u, 2u);

        BOOST_TEST(a);
        BOOST_TEST(*a == 4);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        auto a = safe_multiply<uint32_t>(UINT32_MAX, 2u);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_multiply<int>(2u, 2u);

        static_assert(a);
        static_assert(*a == 4);

        auto constexpr b = safe_multiply<uint32_t>(UINT32_MAX, 2u);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // unsinged_unsigned

BOOST_AUTO_TEST_SUITE(option)

    BOOST_AUTO_TEST_CASE(no_overflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_overflow;

        auto a  = safe_multiply<int32_t, opt>(INT32_MAX, 2);
        BOOST_TEST(a);
        // value is undefined

        auto b  = safe_multiply<int32_t, opt>(INT32_MIN, 2);
        BOOST_TEST(!b);
        BOOST_TEST(b.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(no_underflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_underflow;

        auto a  = safe_multiply<int32_t, opt>(INT32_MIN, 2);
        BOOST_TEST(a);
        // value is undefined

        auto b  = safe_multiply<int32_t, opt>(INT32_MAX, 2);
        BOOST_TEST(!b);
        BOOST_TEST(b.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(no_overflow_underflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_overflow
                           | arithmetic_option::dont_check_underflow;

        auto a  = safe_multiply<int32_t, opt>(INT32_MAX, 2);
        BOOST_TEST(a);
        // value is undefined

        auto b  = safe_multiply<int32_t, opt>(INT32_MIN, 2);
        BOOST_TEST(b);
        // value is undefined
    }

BOOST_AUTO_TEST_SUITE_END() // option

BOOST_AUTO_TEST_SUITE_END() // safe_multiply_

BOOST_AUTO_TEST_SUITE_END() // arithmetic
