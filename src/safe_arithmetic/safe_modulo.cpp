#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/arithmetic/safe_modulo.hpp>

#include <iostream>
#include <cstdint>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(safe_arithmetic)

BOOST_AUTO_TEST_SUITE(safe_modulo_)

BOOST_AUTO_TEST_SUITE(signed_signed)

    BOOST_AUTO_TEST_CASE(normal)
    {
        auto a = safe_modulo<int>(5, 2);

        BOOST_TEST(a);
        BOOST_TEST(*a == 1);
    }

    BOOST_AUTO_TEST_CASE(wont_be_problem_unlike_builtin_arithmetic)
    {
        auto a = safe_modulo<int32_t>(INT32_MIN, -1);

        BOOST_TEST(a);
        BOOST_TEST(*a == 0);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        auto a = safe_modulo<int16_t>(INT32_MAX - 1, INT32_MAX);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        auto a = safe_modulo<int16_t>(INT32_MIN + 1, INT32_MIN);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(divide_by_zero)
    {
        int32_t a = 100;
        int32_t b = 0;

        auto c = safe_modulo<int32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::divide_by_zero);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_modulo<int>(1, 2);

        static_assert(a);
        static_assert(*a == 1);

        auto constexpr b = safe_modulo<int16_t>(INT32_MAX - 1, INT32_MAX);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // signed_signed

BOOST_AUTO_TEST_SUITE(signed_unsigned)

    BOOST_AUTO_TEST_CASE(normal)
    {
        auto a = safe_modulo<int>(5, 2u);

        BOOST_TEST(a);
        BOOST_TEST(*a == 1);
    }

    BOOST_AUTO_TEST_CASE(wont_be_problem_unlike_builtin_arithmetic)
    {
        auto a = safe_modulo<unsigned>(-2, 2u);

        BOOST_TEST(a);
        BOOST_TEST(*a == 0);

        auto b = safe_modulo<int>(-3, 2u);

        BOOST_TEST(b);
        BOOST_TEST(*b == -1);

        auto c = safe_modulo<int>(-1, 2ull);

        BOOST_TEST(c);
        BOOST_TEST(*c == -1);

        if (is_twos_complement_integer_v<int64_t>) {
            auto d = safe_modulo<int>(INT64_MIN,
                                      static_cast<uint64_t>(INT64_MAX) + 1u);
            BOOST_TEST(d);
            BOOST_TEST(*d == 0);

            auto e = safe_modulo<int64_t>(
                           INT64_MIN, static_cast<uint64_t>(INT64_MAX) + 2u);
            BOOST_TEST(e);
            BOOST_TEST(*e == INT64_MIN);
        }
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        auto a = safe_modulo<int16_t>(INT32_MAX, UINT32_MAX);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        int32_t a = INT32_MIN + 5;
        uint32_t b = static_cast<uint32_t>(INT32_MAX);

        auto c = safe_modulo<int16_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(divide_by_zero)
    {
        int32_t a = 100;
        uint32_t b = 0;

        auto c = safe_modulo<uint32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::divide_by_zero);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_modulo<int>(1, 2u);

        static_assert(a);
        static_assert(*a == 1);

        auto constexpr b = safe_modulo<int16_t>(INT32_MAX, UINT32_MAX);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // signed_unsigned

BOOST_AUTO_TEST_SUITE(unsigned_signed)

    BOOST_AUTO_TEST_CASE(normal)
    {
        auto a = safe_modulo<int>(5u, 2);

        BOOST_TEST(a);
        BOOST_TEST(*a == 1);
    }

    BOOST_AUTO_TEST_CASE(wont_be_problem_unlike_builtin_arithmetic)
    {
        auto a = safe_modulo<int>(2u, -2);

        BOOST_TEST(a);
        BOOST_TEST(*a == 0);

        auto b = safe_modulo<int>(3u, -2);

        BOOST_TEST(b);
        BOOST_TEST(*b == 1);

        auto c = safe_modulo<int>(1u, -2);

        BOOST_TEST(c);
        BOOST_TEST(*c == 1);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        uint32_t a = static_cast<uint32_t>(INT32_MAX - 1);
        int32_t b = INT32_MAX;

        auto c = safe_modulo<int16_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
    }

    BOOST_AUTO_TEST_CASE(divide_by_zero)
    {
        uint32_t a = 100;
        int32_t b = 0;

        auto c = safe_modulo<int32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::divide_by_zero);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_modulo<int>(7u, 3);

        static_assert(a);
        static_assert(*a == 1);

        uint32_t constexpr b = static_cast<uint32_t>(INT32_MAX - 1);
        int32_t constexpr c = INT32_MAX;

        auto constexpr d = safe_modulo<int16_t>(b, c);

        BOOST_TEST(!d);
        BOOST_TEST(d.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // unsigned_signed

BOOST_AUTO_TEST_SUITE(unsinged_unsigned)

    BOOST_AUTO_TEST_CASE(normal)
    {
        auto a = safe_modulo<int>(1u, 2u);

        BOOST_TEST(a);
        BOOST_TEST(*a == 1);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        auto a = safe_modulo<int16_t>(UINT32_MAX - 1, UINT32_MAX);

        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
    }

    BOOST_AUTO_TEST_CASE(divide_by_zero)
    {
        uint32_t a = 100;
        uint32_t b = 0;

        auto c = safe_modulo<int32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::divide_by_zero);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_modulo<int>(1u, 2u);

        static_assert(a);
        static_assert(*a == 1);

        auto constexpr b = safe_modulo<int16_t>(UINT32_MAX - 1, UINT32_MAX);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // unsinged_unsigned

BOOST_AUTO_TEST_SUITE(option)

    BOOST_AUTO_TEST_CASE(no_overflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_overflow;

        auto a  = safe_modulo<int16_t, opt>(INT32_MAX - 1, INT32_MAX);
        BOOST_TEST(a);
        // value is undefined

        auto b  = safe_modulo<int16_t, opt>(INT32_MIN + 1, INT32_MIN);
        BOOST_TEST(!b);
        BOOST_TEST(b.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(no_underflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_underflow;

        auto a  = safe_modulo<int16_t, opt>(INT32_MAX - 1, INT32_MAX);
        BOOST_TEST(!a);
        BOOST_TEST(a.error() == arithmetic_errc::overflow);

        auto b  = safe_modulo<int16_t, opt>(INT32_MIN + 1, INT32_MIN);
        BOOST_TEST(b);
        // value is undefined
    }

    BOOST_AUTO_TEST_CASE(no_overflow_underflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_overflow
                           | arithmetic_option::dont_check_underflow;

        auto a  = safe_modulo<int16_t, opt>(INT32_MAX - 1, INT32_MAX);
        BOOST_TEST(a);
        // value is undefined

        auto b  = safe_modulo<int16_t, opt>(INT32_MIN + 1, INT32_MIN);
        BOOST_TEST(b);
        // value is undefined
    }

    BOOST_AUTO_TEST_CASE(no_zero_division_check, *boost::unit_test::disabled())
    {
        auto constexpr opt = arithmetic_option::dont_check_zero_division;

        // This should cause runtime error
        auto a = safe_modulo<int32_t, opt>(100, 0);
        BOOST_TEST(a);
        // value is undefined
    }

BOOST_AUTO_TEST_SUITE_END() // option

BOOST_AUTO_TEST_SUITE_END() // safe_divide_

BOOST_AUTO_TEST_SUITE_END() // safe_arithmetic
