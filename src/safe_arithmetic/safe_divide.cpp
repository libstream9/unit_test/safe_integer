#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/arithmetic/safe_divide.hpp>

#include <cstdint>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(safe_arithmetic)

BOOST_AUTO_TEST_SUITE(safe_divide_)

BOOST_AUTO_TEST_SUITE(signed_signed)

    BOOST_AUTO_TEST_CASE(positive_positive)
    {
        int64_t a = 1000;
        int64_t b = 500;

        auto c = safe_divide<int64_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 2);
    }

    BOOST_AUTO_TEST_CASE(positive_negative)
    {
        int64_t a = 1000;
        int64_t b = -500;

        auto c = safe_divide<int64_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == -2);
    }

    BOOST_AUTO_TEST_CASE(negative_positive)
    {
        int64_t a = -1000;
        int64_t b = 500;

        auto c = safe_divide<int64_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == -2);
    }

    BOOST_AUTO_TEST_CASE(negative_negative)
    {
        int64_t a = -1000;
        int64_t b = 500;

        auto c = safe_divide<int64_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == -2);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        int64_t a = INT64_MAX;
        int64_t b = 2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        int32_t a = 1;
        int32_t b = -1;

        auto c = safe_divide<uint32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(divide_by_zero)
    {
        int32_t a = 100;
        int32_t b = 0;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::divide_by_zero);
    }

    BOOST_AUTO_TEST_CASE(signed_min_divide_by_minus_one)
    {
        if constexpr (is_twos_complement_integer_v<int64_t>) {
            int64_t a = INT64_MIN;
            int32_t b = -1;

            auto c = safe_divide<int64_t>(a, b);

            BOOST_TEST(!c);
            BOOST_TEST(c.error() == arithmetic_errc::overflow);

            auto d = safe_divide<uint64_t>(a, b);

            BOOST_TEST(d);
            BOOST_TEST(*d == INT64_MAX + 1Lu);
        }
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_divide<int>(4, 2);

        static_assert(a);
        static_assert(*a == 2);

        auto constexpr b = safe_divide<int32_t>(INT64_MAX, 2);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // signed_signed

BOOST_AUTO_TEST_SUITE(unsigned_unsigned)

    BOOST_AUTO_TEST_CASE(normal)
    {
        uint64_t a = 1000;
        uint64_t b = 500;

        auto c = safe_divide<uint64_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 2);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        uint64_t a = UINT64_MAX;
        uint64_t b = 2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(divide_by_zero)
    {
        uint32_t a = 100;
        uint32_t b = 0;

        auto c = safe_divide<uint32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::divide_by_zero);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_divide<int>(4u, 2u);

        static_assert(a);
        static_assert(*a == 2u);

        auto constexpr b = safe_divide<int32_t>(UINT64_MAX, 2u);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // unsigned_unsigned

BOOST_AUTO_TEST_SUITE(signed_unsigned)

    BOOST_AUTO_TEST_CASE(positive_positive_unsigned_common_type)
    {
        int32_t a = 100;
        uint32_t b = 2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 50);
    }

    BOOST_AUTO_TEST_CASE(positive_positive_signed_common_type)
    {
        int64_t a = 100;
        uint32_t b = 2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 50);
    }

    BOOST_AUTO_TEST_CASE(negative_positive_unsigned_common_type)
    {
        int32_t a = -100;
        uint32_t b = 2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == -50);
    }

    BOOST_AUTO_TEST_CASE(negative_positive_signed_common_type)
    {
        int64_t a = -100;
        uint32_t b = 2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == -50);
    }

    BOOST_AUTO_TEST_CASE(negative_positive_uint64_t_common_type)
    {
        int32_t a = -100;
        uint64_t b = 2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == -50);
    }

    BOOST_AUTO_TEST_CASE(negative_positive_corner_case_1)
    {
        int64_t a = INT64_MIN;
        uint64_t b = UINT64_MAX;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 0);
    }

    BOOST_AUTO_TEST_CASE(negative_positive_corner_case_2)
    {
        if (is_twos_complement_integer_v<int64_t>) {
            int64_t a = INT64_MIN;
            uint64_t b = INT64_MAX + 1ul;

            auto c = safe_divide<int32_t>(a, b);

            BOOST_TEST(c);
            BOOST_TEST(*c == -1);
        }
    }

    BOOST_AUTO_TEST_CASE(positive_positive_unsigned_result)
    {
        int32_t a = 100;
        uint32_t b = 2;

        auto c = safe_divide<uint32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 50);
    }

    BOOST_AUTO_TEST_CASE(negative_positive_unsigned_result)
    {
        int32_t a = -100;
        uint32_t b = 2;

        auto c = safe_divide<uint32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        int64_t a = INT64_MAX;
        uint64_t b = 2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        int64_t a = INT64_MIN;
        uint64_t b = 2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(divide_by_zero)
    {
        int32_t a = 100;
        uint32_t b = 0;

        auto c = safe_divide<uint32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::divide_by_zero);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_divide<int>(4, 2u);

        static_assert(a);
        static_assert(*a == 2u);

        auto constexpr b = safe_divide<int32_t>(INT64_MAX, 2u);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // signed_unsigned

BOOST_AUTO_TEST_SUITE(unsigned_signed)

    BOOST_AUTO_TEST_CASE(positive_positive_unsigned_result)
    {
        uint32_t a = 100;
        int32_t b = 2;

        auto c = safe_divide<uint32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 50);
    }

    BOOST_AUTO_TEST_CASE(positive_negative_unsigned)
    {
        uint32_t a = 100;
        int32_t b = -2;

        auto c = safe_divide<uint32_t>(a, b);

        BOOST_TEST(!c);
        BOOST_TEST(c.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(positive_positive_signed)
    {
        uint32_t a = 100;
        int32_t b = 2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == 50);
    }

    BOOST_AUTO_TEST_CASE(positive_negative_signed_1)
    {
        // common type is signed
        uint32_t a = 100;
        int64_t b = -2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == -50);
    }

    BOOST_AUTO_TEST_CASE(positive_negative_signed_2)
    {
        // common type is unsigned
        uint32_t a = 100;
        int32_t b = -2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == -50);
    }

    BOOST_AUTO_TEST_CASE(positive_negative_signed_3)
    {
        // common type is uint64_t
        uint64_t a = 100;
        int32_t b = -2;

        auto c = safe_divide<int32_t>(a, b);

        BOOST_TEST(c);
        BOOST_TEST(*c == -50);
    }

    BOOST_AUTO_TEST_CASE(positive_negative_signed_4)
    {
        if constexpr (is_twos_complement_integer_v<>) {
            // common type is uint64_t
            uint64_t a = INT64_MAX + 100ul; // it cannot be cast int64_t
            int64_t b = INT64_MIN;

            auto c = safe_divide<int32_t>(a, b);

            BOOST_TEST(c);
            BOOST_TEST(*c == -1);
        }
    }

    BOOST_AUTO_TEST_CASE(positive_negative_signed_5)
    {
        if constexpr (is_twos_complement_integer_v<>) {
            // common type is uint64_t
            uint64_t a = INT64_MAX + 100ul; // it cannot be cast int64_t
            int64_t b = -1;

            auto c = safe_divide<int64_t>(a, b);

            BOOST_TEST(!c);
            BOOST_TEST(c.error() == arithmetic_errc::underflow);
        }
    }

    BOOST_AUTO_TEST_CASE(positive_negative_signed_6)
    {
        if constexpr (is_twos_complement_integer_v<>) {
            // common type is uint64_t
            uint64_t a = INT64_MAX + 1ul; // corner case
            int64_t b = -1;

            auto c = safe_divide<int64_t>(a, b);

            BOOST_TEST(c);
            BOOST_TEST(*c == INT64_MIN);
        }
    }

    BOOST_AUTO_TEST_CASE(positive_negative_signed_7)
    {
        if constexpr (is_twos_complement_integer_v<>) {
            // common type is uint64_t
            uint64_t a = INT64_MAX + 100ul;
            int32_t b = INT32_MIN;

            auto c = safe_divide<int64_t>(a, b);

            BOOST_TEST(c);
            BOOST_TEST(*c == INT32_MIN * 2l);
        }
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr a = safe_divide<int>(4u, 2);

        static_assert(a);
        static_assert(*a == 2);

        auto constexpr b = safe_divide<int32_t>(UINT64_MAX, 2);

        static_assert(!b);
        static_assert(b.error() == arithmetic_errc::overflow);
    }

BOOST_AUTO_TEST_SUITE_END() // unsigned_signed

BOOST_AUTO_TEST_SUITE(option)

    BOOST_AUTO_TEST_CASE(no_overflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_overflow;

        auto a  = safe_divide<int32_t, opt>(INT64_MAX, 2);
        BOOST_TEST(a);
        // value is undefined

        auto b  = safe_divide<uint32_t, opt>(-10, 2);
        BOOST_TEST(!b);
        BOOST_TEST(b.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(no_underflow_check_1)
    {
        auto constexpr opt = arithmetic_option::dont_check_underflow;

        auto a  = safe_divide<int32_t, opt>(INT64_MIN, 2);
        BOOST_TEST(a);
        // value is undefined

        auto b  = safe_divide<int32_t, opt>(INT64_MAX, 2);
        BOOST_TEST(!b);
        BOOST_TEST(b.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(no_underflow_check_2)
    {
        if constexpr (is_twos_complement_integer_v<>) {
            auto constexpr opt = arithmetic_option::dont_check_underflow;

            auto a  = safe_divide<uint32_t, opt>(-1, 2);
            BOOST_TEST(a);
            // value is undefined

            auto b  = safe_divide<uint32_t, opt>(-1, 2u);
            BOOST_TEST(b);
            // value is undefined

            auto c  = safe_divide<int64_t, opt>(UINT64_MAX, -1);
            BOOST_TEST(c);
            // value is undefined

            auto d  = safe_divide<uint32_t, opt>(10u, -1);
            BOOST_TEST(d);
            // value is undefined
        }
    }

    BOOST_AUTO_TEST_CASE(no_overflow_underflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_overflow
                           | arithmetic_option::dont_check_underflow;

        auto a  = safe_divide<int32_t, opt>(INT64_MAX, 2);
        BOOST_TEST(a);
        // value is undefined

        auto b  = safe_divide<int32_t, opt>(INT64_MIN, 2);
        BOOST_TEST(b);
        // value is undefined
    }

    BOOST_AUTO_TEST_CASE(no_zero_division_check)
    {
#if 0
        auto constexpr opt = arithmetic_option::dont_check_zero_division;

        // This should cause runtime error
        auto a = safe_divide<int32_t, opt>(100, 0);
        BOOST_TEST(a);
        // value is undefined
#endif
    }

BOOST_AUTO_TEST_SUITE_END() // option

BOOST_AUTO_TEST_SUITE_END() // safe_divide_

BOOST_AUTO_TEST_SUITE_END() // safe_arithmetic
