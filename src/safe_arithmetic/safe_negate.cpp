#include <boost/test/unit_test.hpp>

#include <iostream>

#include <stream9/safe_integer/arithmetic/safe_negate.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic)

BOOST_AUTO_TEST_SUITE(safe_negate_)

BOOST_AUTO_TEST_SUITE(signed_)

    BOOST_AUTO_TEST_CASE(normal)
    {
        auto oc = safe_negate(1);

        static_assert(std::is_same_v<
            decltype(oc), arithmetic_outcome<int> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -1);
    }

    BOOST_AUTO_TEST_CASE(to_smaller_type)
    {
        int32_t a = 1;

        auto oc = safe_negate<int16_t>(a);

        static_assert(std::is_same_v<
            decltype(oc), arithmetic_outcome<int16_t> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -1);
    }

    BOOST_AUTO_TEST_CASE(to_larger_type)
    {
        int32_t a = 1;

        auto oc = safe_negate<int64_t>(a);

        static_assert(std::is_same_v<
            decltype(oc), arithmetic_outcome<int64_t> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -1);
    }

    BOOST_AUTO_TEST_CASE(overflow_1)
    {
        int32_t a = -INT16_MAX;

        auto oc = safe_negate<int16_t>(a);

        static_assert(std::is_same_v<
            decltype(oc), arithmetic_outcome<int16_t> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == INT16_MAX);

        oc = safe_negate<int16_t>(a - 1);

        BOOST_TEST(!oc);
        BOOST_TEST(oc.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(overflow_2)
    {
        if constexpr (is_twos_complement_integer_v<int32_t>) {
            int32_t a = INT32_MIN;

            auto oc = safe_negate(a);

            static_assert(std::is_same_v<
                decltype(oc), arithmetic_outcome<int32_t> >);

            BOOST_TEST(!oc);
            BOOST_TEST(oc.error() == arithmetic_errc::overflow);

            auto oc2 = safe_negate<int64_t>(a);

            static_assert(std::is_same_v<
                decltype(oc2), arithmetic_outcome<int64_t> >);

            BOOST_TEST(oc2);
            BOOST_TEST(*oc2 == static_cast<int64_t>(INT32_MAX) + 1);
        }
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        int32_t a = INT16_MAX + 1;

        auto oc = safe_negate<int16_t>(a);

        static_assert(std::is_same_v<
            decltype(oc), arithmetic_outcome<int16_t> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == INT16_MIN);

        oc = safe_negate<int16_t>(a + 1);

        BOOST_TEST(!oc);
        BOOST_TEST(oc.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr oc = safe_negate(100);

        static_assert(oc);
        static_assert(*oc == -100);
    }

BOOST_AUTO_TEST_SUITE_END() // signed_

BOOST_AUTO_TEST_SUITE(negative_)

    BOOST_AUTO_TEST_CASE(smaller_than_uint64)
    {
        uint16_t a = 1;

        auto oc = safe_negate(a);

        static_assert(std::is_same_v<
                decltype(oc), arithmetic_outcome<int32_t> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -1);
    }

    BOOST_AUTO_TEST_CASE(uint64_1)
    {
        uint64_t a = 1;

        auto oc = safe_negate(a);

        static_assert(std::is_same_v<
                decltype(oc), arithmetic_outcome<int64_t> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -1);
    }

    BOOST_AUTO_TEST_CASE(to_smaller_type)
    {
        uint32_t a = 1;

        auto oc = safe_negate<int16_t>(a);

        static_assert(std::is_same_v<
                decltype(oc), arithmetic_outcome<int16_t> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -1);
    }

    BOOST_AUTO_TEST_CASE(to_larger_type)
    {
        uint32_t a = 1;

        auto oc = safe_negate<int64_t>(a);

        static_assert(std::is_same_v<
                decltype(oc), arithmetic_outcome<int64_t> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -1);
    }

    BOOST_AUTO_TEST_CASE(underflow_1)
    {
        uint32_t a = INT16_MAX;

        auto oc = safe_negate<int16_t>(a);

        static_assert(std::is_same_v<
            decltype(oc), arithmetic_outcome<int16_t> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -INT16_MAX);

        oc = safe_negate<int16_t>(a + 100);

        BOOST_TEST(!oc);
        BOOST_TEST(oc.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(underflow_2)
    {
        uint64_t a = INT64_MAX;

        auto oc = safe_negate<int64_t>(a);

        static_assert(std::is_same_v<
            decltype(oc), arithmetic_outcome<int64_t> >);

        BOOST_TEST(oc);
        BOOST_TEST(*oc == -INT64_MAX);

        oc = safe_negate<int64_t>(a + 1);

        BOOST_TEST(!oc);
        BOOST_TEST(oc.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(underflow_3)
    {
        uint64_t a = UINT64_MAX;

        auto oc = safe_negate<int64_t>(a);

        static_assert(std::is_same_v<
            decltype(oc), arithmetic_outcome<int64_t> >);

        BOOST_TEST(!oc);
        BOOST_TEST(oc.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(underflow_4)
    {
        uint64_t a = 1;

        auto oc = safe_negate<uint64_t>(a);

        static_assert(std::is_same_v<
                decltype(oc), arithmetic_outcome<uint64_t> >);

        BOOST_TEST(!oc);
        BOOST_TEST(oc.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(as_constexpr)
    {
        auto constexpr oc = safe_negate(100u);

        static_assert(oc);
        static_assert(*oc == -100);
    }

BOOST_AUTO_TEST_SUITE_END() // negative_

BOOST_AUTO_TEST_SUITE(option)

    BOOST_AUTO_TEST_CASE(no_overflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_overflow;

        int32_t a = -(INT16_MAX + 10);

        auto oc = safe_negate<int16_t>(a);
        BOOST_TEST(!oc);

        oc  = safe_negate<int16_t, opt>(a);
        BOOST_TEST(oc);
        // value is undefined

        int32_t b = INT16_MAX + 10;

        oc = safe_negate<int16_t, opt>(b);
        BOOST_TEST(!oc);
        BOOST_TEST(oc.error() == arithmetic_errc::underflow);

        uint64_t c = UINT64_MAX;

        auto oc2 = safe_negate<int64_t, opt>(c);
        BOOST_TEST(!oc2);
        BOOST_TEST(oc.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(no_underflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_underflow;

        int32_t a = INT16_MAX + 10;

        auto oc = safe_negate<int16_t>(a);
        BOOST_TEST(!oc);

        oc  = safe_negate<int16_t, opt>(a);
        BOOST_TEST(oc);
        // value is undefined

        uint64_t c = UINT64_MAX;

        auto oc2 = safe_negate<int64_t, opt>(c);
        BOOST_TEST(oc2);
        // value is undefined

        int32_t b = -(INT16_MAX + 10);

        oc = safe_negate<int16_t, opt>(b);
        BOOST_TEST(!oc);
        BOOST_TEST(oc.error() == arithmetic_errc::overflow);
    }

    BOOST_AUTO_TEST_CASE(no_overflow_underflow_check)
    {
        auto constexpr opt = arithmetic_option::dont_check_overflow
                           | arithmetic_option::dont_check_underflow;

        int32_t a = -(INT16_MAX + 10);

        auto oc = safe_negate<int16_t>(a);
        BOOST_TEST(!oc);

        oc  = safe_negate<int16_t, opt>(a);
        BOOST_TEST(oc);
        // value is undefined

        int32_t b = INT16_MAX + 10;

        oc = safe_negate<int16_t>(b);
        BOOST_TEST(!oc);

        oc = safe_negate<int16_t, opt>(b);
        BOOST_TEST(oc);
        // value is undefined
    }

BOOST_AUTO_TEST_SUITE_END() // option

BOOST_AUTO_TEST_SUITE_END() // safe_negate_

BOOST_AUTO_TEST_SUITE_END() // arithmetic
