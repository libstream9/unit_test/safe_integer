#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/arithmetic/safe_convert.hpp>

#include <cstdint>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(safe_arithmetic)

BOOST_AUTO_TEST_SUITE(safe_convert_)

    BOOST_AUTO_TEST_CASE(normal)
    {
        uint32_t a = 10;
        auto b = safe_convert<int32_t>(a);

        static_assert(std::is_same_v<decltype(b)::value_type, int32_t>);
        BOOST_TEST(b);
        BOOST_TEST(*b == 10);
    }

    BOOST_AUTO_TEST_CASE(overflow)
    {
        int64_t a = std::numeric_limits<int32_t>::max();
        int64_t b = std::numeric_limits<int32_t>::max() + 1L;

        auto c = safe_convert<int32_t>(a);
        BOOST_TEST(c);
        BOOST_TEST(*c == a);

        auto d = safe_convert<int32_t>(b);
        BOOST_TEST(!d);
        BOOST_TEST(d.error() == arithmetic_errc::overflow);

        auto e = safe_convert<
                    int32_t, arithmetic_option::dont_check_overflow>(b);
        BOOST_TEST(e);

        auto f = safe_convert<
                    int32_t, arithmetic_option::dont_check_underflow>(b);
        BOOST_TEST(!f);

        auto g = safe_convert<
                    int32_t, arithmetic_option::dont_check_zero_division>(b);
        BOOST_TEST(!g);
    }

    BOOST_AUTO_TEST_CASE(overflow_with_option)
    {
        using o = arithmetic_option;
        int64_t a = std::numeric_limits<int32_t>::max() + 1L;

        auto b = safe_convert<
                    int32_t, o::dont_check_overflow>(a);
        BOOST_TEST(b);

        auto c = safe_convert<
                    int32_t, o::dont_check_underflow>(a);
        BOOST_TEST(!c);

        auto d = safe_convert<
                    int32_t, o::dont_check_zero_division>(a);
        BOOST_TEST(!d);

        auto e = safe_convert<int32_t,
             o::dont_check_overflow | o::dont_check_underflow>(a);
        BOOST_TEST(e);

        auto f = safe_convert<int32_t,
             o::dont_check_underflow | o::dont_check_zero_division>(a);
        BOOST_TEST(!f);

        auto g = safe_convert<int32_t,
             o::dont_check_overflow | o::dont_check_zero_division>(a);
        BOOST_TEST(g);

        auto h = safe_convert<int32_t,
             o::dont_check_overflow | o::dont_check_underflow
                                    | o::dont_check_zero_division>(a);
        BOOST_TEST(h);
    }

    BOOST_AUTO_TEST_CASE(underflow)
    {
        int64_t a = std::numeric_limits<int32_t>::min();
        int64_t b = std::numeric_limits<int32_t>::min() - 1L;

        auto c = safe_convert<int32_t>(a);
        BOOST_TEST(c);
        BOOST_TEST(*c == a);

        auto d = safe_convert<int32_t>(b);
        BOOST_TEST(!d);
        BOOST_TEST(d.error() == arithmetic_errc::underflow);
    }

    BOOST_AUTO_TEST_CASE(underflow_with_option)
    {
        using o = arithmetic_option;
        int64_t a = std::numeric_limits<int32_t>::min() - 1L;

        auto b = safe_convert<
                    int32_t, o::dont_check_overflow>(a);
        BOOST_TEST(!b);

        auto c = safe_convert<
                    int32_t, o::dont_check_underflow>(a);
        BOOST_TEST(c);

        auto d = safe_convert<
                    int32_t, o::dont_check_zero_division>(a);
        BOOST_TEST(!d);

        auto e = safe_convert<int32_t,
             o::dont_check_overflow | o::dont_check_underflow>(a);
        BOOST_TEST(e);

        auto f = safe_convert<int32_t,
             o::dont_check_underflow | o::dont_check_zero_division>(a);
        BOOST_TEST(f);

        auto g = safe_convert<int32_t,
             o::dont_check_overflow | o::dont_check_zero_division>(a);
        BOOST_TEST(!g);

        auto h = safe_convert<int32_t,
             o::dont_check_overflow | o::dont_check_underflow
                                    | o::dont_check_zero_division>(a);
        BOOST_TEST(h);
    }

BOOST_AUTO_TEST_SUITE_END() // safe_convert

BOOST_AUTO_TEST_SUITE_END() // safe_arithmetic
