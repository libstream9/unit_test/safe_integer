#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/utility.hpp>

using namespace stream9::safe_integers;

BOOST_AUTO_TEST_SUITE(arithmetic_outcome__)

    BOOST_AUTO_TEST_CASE(construction)
    {
        arithmetic_outcome<int> a = 1;
        arithmetic_outcome<int> b = arithmetic_errc::overflow;

        (void)a; (void)b;
    }

    BOOST_AUTO_TEST_SUITE(equal)

        BOOST_AUTO_TEST_CASE(same_type)
        {
            arithmetic_outcome<int> a = 1;
            arithmetic_outcome<int> b = 1;
            arithmetic_outcome<int> c = 2;

            BOOST_TEST(a == b);
            BOOST_TEST(b == a);
            BOOST_TEST(!(a == c));
            BOOST_TEST(!(c == a));
        }

        BOOST_AUTO_TEST_CASE(different_size_type)
        {
            arithmetic_outcome<int32_t> a = 1;
            arithmetic_outcome<int64_t> b = 1;
            arithmetic_outcome<int64_t> c = 2;

            BOOST_TEST(a == b);
            BOOST_TEST(b == a);
            BOOST_TEST(!(a == c));
            BOOST_TEST(!(c == a));
        }

        BOOST_AUTO_TEST_CASE(different_sign_type)
        {
            arithmetic_outcome<int32_t> a = 1;
            arithmetic_outcome<uint32_t> b = 1;
            arithmetic_outcome<uint32_t> c = 2;

            BOOST_TEST(a == b);
            BOOST_TEST(b == a);
            BOOST_TEST(!(a == c));
            BOOST_TEST(!(c == a));
        }

        BOOST_AUTO_TEST_CASE(error)
        {
            arithmetic_outcome<int> a = arithmetic_errc::overflow;
            arithmetic_outcome<int> b = arithmetic_errc::overflow;
            arithmetic_outcome<int> c = 1;
            arithmetic_outcome<int> d = arithmetic_errc::underflow;

            BOOST_TEST(a == b);
            BOOST_TEST(b == a);
            BOOST_TEST(!(a == c));
            BOOST_TEST(!(c == a));
            BOOST_TEST(!(a == d));
            BOOST_TEST(!(d == a));
        }

    BOOST_AUTO_TEST_SUITE_END() // equal

    BOOST_AUTO_TEST_SUITE(not_equal)

        BOOST_AUTO_TEST_CASE(same_type)
        {
            arithmetic_outcome<int> a = 1;
            arithmetic_outcome<int> b = 2;
            arithmetic_outcome<int> c = 1;

            BOOST_TEST(a != b);
            BOOST_TEST(b != a);
            BOOST_TEST(!(a != c));
            BOOST_TEST(!(c != a));
        }

        BOOST_AUTO_TEST_CASE(different_size_type)
        {
            arithmetic_outcome<int32_t> a = 1;
            arithmetic_outcome<int64_t> b = 2;
            arithmetic_outcome<int64_t> c = 1;

            BOOST_TEST(a != b);
            BOOST_TEST(b != a);
            BOOST_TEST(!(a != c));
            BOOST_TEST(!(c != a));
        }

        BOOST_AUTO_TEST_CASE(different_sign_type)
        {
            arithmetic_outcome<int32_t> a = 1;
            arithmetic_outcome<uint32_t> b = 2;
            arithmetic_outcome<uint32_t> c = 1;

            BOOST_TEST(a != b);
            BOOST_TEST(b != a);
            BOOST_TEST(!(a != c));
            BOOST_TEST(!(c != a));
        }

        BOOST_AUTO_TEST_CASE(error)
        {
            arithmetic_outcome<int> a = arithmetic_errc::overflow;
            arithmetic_outcome<int> b = arithmetic_errc::underflow;
            arithmetic_outcome<int> c = 1;
            arithmetic_outcome<int> d = arithmetic_errc::overflow;

            BOOST_TEST(a != b);
            BOOST_TEST(b != a);
            BOOST_TEST(a != c);
            BOOST_TEST(c != a);
            BOOST_TEST(!(a != d));
            BOOST_TEST(!(d != a));
        }

    BOOST_AUTO_TEST_SUITE_END() // not_equal

    BOOST_AUTO_TEST_SUITE(less)

        BOOST_AUTO_TEST_CASE(same_type)
        {
            arithmetic_outcome<int> a = 1;
            arithmetic_outcome<int> b = 2;
            arithmetic_outcome<int> c = 1;

            BOOST_TEST(a < b);
            BOOST_TEST(!(b < a));
            BOOST_TEST(!(a < c));
            BOOST_TEST(!(c < a));
        }

        BOOST_AUTO_TEST_CASE(different_size_type)
        {
            arithmetic_outcome<int32_t> a = 1;
            arithmetic_outcome<int64_t> b = 2;
            arithmetic_outcome<int64_t> c = 1;

            BOOST_TEST(a < b);
            BOOST_TEST(!(b < a));
            BOOST_TEST(!(a < c));
            BOOST_TEST(!(c < a));
        }

        BOOST_AUTO_TEST_CASE(different_sign_type)
        {
            arithmetic_outcome<int32_t> a = 1;
            arithmetic_outcome<uint32_t> b = 2;
            arithmetic_outcome<uint32_t> c = 1;

            BOOST_TEST(a < b);
            BOOST_TEST(!(b < a));
            BOOST_TEST(!(a < c));
            BOOST_TEST(!(c < a));
        }

        BOOST_AUTO_TEST_CASE(error)
        {
            arithmetic_outcome<int> a = arithmetic_errc::underflow;
            arithmetic_outcome<int> b = arithmetic_errc::overflow;
            arithmetic_outcome<int> c = 1;
            arithmetic_outcome<int> d = arithmetic_errc::divide_by_zero;

            BOOST_TEST(a < b);
            BOOST_TEST(!(b < a));

            BOOST_TEST(a < c);
            BOOST_TEST(!(c < a));
            BOOST_TEST(!(b < c));
            BOOST_TEST(c < b);

            BOOST_TEST(!(a < d));
            BOOST_TEST(!(d < a));
        }

    BOOST_AUTO_TEST_SUITE_END() // less

    BOOST_AUTO_TEST_SUITE(greater)

        BOOST_AUTO_TEST_CASE(same_type)
        {
            arithmetic_outcome<int> a = 2;
            arithmetic_outcome<int> b = 1;
            arithmetic_outcome<int> c = 2;

            BOOST_TEST(a > b);
            BOOST_TEST(!(b > a));
            BOOST_TEST(!(a > c));
            BOOST_TEST(!(c > a));
        }

        BOOST_AUTO_TEST_CASE(different_size_type)
        {
            arithmetic_outcome<int32_t> a = 2;
            arithmetic_outcome<int64_t> b = 1;
            arithmetic_outcome<int64_t> c = 2;

            BOOST_TEST(a > b);
            BOOST_TEST(!(b > a));
            BOOST_TEST(!(a > c));
            BOOST_TEST(!(c > a));
        }

        BOOST_AUTO_TEST_CASE(different_sign_type)
        {
            arithmetic_outcome<int32_t> a = 2;
            arithmetic_outcome<uint32_t> b = 1;
            arithmetic_outcome<uint32_t> c = 2;

            BOOST_TEST(a > b);
            BOOST_TEST(!(b > a));
            BOOST_TEST(!(a > c));
            BOOST_TEST(!(c > a));
        }

        BOOST_AUTO_TEST_CASE(error)
        {
            arithmetic_outcome<int> a = arithmetic_errc::overflow;
            arithmetic_outcome<int> b = arithmetic_errc::underflow;
            arithmetic_outcome<int> c = 1;
            arithmetic_outcome<int> d = arithmetic_errc::divide_by_zero;

            BOOST_TEST(a > b);
            BOOST_TEST(!(b > a));

            BOOST_TEST(a > c);
            BOOST_TEST(!(c > a));
            BOOST_TEST(!(b > c));
            BOOST_TEST(c > b);

            BOOST_TEST(!(b > d));
            BOOST_TEST(!(d > b));
        }

    BOOST_AUTO_TEST_SUITE_END() // greater

    BOOST_AUTO_TEST_SUITE(max_)

        using utility::max;

        BOOST_AUTO_TEST_CASE(one_value)
        {
            arithmetic_outcome<int> a = 1;

            BOOST_TEST(max(a) == a);
        }

        BOOST_AUTO_TEST_CASE(two_values)
        {
            arithmetic_outcome<int> a = 1;
            arithmetic_outcome<int> b = 2;

            BOOST_TEST(max(a, b) == b);
        }

        BOOST_AUTO_TEST_CASE(three_values)
        {
            arithmetic_outcome<int> a = 1;
            arithmetic_outcome<int> b = 2;
            arithmetic_outcome<int> c = -1;

            BOOST_TEST(max(a, b, c) == b);
        }

        BOOST_AUTO_TEST_CASE(including_errors)
        {
            arithmetic_outcome<int> a = 1;
            arithmetic_outcome<int> b = -1;
            arithmetic_outcome<int> c = arithmetic_errc::overflow;
            arithmetic_outcome<int> d = arithmetic_errc::underflow;

            BOOST_TEST(max(a, b, c, d) == c);
        }

    BOOST_AUTO_TEST_SUITE_END() // max

    BOOST_AUTO_TEST_SUITE(min_)

        using utility::min;

        BOOST_AUTO_TEST_CASE(one_value)
        {
            arithmetic_outcome<int> a = 1;

            BOOST_TEST(min(a) == a);
        }

        BOOST_AUTO_TEST_CASE(two_values)
        {
            arithmetic_outcome<int> a = 1;
            arithmetic_outcome<int> b = 2;

            BOOST_TEST(min(a, b) == a);
        }

        BOOST_AUTO_TEST_CASE(three_values)
        {
            arithmetic_outcome<int> a = 1;
            arithmetic_outcome<int> b = 2;
            arithmetic_outcome<int> c = -1;

            BOOST_TEST(min(a, b, c) == c);
        }

        BOOST_AUTO_TEST_CASE(including_errors)
        {
            arithmetic_outcome<int> a = 1;
            arithmetic_outcome<int> b = -1;
            arithmetic_outcome<int> c = arithmetic_errc::overflow;
            arithmetic_outcome<int> d = arithmetic_errc::underflow;

            BOOST_TEST(min(a, b, c, d) == d);
        }

    BOOST_AUTO_TEST_SUITE_END() // min

BOOST_AUTO_TEST_SUITE_END() // arithmetic_outcome__
