#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

namespace safe_integers::tests {

BOOST_AUTO_TEST_SUITE(conversion)

    BOOST_AUTO_TEST_SUITE(to_underlying_builtin)

        BOOST_AUTO_TEST_CASE(explicit_)
        {
            safe_integer<int> a = 100;

            BOOST_TEST(static_cast<int>(a) == a);
        }

        BOOST_AUTO_TEST_CASE(implicit)
        {
            safe_integer<int> a = 100;

            int b = a;

            BOOST_TEST(b == a);
        }

        BOOST_AUTO_TEST_CASE(as_constexpr)
        {
            safe_integer<int> constexpr a = 100;

            static_assert(static_cast<int>(a) == a);
        }

    BOOST_AUTO_TEST_SUITE_END() // to_underlying_builtin

    BOOST_AUTO_TEST_SUITE(to_different_builtin)

        BOOST_AUTO_TEST_CASE(explicit_)
        {
            safe_integer<int> a = 100;

            BOOST_TEST(static_cast<unsigned>(a) == a);
        }

        BOOST_AUTO_TEST_CASE(implicit)
        {
            safe_integer<int> a = 100;

            unsigned b = a;

            BOOST_TEST(b == a);
        }

        BOOST_AUTO_TEST_CASE(as_constexpr)
        {
            safe_integer<int> constexpr a = 100;

            static_assert(static_cast<unsigned>(a) == a);
        }

        BOOST_AUTO_TEST_CASE(overflow)
        {
            safe_integer<int32_t> a = INT32_MAX;

            BOOST_CHECK_THROW(static_cast<int16_t>(a), overflow_error);
        }

        BOOST_AUTO_TEST_CASE(underflow)
        {
            safe_integer<int32_t> a = INT32_MIN;

            BOOST_CHECK_THROW(static_cast<int16_t>(a), underflow_error);
        }

    BOOST_AUTO_TEST_SUITE_END() // to_different_builtin

BOOST_AUTO_TEST_SUITE_END() // conversion

} // namespace safe_integers::tests
