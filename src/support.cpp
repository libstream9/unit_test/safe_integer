#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

namespace safe_integers::test {

BOOST_AUTO_TEST_SUITE(is_signed_)

    BOOST_AUTO_TEST_CASE(true_)
    {
        static_assert(std::is_signed_v<safe_integer<int>>);
    }

    BOOST_AUTO_TEST_CASE(false_)
    {
        static_assert(!std::is_signed_v<safe_integer<size_t>>);
        //static_assert(!std::is_signed_v<safe_integer<int, 0, 100>>); //TODO shouldnt this hold?
    }

BOOST_AUTO_TEST_SUITE_END() // is_signed_

BOOST_AUTO_TEST_SUITE(is_unsigned_)

    BOOST_AUTO_TEST_CASE(true_)
    {
        static_assert(std::is_unsigned_v<safe_integer<size_t>>);
        //static_assert(std::is_unsigned_v<safe_integer<int, 0, 100>>); //TODO shouldnt this hold?
    }

    BOOST_AUTO_TEST_CASE(false_)
    {
        static_assert(!std::is_unsigned_v<safe_integer<int>>);
    }

BOOST_AUTO_TEST_SUITE_END() // is_unsigned_

BOOST_AUTO_TEST_SUITE(is_integral_)

    BOOST_AUTO_TEST_CASE(true_)
    {
        static_assert(std::is_integral_v<safe_integer<int>>);
        static_assert(std::is_integral_v<safe_integer<int, 100>>);
        static_assert(std::is_integral_v<safe_integer<int, -100, 0>>);
    }

BOOST_AUTO_TEST_SUITE_END() // is_integral_

BOOST_AUTO_TEST_SUITE(numeric_limits_)

    BOOST_AUTO_TEST_CASE(min_)
    {
        using T = safe_integer<int, 100, 200>;

        static_assert(std::numeric_limits<T>::min() == 100);
    }

    BOOST_AUTO_TEST_CASE(max_)
    {
        using T = safe_integer<int, 100, 200>;

        static_assert(std::numeric_limits<T>::max() == 200);
    }

    BOOST_AUTO_TEST_CASE(is_integer_)
    {
        using T = safe_integer<int, 100, 200>;

        static_assert(std::numeric_limits<T>::is_integer);
    }

BOOST_AUTO_TEST_SUITE_END() // numeric_limits_

} // namespace testing
