#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

//TODO change to safe_convert's test
BOOST_AUTO_TEST_SUITE(integral_cast_)

    BOOST_AUTO_TEST_CASE(signed_to_unsigned_1)
    {
        int a = 100;
        auto b = integral_cast<unsigned>(a);

        static_assert(std::is_same_v<decltype(b), unsigned>);
        BOOST_TEST(b == 100);
    }

    BOOST_AUTO_TEST_CASE(signed_to_unsigned_overflow)
    {
        int a = -100;
        BOOST_CHECK_THROW(integral_cast<unsigned>(a), conversion_error);
    }

    BOOST_AUTO_TEST_CASE(smaller_type_to_larger_type)
    {
        int32_t a = 100;
        auto b = integral_cast<int64_t>(a);

        static_assert(std::is_same_v<decltype(b), int64_t>);
        BOOST_TEST(b == 100L);
    }

    BOOST_AUTO_TEST_CASE(larger_type_to_smaller_type)
    {
        int64_t a = 100;
        auto b = integral_cast<int32_t>(a);

        static_assert(std::is_same_v<decltype(b), int32_t>);
        BOOST_TEST(b == 100);
    }

    BOOST_AUTO_TEST_CASE(larger_type_to_smaller_type_overflow)
    {
        int64_t a = std::numeric_limits<int64_t>::max();
        BOOST_CHECK_THROW(integral_cast<int32_t>(a), conversion_error);

        int64_t b = std::numeric_limits<int64_t>::min();
        BOOST_CHECK_THROW(integral_cast<int32_t>(b), conversion_error);
    }

    BOOST_AUTO_TEST_CASE(constexpr_ness)
    {
        auto constexpr a = integral_cast<int64_t>(100);

        static_assert(std::is_same_v<decltype(a), int64_t const>);
        BOOST_TEST(a == 100);
    }

BOOST_AUTO_TEST_SUITE_END() // integral_cast
