#include <boost/test/unit_test.hpp>

#include <stream9/safe_integer/safe_integer.hpp>

using namespace stream9::safe_integers;

namespace safe_integers::tests {

BOOST_AUTO_TEST_SUITE(assignment)

    BOOST_AUTO_TEST_CASE(copy_assignment)
    {
        safe_integer a = 100;
        safe_integer b = 200;

        BOOST_TEST(a != b);

        a = b;

        BOOST_TEST(a == b);
    }

    BOOST_AUTO_TEST_CASE(builtin_type)
    {
        safe_integer<int> a = 1;
        int b = 2;

        BOOST_TEST(a != b);

        a = b;

        BOOST_TEST(a == b);
    }

    BOOST_AUTO_TEST_CASE(different_type_value)
    {
        safe_integer<int> a = 1;
        size_t b = 2;

        BOOST_TEST(a != b);

        a = b;

        BOOST_TEST(a == b);
    }

    BOOST_AUTO_TEST_CASE(different_safe_integer)
    {
        safe_integer<int32_t> a = 1;
        safe_integer<int64_t, 0> b = 2;

        BOOST_TEST(a != b);

        a = b;

        BOOST_TEST(a == b);
    }

    BOOST_AUTO_TEST_SUITE(overflow)

        BOOST_AUTO_TEST_CASE(from_safe_integer)
        {
            safe_integer<int32_t> a = 10;
            safe_integer<uint32_t> b = INT32_MAX + 1u;

            BOOST_CHECK_THROW(a = b, overflow_error);
        }

        BOOST_AUTO_TEST_CASE(from_builtin)
        {
            safe_integer<int32_t> a = 10;
            uint32_t b = INT32_MAX + 1u;

            BOOST_CHECK_THROW(a = b, overflow_error);
        }

        BOOST_AUTO_TEST_CASE(from_safe_integer_to_bounded)
        {
            safe_integer<int32_t, 0, 10> a = 10;
            safe_integer<int32_t> b = 15;

            BOOST_CHECK_THROW(a = b, overflow_error);
        }

        BOOST_AUTO_TEST_CASE(from_builtin_to_bounded)
        {
            safe_integer<int32_t, 0, 10> a = 10;
            int32_t b = 15;

            BOOST_CHECK_THROW(a = b, overflow_error);
        }

    BOOST_AUTO_TEST_SUITE_END() // overflow

    BOOST_AUTO_TEST_SUITE(underflow)

        BOOST_AUTO_TEST_CASE(from_safe_integer)
        {
            safe_integer<uint32_t> a = 10;
            safe_integer<int32_t> b = -10;

            BOOST_CHECK_THROW(a = b, underflow_error);
        }

        BOOST_AUTO_TEST_CASE(from_builtin)
        {
            safe_integer<uint32_t> a = 10;
            int32_t b = -10;

            BOOST_CHECK_THROW(a = b, underflow_error);
        }

        BOOST_AUTO_TEST_CASE(from_safe_integer_to_bounded)
        {
            safe_integer<int32_t, 0, 10> a = 10;
            safe_integer<int32_t> b = -1;

            BOOST_CHECK_THROW(a = b, underflow_error);
        }

        BOOST_AUTO_TEST_CASE(from_builtin_to_bounded)
        {
            safe_integer<int32_t, 0, 10> a = 10;
            int32_t b = -10;

            BOOST_CHECK_THROW(a = b, underflow_error);
        }

    BOOST_AUTO_TEST_SUITE_END() // underflow

BOOST_AUTO_TEST_SUITE_END() // assignment

} // namespace safe_integers::tests
